@extends('partials.main')
@section('content')
    @if(isset($content['header_status']) && $content['header_status'] == 'on')
        <!-- banner -->
        <section class="ic-banner glance" style="background-image: url({{ Storage::url('images/6l9NyOXqjeK3loy2is0VUg2bgErWNVvkCoi2qAKp.png') }})">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="ic-banner-caption">
                            <h1>Latest News</h1>
                            <div class="ic-breadcrumb">
                                <ul>
                                    <li><a href="/">Home</a></li>
                                    <li><span> > </span></li>
                                    <li><a href="/">Latest News</a></li>
                                    <li><span> > </span></li>
                                    <li>{{ $latest_news->title }}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- banner /end -->
    @else
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="ic-banner-caption ic-banner-caption-inverse">
                        <h1>Latest News</h1>
                        <div class="ic-breadcrumb ic-breadcrumb-inverse">
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><span> > </span></li>
                                <li><a href="/">Latest News</a></li>
                                <li><span> > </span></li>
                                <li>{{ $latest_news->title }}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- message and news -->
    <section class="ic-notice-gallery">
        <div class="container">
            <div class="row">
                <div class="ic-single-card">
                    @if($latest_news->news_image != '')
                        <figure>
                            <img class="img-responsive" src="{{ Storage::url($latest_news->news_image) }}" alt="">
                            <a href="{{ Storage::url($latest_news->news_image) }}" data-fancybox="group" data-caption="Image caption here">
                            </a>
                        </figure>
                    @endif
                    {!! $latest_news->details !!}
                </div>
            </div>
        </div>
    </section>
    <!-- message and news /end-->
@endsection
