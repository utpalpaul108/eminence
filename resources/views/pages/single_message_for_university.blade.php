@extends('partials.main')

@section('content')
    @if(isset($content['header_status']) && $content['header_status'] == 'on')
    <!-- banner -->
    <section class="ic-banner message" style="background-image: url({{ Storage::url('images/banner/message.png') }}">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="ic-banner-caption">
                        {{--<h1>Principal Message</h1>--}}
                        <div class="ic-breadcrumb">
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><span> > </span></li>
                                <li>Message</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner /end -->
    @else
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="ic-banner-caption ic-banner-caption-inverse">
                        {{--<h1>Principal Message</h1>--}}
                        <div class="ic-breadcrumb ic-breadcrumb-inverse">
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><span> > </span></li>
                                <li>Message</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- message and news -->
    <section class="ic-message-n-news">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="ic-single-message">
                        <div class="ic-single-message-figcap">
                            <div class="ic-ic-smf-figure">
                                <p>
                                    <img class="img-responsive" src="{{ Storage::url($message->profile_image) }}" alt="Principal detail" style="max-height: 370px">
                                    {!! $message->message !!}
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="ic-news-card">
                        <div class="ic-title">
                            <h2>Latest news</h2>
                        </div>
                       @include('partials.latest_news')
                    </div>
                    <div class="ic-news-card notice-board">
                        <div class="ic-title">
                            <h2>Notice Board</h2>
                        </div>
                        @include('partials.notice_board')
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- message and news /end-->
@endsection