@extends('partials.main')

@section('content')
    @if(isset($content['header_status']) && $content['header_status'] == 'on')
        <!-- banner -->
        <section class="ic-banner teacher" style="background-image: url({{ Storage::url($content['header_image']) }})">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="ic-banner-caption">
                            <h1>{{ $content['header_title'] }}</h1>
                            <div class="ic-breadcrumb">
                                <ul>
                                    <li><a href="/">Home</a></li>
                                    <li><span> > </span></li>
                                    <li><a href="/">Academic</a></li>
                                    <li><span> > </span></li>
                                    <li><a href="/faculty_members">Faculty members</a></li>
                                    <li><span> > </span></li>
                                    <li>faculty teacher</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- banner /end -->
    @else
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="ic-banner-caption ic-banner-caption-inverse">
                        <h1>{{ $content['header_title'] }}</h1>
                        <div class="ic-breadcrumb ic-breadcrumb-inverse">
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><span> > </span></li>
                                <li><a href="/">Academic</a></li>
                                <li><span> > </span></li>
                                <li><a href="/faculty_members">Faculty members</a></li>
                                <li><span> > </span></li>
                                <li>faculty teacher</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- teachers -->
    @include('partials.all_faculty_members')
    <!-- teachers /end -->
@endsection