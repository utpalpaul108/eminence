@extends('partials.main')

@section('content')

    @if(isset($content['header_status']) && $content['header_status'] == 'on')
        <!-- banner -->
        <section class="ic-banner class-routine" style="background-image: url({{ Storage::url($content['header_image']) }})">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="ic-banner-caption">
                            <h1>{{ $content['header_title'] }}</h1>
                            <div class="ic-breadcrumb">
                                <ul>
                                    <li><a href="/">Home</a></li>
                                    <li><span> > </span></li>
                                    <li><a href="/">Department</a></li>
                                    <li><span> > </span></li>
                                    <li>Class Routine</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- banner /end -->
    @else
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="ic-banner-caption ic-banner-caption-inverse">
                        <h1>{{ $content['header_title'] }}</h1>
                        <div class="ic-breadcrumb ic-breadcrumb-inverse">
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><span> > </span></li>
                                <li><a href="/">Department</a></li>
                                <li><span> > </span></li>
                                <li>Class Routine</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- class routine -->
    <section class="ic-class-routine">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="ic-course-requirements">
                        <h2>{{ $content['page_title'] }}</h2>
                        {!! $content['page_details'] !!}
                    </div>
                    <div class="ic-class-routine-tbl ic-course-entry  table-responsive ic-narrow-screen-tbl">
                        <table class="table">
                            <tr>
                                <th class="ic-rsn">Serial No</th>
                                <th><span>{{ $content['routine_title'] }}</span></th>
                                <th></th>
                                <th></th>
                            </tr>

                            @php($sl=1)
                            @foreach($all_routine as $routine)
                                <tr>
                                    <td class="ic-s-no">{{ $sl }}</td>
                                    <td class="ic-course-title">{{ $routine->routine_title }}</td>
                                    {!! Form::open(['action'=>'DownloadFileController@download', 'method'=>'GET']) !!}
                                        <input type="hidden" name="url" value="{{ $routine->download_url }}">
                                        <td class="ic-download-routine">
                                            <button class="no_button_style" type="submit">Download</button>
                                        </td>
                                    {!! Form::close() !!}
                                    <td><a href="#" data-toggle="modal" data-target="#icView{{ $sl }}">View</a></td>

                                    <!-- Modal -->
                                    <div id="icView{{ $sl }}" class="modal fade" role="dialog">
                                        <div class="modal-dialog modal-lg">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Class Routine</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <embed src="{{ Storage::url($routine->download_url) }}"
                                                           frameborder="0" width="100%" height="400px">

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </tr>
                                @php($sl++)
                            @endforeach
                        </table>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="ic-news-card">
                        <div class="ic-title">
                            <h2>{{ $content['latest_news_title'] }}</h2>
                        </div>
                        @include('partials.latest_news')
                    </div>
                    <div class="ic-news-card notice-board">
                        <div class="ic-title">
                            <h2>{{ $content['notice_board_title'] }}</h2>
                        </div>
                        @include('partials.notice_board')
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- class routine /end-->
@endsection