@extends('partials.main')

@section('content')

    @if(isset($content['header_status']) && $content['header_status'] == 'on')
        <!-- banner -->
        <section class="ic-banner contact-us" style="background-image: url({{ Storage::url($content['header_image']) }})">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="ic-banner-caption">
                            <h1>Contact us</h1>
                            <div class="ic-breadcrumb">
                                <ul>
                                    <li><a href="/">Home</a></li>
                                    <li><span> > </span></li>
                                    <li>Contact us</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- banner /end -->
    @else
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="ic-banner-caption ic-banner-caption-inverse">
                        <h1>Contact us</h1>
                        <div class="ic-breadcrumb ic-breadcrumb-inverse">
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><span> > </span></li>
                                <li>Contact us</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- cotnact us -->
    <section class="ic-contact-us">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="ic-contact-col">
                        <div class="ic-title">
                            <h2>{{ $content['header_title'] }}</h2>
                        </div>
                        <div class="ic-contact-log">
                            <a href="#">
                                <img src="{{ Storage::url('images/contact-logo.png') }}" alt="">
                            </a>
                        </div>
                        <div class="ic-contact-address">
                            <address>
                                {{ $content['contact_address'] }}
                            </address>
                        </div>
                        <div class="ic-single-contact land-phone">
                            <h3 style="background-image: url({{ Storage::url('images/telephone.png') }})">{{ $content['phone_number_title'] }}</h3>
                            @foreach($content['phone_number'] as $phone_number)
                                <a href="tel:{{ $phone_number }}">{{ $phone_number }} </a><br>
                            @endforeach
                        </div>
                        <div class="ic-single-contact cell-phone">
                            <h3 style="background-image: url({{ Storage::url('images/cell-phone.png') }})">{{ $content['mobile_number_title'] }}</h3>
                            @foreach($content['mobile_number'] as $mobile_number)
                                <a href="tel:{{ $mobile_number }}3">{{ $mobile_number }}</a> <br>
                            @endforeach
                        </div>
                        <div class="ic-single-contact mail">
                            <h3>{{ $content['mail_address_title'] }}</h3>
                            <a href="mailto:{{ $content['mail_address'] }}">{{ $content['mail_address'] }}</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-md-offset-3">
                    <div class="ic-contact-tomail">
                        <div class="ic-title">
                            <h2>{{ $content['message_us_title'] }}</h2>
                        </div>
                        <p>{{ $content['message_us_subtitle'] }}</p>
                        {!! Form::open(['action'=>'SendMailController@send']) !!}
                            <input type="text" id="urname" placeholder="Your name">
                            <input type="email" id="uremail" placeholder="Your email">
                            <input type="text" placeholder="Subject">
                            <textarea name="icmessage" placeholder="Your Message" id="id-message" cols="30" rows="7"></textarea>
                            <button type="submit" class="ic-btn">Send message</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- cotnact us /end -->
@endsection