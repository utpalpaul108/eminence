@extends('partials.main')
@section('content')
    @if(isset($content['header_status']) && $content['header_status'] == 'on')
        <!-- banner -->
        <section class="ic-banner glance" style="background-image: url({{ Storage::url($content['header_image']) }})">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="ic-banner-caption">
                            <h1>{{ $content['header_title'] }}</h1>
                            <div class="ic-breadcrumb">
                                <ul>
                                    <li><a href="/">Home</a></li>
                                    <li><span> > </span></li>
                                    <li>Admission</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- banner /end -->
    @else
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="ic-banner-caption ic-banner-caption-inverse">
                        <h1>{{ $content['header_title'] }}</h1>
                        <div class="ic-breadcrumb ic-breadcrumb-inverse">
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><span> > </span></li>
                                <li>Admission</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- message and news -->
    <section class="ic-message-n-news">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="ic-fig">
                        <img class="img-responsive" src="{{ Storage::url($content['page_image']) }}" alt="">
                    </div>
                    {!! $content['page_details'] !!}
                    @if(isset($page->contents['download_course_outline']) && $page->contents['download_course_outline'] != null)
                        <a href="#" class="ic-gbtn" data-toggle="modal" data-target="#myModal">View</a>
                        <a href="{{ action('PageController@download_course_outline',['id'=>$page->id]) }}" class="ic-gbtn download">Download</a>

                        <!-- Modal -->
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Admission Info</h4>
                                    </div>
                                    <div class="modal-body">
                                        <embed src="{{ Storage::url($page->contents['download_course_outline']) }}"
                                               frameborder="0" width="100%" height="400px">

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-md-4">
                    <div class="ic-news-card">
                        <div class="ic-title">
                            <h2>{{ $content['latest_news_title'] }}</h2>
                        </div>
                        @include('partials.latest_news')
                    </div>
                    <div class="ic-news-card notice-board">
                        <div class="ic-title">
                            <h2>{{ $content['notice_board_title'] }}</h2>
                        </div>
                        @include('partials.notice_board')
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- message and news /end-->
@endsection
