@extends('partials.main')

@section('content')
    <!-- home slider -->
    @include('partials.slider')
    <!-- home slider /end -->

    @include('partials.hot_news')

    <!-- welcome -->
    <section class="ic-welcome">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <figure>
                        <img class="img-responsive" src="{{ Storage::url($content['page_image']) }}" alt="Welcome photo">
                    </figure>
                </div>
                <div class="col-md-6 col-md-offset-1">
                    <div class="ic-welcome-caption">
                        <h2>{{ $content['page_title'] }}</h2>
                        <h3>{{ $content['page_subtitle'] }}</h3>
                        {!! $content['page_details'] !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- welcome /end -->

    <!-- message and news -->
    <section class="ic-message-n-news">
        <div class="container">
            <div class="row">
                @include('partials.message_for_university')
                <div class="col-md-4">
                    {{-- For Latest News --}}
                    <div class="ic-news-card">
                        <div class="ic-title">
                            <h2>{{ $content['latest_news_title'] }}</h2>
                        </div>
                        @include('partials.latest_news')
                    </div>

                    {{-- For Notice Board --}}
                    <div class="ic-news-card notice-board">
                        <div class="ic-title">
                            <h2>{{ $content['notice_board_title'] }}</h2>
                        </div>
                        @include('partials.notice_board')
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- message and news /end-->

    <!-- gallery -->
    <section class="ic-gallery">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="ic-title text-center">
                        <h2>{{ $content['photo_gallery_title'] }}</h2>
                    </div>
                    @if(isset($content['photo_gallery_subtitle']) && $content['photo_gallery_subtitle'] !='')
                        <div class="ic-portfolio-lead">
                            {{ $content['photo_gallery_subtitle'] }}
                        </div>
                    @endif
                </div>
            </div>
            @include('partials.photo_gallery')

        </div>
        </div>
    </section>
    <!-- gallery /end -->
@endsection