@extends('partials.main')
@section('content')
    @if(isset($content['header_status']) && $content['header_status'] == 'on')
        <!-- banner -->
        <section class="ic-banner check-result" style="background-image: url({{ Storage::url($content['header_image']) }})">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="ic-banner-caption">
                            <h1>{{ $content['header_title'] }}</h1>
                            <div class="ic-breadcrumb">
                                <ul>
                                    <li><a href="/">Home</a></li>
                                    <li><span> > </span></li>
                                    <li>Results</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- banner /end -->
    @else
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="ic-banner-caption ic-banner-caption-inverse">
                        <h1>{{ $content['header_title'] }}</h1>
                        <div class="ic-breadcrumb ic-breadcrumb-inverse">
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><span> > </span></li>
                                <li>Results</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- class routine -->
    <section class="ic-class-routine" >
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="ic-check-result">
                        <h2>{{ $content['result_title'] }}</h2>
                        {!! $content['result_details'] !!}
                    </div>
                    <div class="ic-result-form">
                        <form action="#">
                            <input type="text" name="student_id" id="student_id" placeholder="Student ID" required>
                            <select name="section_id" id="section_id" required>
                                <option value="">Select Section</option>
                            </select>
                            <select name="exam_type" id="exam_type" required>
                                <option value="">Select Exam Type</option>
                                @foreach($exam_types as $exam_type)
                                    <option value="{{ $exam_type->id }}">{{ $exam_type->exam_type }}</option>
                                @endforeach
                            </select>
                            <div>
                                <p id="success" class="text-primary show_result"></p>
                                <p id="failed" class="text-danger show_result"></p>
                            </div>
                            <button type="submit" id="show_result" class="ic-btn">Submit</button>
                        </form>
                    </div>
                    {{--<div class="ic-hsc-result">--}}
                        {{--<h3>{{ $content['hsc_result_title'] }}</h3>--}}
                        {{--{!! $content['hsc_result_details']  !!}--}}
                        {{--{!! Form::open(['action'=>'HscResultController@download','method'=>'get']) !!}--}}
                        {{--<select name="year" id="ic-hsc-result" required>--}}
                            {{--@foreach($hsc_results as $hsc_result)--}}
                                {{--<option value="{{ $hsc_result->year }}">{{ $hsc_result->year }}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                        {{--<button type="submit" class="ic-btn">Download</button>--}}
                        {{--{!! Form::close() !!}--}}
                    {{--</div>--}}
                </div>
                <div class="col-md-4">
                    <div class="ic-news-card">
                        <div class="ic-title">
                            <h2>{{ $content['latest_news_title'] }}</h2>
                        </div>
                        @include('partials.latest_news')
                    </div>
                    <div class="ic-news-card notice-board">
                        <div class="ic-title">
                            <h2>{{ $content['notice_board_title'] }}</h2>
                        </div>
                        @include('partials.notice_board')
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- class routine /end-->
@endsection

@section('js')
    <script>
        $(document).ready(function () {

            $('#student_id').change(function (e) {
                $('#section_id').html('');
                var student_id=$('#student_id').val();
                $.ajax({
                    type:'GET',
                    url:'/ajax_call/get_student_sections',
                    data:{
                        'student_id':student_id
                    },
                    success:function (response) {
                        var res=JSON.parse(response);
                        var options='<option value="">Select Section</option>';
                        options=options+res.sections;
                        $('#section_id').html(options);
                    }
                })
            });

            $('#show_result').click(function (e) {
                e.preventDefault();
                var student_id = $('#student_id').val();
                var section_id = $('#section_id').val();
                var exam_type = $('#exam_type').val();
                $.ajax({
                    type:'GET',
                    url:'results/show',
                    data:{'student_id':student_id,'section_id':section_id,'exam_type':exam_type},
                    success:function (response) {
                        var res= JSON.parse(response);
                        $('.show_result').hide();
                        $('#success, #failed').html('');
                        $('#'+res.status).html(res.result);
                        $('#'+res.status).show();
                    }
                })
            })
        })
    </script>
@endsection