@extends('partials.main')

@section('content')
    @if(isset($content['header_status']) && $content['header_status'] == 'on')
        <!-- banner -->
        <section class="ic-banner course-requirements" style="background-image: url({{ Storage::url($content['header_image']) }})">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="ic-banner-caption">
                            <h1>{{ $content['header_title'] }}</h1>
                            <div class="ic-breadcrumb">
                                <ul>
                                    <li><a href="/">Home</a></li>
                                    <li><span> > </span></li>
                                    <li><a href="/">Department</a></li>
                                    <li><span> > </span></li>
                                    <li>Entry requirement</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @else
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="ic-banner-caption ic-banner-caption-inverse">
                        <h1>{{ $content['header_title'] }}</h1>
                        <div class="ic-breadcrumb ic-breadcrumb-inverse">
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><span> > </span></li>
                                <li><a href="/">Department</a></li>
                                <li><span> > </span></li>
                                <li>Entry requirement</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- banner /end -->

    <!-- course entry requirements -->
    <section class="ic-course-requirements-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="ic-course-requirements">
                        <h2>{{ $content['page_title'] }}</h2>
                        {!! $content['page_details'] !!}
                    </div>
                    <div class="ic-course-entry table-responsive ic-narrow-screen-tbl">
                        <table class="table">
                            <tr>
                                <th class="ic-s-no ces">Serial No</th>
                                <th>{{ $content['requirement_title'] }}</th>
                            </tr>
                            @php($sl=1)
                            @foreach($page->contents['entry_requirement'] as $requirement)
                                <tr>
                                    <td class="ic-s-no">{{ $sl }}</td>
                                    <td>{{ $requirement }}</td>
                                </tr>
                                @php($sl++)
                            @endforeach
                        </table>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="ic-news-card">
                        <div class="ic-title">
                            <h2>{{ $content['latest_news_title'] }}</h2>
                        </div>
                        @include('partials.latest_news')
                    </div>
                    <div class="ic-news-card notice-board">
                        <div class="ic-title">
                            <h2>{{ $content['notice_board_title'] }}</h2>
                        </div>
                        @include('partials.notice_board')
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- course entry requirements /end-->
@endsection