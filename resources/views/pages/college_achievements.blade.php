@extends('partials.main')

@section('content')

    @if(isset($content['header_status']) && $content['header_status'] == 'on')
        <!-- banner -->
        <section class="ic-banner collage-achivement" style="background-image: url({{ Storage::url($content['header_image']) }})">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="ic-banner-caption">
                            <h1>{{ $content['header_title'] }}</h1>
                            <div class="ic-breadcrumb">
                                <ul>
                                    <li><a href="/">Home</a></li>
                                    <li><span> > </span></li>
                                    <li>collage achivement</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- banner /end -->
    @else
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="ic-banner-caption ic-banner-caption-inverse">
                        <h1>{{ $content['header_title'] }}</h1>
                        <div class="ic-breadcrumb ic-breadcrumb-inverse">
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><span> > </span></li>
                                <li>collage achivement</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- collage achivement -->
    <section class="ic-collage-achivement">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="ic-ca-col">
                        <h2>{{ $content['page_title'] }}</h2>
                        {!! $content['page_details'] !!}
                    </div>
                    @include('partials.all_achievements')
                </div>
                <div class="col-md-4">
                    <div class="ic-news-card">
                        <div class="ic-title">
                            <h2>{{ $content['latest_news_title'] }}</h2>
                        </div>
                        @include('partials.latest_news')
                    </div>
                    <div class="ic-news-card notice-board">
                        <div class="ic-title">
                            <h2> {{ $content['notice_board_title'] }}</h2>
                        </div>
                        @include('partials.notice_board')
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- collage achivement /end-->
@endsection