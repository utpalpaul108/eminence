@extends('partials.main')

@section('content')

    @if(isset($content['header_status']) && $content['header_status'] == 'on')
        <!-- banner -->
        <section class="ic-banner bba-outline" style="background-image: url({{ Storage::url($content['header_image']) }})">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="ic-banner-caption">
                            <h1>{{ $content['header_title'] }}</h1>
                            <div class="ic-breadcrumb">
                                <ul>
                                    <li><a href="/">Home</a></li>
                                    <li><span> > </span></li>
                                    <li><a href="/">Department</a></li>
                                    <li><span> > </span></li>
                                    <li>Course Outline</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- banner /end -->
    @else
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="ic-banner-caption ic-banner-caption-inverse">
                        <h1>{{ $content['header_title'] }}</h1>
                        <div class="ic-breadcrumb ic-breadcrumb-inverse">
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><span> > </span></li>
                                <li><a href="/">Department</a></li>
                                <li><span> > </span></li>
                                <li>Course Outline</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- course outline -->
    <section class="ic-course-outline">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="ic-course-outline">
                        <div class="ic-course-outline-fig">
                            <img class="img-responsive" src="{{ Storage::url('images/bba-course-outline.png') }}" alt="">
                            <div class="ic-course-outline-figcaption">
                                <h2>{{ $content['page_title'] }}</h2>
                                {!! $content['page_details'] !!}
                                {{--<div  id="icView"  style="display: none">--}}
                                    {{--<img class="img-responsive" src="{{ Storage::url($page->contents['download_course_outline']) }}" alt="" style="max-width: 660px">--}}
                                {{--</div>--}}
                                {{--<a href="#icView" class="ic-link ic-btn course-outline" data-fancybox>View Course Outline</a>--}}
{{--                                <a href="{{ action('PageController@download_course_outline',['id'=>$page->id]) }}" class="ic-link ic-btn course-download">Download Course Outline</a>--}}

                                @if($all_courses != '')
                                    <!-- course Outline -->
                                    <div class="ic-class-routine-tbl ic-course-entry  table-responsive ic-narrow-screen-tbl">
                                        <table class="table">
                                            <tr>
                                                <th class="ic-rsn">Serial No</th>
                                                <th><span>{{ $content['course_outline_title'] }}</span></th>
                                                <th></th>
                                                <th></th>
                                            </tr>

                                            @php($sl=1)
                                            @foreach($all_courses as $course)
                                                <tr>
                                                    <td class="ic-s-no">{{ $sl }}</td>
                                                    <td class="ic-course-title">{{ $course->course_title }}</td>
                                                    {!! Form::open(['action'=>'DownloadFileController@download', 'method'=>'GET']) !!}
                                                        <input type="hidden" name="url" value="{{ $course->download_url }}">
                                                        <td class="ic-download-routine text-center">
                                                            <button class="no_button_style" type="submit">Download</button>
                                                        </td>
                                                    {!! Form::close() !!}
                                                    <td></td>

                                                </tr>
                                                @php($sl++)
                                            @endforeach
                                        </table>
                                    </div>
                                    <!-- course Outline /end-->
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="ic-news-card">
                        <div class="ic-title">
                            <h2>{{ $content['latest_news_title'] }}</h2>
                        </div>
                        @include('partials.latest_news')
                    </div>
                    <div class="ic-news-card notice-board">
                        <div class="ic-title">
                            <h2>{{ $content['notice_board_title'] }}</h2>
                        </div>
                        @include('partials.notice_board')
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- course outline /end-->
@endsection