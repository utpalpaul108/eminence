@extends('partials.main')

@section('content')
    @if(isset($content['header_status']) && $content['header_status'] == 'on')
        <!-- banner -->
        <section class="ic-banner aminstrative-staff" style="background-image: url({{ Storage::url($content['header_image']) }})">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="ic-banner-caption">
                            <h1>{{ $content['header_title'] }}</h1>
                            <div class="ic-breadcrumb">
                                <ul>
                                    <li><a href="/">Home</a></li>
                                    <li><span> > </span></li>
                                    <li><a href="/academic_staff">Academic staff</a></li>
                                    <li><span> > </span></li>
                                    <li><a href="/office_staff">Office staff</a></li>
                                    <li><span> > </span></li>
                                    <li>Administrative Staff</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- banner /end -->
    @else
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="ic-banner-caption ic-banner-caption-inverse">
                        <h1>{{ $content['header_title'] }}</h1>
                        <div class="ic-breadcrumb ic-breadcrumb-inverse">
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><span> > </span></li>
                                <li><a href="/academic_staff">Academic staff</a></li>
                                <li><span> > </span></li>
                                <li><a href="/office_staff">Office staff</a></li>
                                <li><span> > </span></li>
                                <li>Administrative Staff</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- adminstrative staff -->
    @include('partials.all_administrative_staff')
    <!-- adminstrative staff /end -->
@endsection