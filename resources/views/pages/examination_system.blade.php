@extends('partials.main')

@section('content')

    @if(isset($content['header_status']) && $content['header_status'] == 'on')
        <!-- banner -->
        <section class="ic-banner exam-system" style="background-image: url({{ Storage::url($content['header_image'] ) }})">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="ic-banner-caption">
                            <h1>{{ $content['header_title'] }}</h1>
                            <div class="ic-breadcrumb">
                                <ul>
                                    <li><a href="/">Home</a></li>
                                    <li><span> > </span></li>
                                    <li>Examination System</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- banner /end -->
    @else
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="ic-banner-caption ic-banner-caption-inverse">
                        <h1>{{ $content['header_title'] }}</h1>
                        <div class="ic-breadcrumb ic-breadcrumb-inverse">
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><span> > </span></li>
                                <li>Examination System</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- examination system -->
    <section class="ic-exam-system">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ic-ex-inint">
                    <p>{{ $content['page_title'] }}</p>
                </div>
                <div class="ic-exam-system-table table-responsive ic-narrow-screen-tbl">
                    <table class="table table-bordered">
                        <tr>
                            <th>Name Of The Exam</th>
                            <th>Exam Duration</th>
                            <th>Exam Marks</th>
                            <th>Class</th>
                        </tr>
                        @foreach($content['examination_system'] as $system)
                            <tr>
                                <td>{{ $system['name'] }}</td>
                                <td>{{ $system['duration'] }}</td>
                                <td>{{ $system['marks'] }}</td>
                                <td>{{ $system['class'] }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <div class="col-xs-12 ic-other-info">
                    {!! $content['page_details'] !!}
                </div>
            </div>
        </div>
    </section>
    <!-- examination system /end -->
@endsection