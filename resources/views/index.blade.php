@extends('partials.main')

@section('content')
    <!-- home slider -->
    @include('partials.slider')
    <!-- home slider /end -->

    <!-- welcome -->
    <section class="ic-welcome">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <figure>
                        <img class="img-responsive" src="{{ Storage::url('images/welcome.jpg') }}" alt="Welcome photo">
                    </figure>
                </div>
                <div class="col-md-6 col-md-offset-1">
                    <div class="ic-welcome-caption">
                        <h2>Welcome to</h2>
                        <h3>Eminence College</h3>
                        <p>Welcome to our Eminence College website. We are one of the prominent, Comprehensive and Co-education College in Bangladesh offering higher secondary curriculum under Board of Intermediate and Secondary Education, Dhaka and BBA, CSE (Professional) under National University. The College is pursuing its vision since 2015 to emerge as the centre of distinction on strategy and development studies to meet the challenges of 21st century. Eminence College endeavors to formulate befitting academic curriculum for the potential policy planners, leaders and thinkers from home and abroad. The College allows freedom of expression following a non-attributable policy that allows for an environment of free and open discussion, exchange of ideas and crystallization of consensus.</p>
                        <p>Eminence College, we are dedicated to providing a quality education program and services in order to help our students attain their full potential academically, socially and physically as well as provide them with the knowledge skills required for them to be valued and contributing members of our society.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- welcome /end -->

    <!-- message and news -->
    <section class="ic-message-n-news">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="ic-single-message">
                        <div class="ic-title">
                            <h2>Chairman's message</h2>
                        </div>
                        <div class="row">
                            <div class="col-xs-5 col-xxs-12">
                                <img class="img-responsive" src="{{ Storage::url('images/chairman.jpg') }}" alt="">
                            </div>
                            <div class="col-xs-7 col-xxs-12">
                                <div class="ic-message">
                                    <span>Group Captain</span>
                                    <span>Abu Zafar Chowdhury, psc (Retd)</span>
                                    <p>Eminence College is an endeavor established recently as a symbol of ideal institution to impart education with morality, ethics and values to the ambitious young generation of the People’s Republic of Bangladesh. I am proud to take such initiative on the context of the need of such institution at this time in the country to inspire and communicate the aim and objectives of the liberation war fought in 1971 against the rulers of Pakistan. </p>
                                    <a class="ic-link" href="chairman_message.php">Read more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ic-single-message">
                        <div class="ic-title">
                            <h2>Principal's Message</h2>
                        </div>
                        <div class="row">
                            <div class="col-xs-5 col-xxs-12">
                                <img class="img-responsive" src="{{ Storage::url('images/principle.png') }}" alt="">
                            </div>
                            <div class="col-xs-7 col-xxs-12">
                                <div class="ic-message">
                                    <span>Group Captain</span>
                                    <span>Abu Zafar Chowdhury, psc (Retd)</span>
                                    <p>Eminence College is an endeavor established recently as a symbol of ideal institution to impart education with morality, ethics and values to the ambitious young generation of the People’s Republic of Bangladesh. I am proud to take such initiative on the context of the need of such institution at this time in the country to inspire and communicate the aim and objectives of the liberation war fought in 1971 against the rulers of Pakistan. </p>
                                    <a class="ic-link" href="principal_message.php">Read more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="ic-news-card">
                        <div class="ic-title">
                            <h2>Latest news</h2>
                        </div>
                        <div class="ic-ln-col">
                            <ul>
                                <li><a href="#">Welcome to our website for the Eminence College in Dhaka, Bangladesh. <br> by admin / 17 Aug 2017</a></li>
                                <li><a href="#">Welcome to our website for the Eminence College in Dhaka, Bangladesh. <br> by admin / 17 Aug 2017</a></li>
                                <li><a href="#">Welcome to our website for the Eminence College in Dhaka, Bangladesh. <br> by admin / 17 Aug 2017</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="ic-news-card notice-board">
                        <div class="ic-title">
                            <h2>Notice Board</h2>
                        </div>
                        <div class="ic-ln-col notice-col">
                            <ul>
                                <li><a href="#">Welcome to our website for the Eminence College in Dhaka, Bangladesh. <br> by admin / 17 Aug 2017</a></li>
                                <li><a href="#">Welcome to our website for the Eminence College in Dhaka, Bangladesh. <br> by admin / 17 Aug 2017</a></li>
                                <li><a href="#">Welcome to our website for the Eminence College in Dhaka, Bangladesh. <br> by admin / 17 Aug 2017</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- message and news /end-->

    <!-- gallery -->
    <section class="ic-gallery">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="ic-title text-center">
                        <h2>Photo Gallery</h2>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="ic-portfolio-nav">
                        <button type="button" data-filter="all">All</button>
                        <button type="button" data-filter=".campus">Capmpus</button>
                        <button type="button" data-filter=".library">Library</button>
                        <button type="button" data-filter=".tour">Tour</button>
                        <button type="button" data-filter=".sports">Sports</button>
                        <button type="button" data-filter=".functions">Function</button>
                    </div>
                    <div class="row ic-portfolio-items">
                        <div class="col-sm-3 ic-xxs-6 col-xxs-12 ic-single-card mix campus">
                            <figure>
                                <img class="img-responsive" src="{{ Storage::url('images/gallery/1.jpg') }}" alt="">
                                <div class="ic-card-overlay">
                                    <a href="{{ Storage::url('images/gallery/largeImage.png') }}" data-fancybox="group" data-caption="Image caption here">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </figure>
                        </div>
                        <div class="col-sm-3 ic-xxs-6 col-xxs-12 ic-single-card mix library">
                            <figure>
                                <img class="img-responsive" src="{{ Storage::url('images/gallery/2.jpg') }}" alt="">
                                <div class="ic-card-overlay">
                                    <a href="{{ Storage::url('images/gallery/largeImage.png') }}" data-fancybox="group" data-caption="Image caption here">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </figure>
                        </div>
                        <div class="col-sm-3 ic-xxs-6 col-xxs-12 ic-single-card mix tour sports">
                            <figure>
                                <img class="img-responsive" src="{{ Storage::url('images/gallery/3.jpg') }}" alt="">
                                <div class="ic-card-overlay">
                                    <a href="{{ Storage::url('images/gallery/largeImage.png') }}" data-fancybox="group" data-caption="Image caption here">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </figure>
                        </div>
                        <div class="col-sm-3 ic-xxs-6 col-xxs-12 ic-single-card mix campus sports functions">
                            <figure>
                                <img class="img-responsive" src="{{ Storage::url('images/gallery/4.jpg') }}" alt="">
                                <div class="ic-card-overlay">
                                    <a href="{{ Storage::url('images/gallery/largeImage.png') }}" data-fancybox="group" data-caption="Image caption here">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </figure>
                        </div>
                        <div class="col-sm-3 ic-xxs-6 col-xxs-12 ic-single-card mix campus">
                            <figure>
                                <img class="img-responsive" src="{{ Storage::url('images/gallery/5.jpg') }}" alt="">
                                <div class="ic-card-overlay">
                                    <a href="{{ Storage::url('images/gallery/largeImage.png') }}" data-fancybox="group" data-caption="Image caption here">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </figure>
                        </div>
                        <div class="col-sm-3 ic-xxs-6 col-xxs-12 ic-single-card mix library">
                            <figure>
                                <img class="img-responsive" src="{{ Storage::url('images/gallery/6.jpg') }}" alt="">
                                <div class="ic-card-overlay">
                                    <a href="{{ Storage::url('images/gallery/largeImage.png') }}" data-fancybox="group" data-caption="Image caption here">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </figure>
                        </div>
                        <div class="col-sm-3 ic-xxs-6 col-xxs-12 ic-single-card mix tour sports">
                            <figure>
                                <img class="img-responsive" src="{{ Storage::url('images/gallery/7.jpg') }}" alt="">
                                <div class="ic-card-overlay">
                                    <a href="{{ Storage::url('images/gallery/largeImage.png') }}" data-fancybox="group" data-caption="Image caption here">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </figure>
                        </div>
                        <div class="col-sm-3 ic-xxs-6 col-xxs-12 ic-single-card mix campus sports functions">
                            <figure>
                                <img class="img-responsive" src="{{ Storage::url('images/gallery/8.jpg') }}" alt="">
                                <div class="ic-card-overlay">
                                    <a href="{{ Storage::url('images/gallery/largeImage.png') }}" data-fancybox="group" data-caption="Image caption here">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </figure>
                        </div>
                        <div class="col-sm-3 ic-xxs-6 col-xxs-12 ic-single-card mix campus">
                            <figure>
                                <img class="img-responsive" src="{{ Storage::url('images/gallery/9.jpg') }}" alt="">
                                <div class="ic-card-overlay">
                                    <a href="{{ Storage::url('images/gallery/largeImage.png') }}" data-fancybox="group" data-caption="Image caption here">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </figure>
                        </div>
                        <div class="col-sm-3 ic-xxs-6 col-xxs-12 ic-single-card mix library">
                            <figure>
                                <img class="img-responsive" src="{{ Storage::url('images/students.png') }}" alt="">
                                <div class="ic-card-overlay">
                                    <a href="{{ Storage::url('images/gallery/largeImage.png') }}" data-fancybox="group" data-caption="Image caption here">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </figure>
                        </div>
                        <div class="col-sm-3 ic-xxs-6 col-xxs-12 ic-single-card mix tour sports">
                            <figure>
                                <img class="img-responsive" src="{{ Storage::url('images/gallery/1.jpg') }}" alt="">
                                <div class="ic-card-overlay">
                                    <a href="{{ Storage::url('images/gallery/largeImage.png') }}" data-fancybox="group" data-caption="Image caption here">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </figure>
                        </div>
                        <div class="col-sm-3 ic-xxs-6 col-xxs-12 ic-single-card mix campus sports functions">
                            <figure>
                                <img class="img-responsive" src="{{ Storage::url('images/gallery/2.jpg') }}" alt="">
                                <div class="ic-card-overlay">
                                    <a href="{{ Storage::url('images/gallery/largeImage.png') }}" data-fancybox="group" data-caption="Image caption here">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- gallery /end -->
@endsection