@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Study Groups
                <small>All Study Groups</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a href="#">Dashboard</a>
                </li>
                <li class="active">Add Study Group</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        @can('add_study_group')
                            <div class="box-header">
                                <div class="pull-left">
                                    <a id="add-button" title="Add New Study Group" class="btn btn-success" href="{{ action('StudyGroupController@create') }}"><i class="fa fa-plus-circle"></i> Add Study Group</a>
                                </div>
                            </div>
                        @endcan
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Group Name</th>
                                    <th class="text-center">About Group</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($all_study_groups->currentPage()-1)*$all_study_groups->perPage())+1)
                                @foreach($all_study_groups as $study_group)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $study_group->group_name }}</td>
                                        <td>{!! str_limit($study_group->about_group,150,'(...)') !!}</td>
                                        <td>
                                            @can('edit_study_group')
                                                <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('StudyGroupController@edit',['id'=>$study_group->id]) }}">
                                                    Edit
                                                </a>&nbsp;
                                            @endcan
                                            @can('delete_study_group')
                                                {!! Form::open(['action'=>['StudyGroupController@destroy',$study_group->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                                                    <button title="Delete" class="btn btn-xs btn-danger delete-row" onclick="return confirm('Are You Sure ?')">
                                                        Delete
                                                    </button>
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $all_study_groups->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection