@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add New Exam Type
            </h1>

            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Exam</a></li>
                <li class="active">Add New Exam Type</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>'ResultGradeController@store']) !!}
                    <div class="col-xs-9">
                        @if(session()->has('error'))
                            <div class="alert alert-danger">
                                {{ $error }}
                            </div>
                        @endif
                        <div class="box">
                            <!-- form start -->
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="mark_range_from">Mark Range From</label>
                                    <input type="number" name="mark_range_from" value="{{ old('mark_range_from') }}" placeholder="Mark Range From" id="mark_range_from" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="mark_range_to">Mark Range To</label>
                                    <input type="number" name="mark_range_to" value="{{ old('mark_range_to') }}" placeholder="Mark Range To" id="mark_range_to" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="gpa">GPA</label>
                                    <input type="text" name="gpa" value="{{ old('gpa') }}" placeholder="Enter GPA" id="gpa" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="grade">Grade</label>
                                    <input type="text" name="grade" value="{{ old('grade') }}" placeholder="Enter Grade" id="grade" class="form-control" required>
                                </div>

                            </div>
                            <!-- /.box-body -->
                            @can('add_result_grade')
                                <div class="box-footer">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            @endcan
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
@endsection