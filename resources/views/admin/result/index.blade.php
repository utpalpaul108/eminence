@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Student Result
                <small>All Student Result</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a href="#">Dashboard</a>
                </li>
                <li class="active">All Student Result</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <div class="pull-left">
                                <a id="add-button" title="Add New Result" class="btn btn-success" href="{{ action('ResultController@create') }}"><i class="fa fa-plus-circle"></i> Add Result</a>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Student ID</th>
                                    <th class="text-center">Department</th>
                                    <th class="text-center">Semester</th>
                                    <th class="text-center">Year</th>
                                    <th class="text-center">Result</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($all_results->currentPage()-1)*$all_results->perPage())+1)
                                @foreach($all_results as $result)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $result->student_id }}</td>
                                        <td>{{ $result->department['name'] }}</td>
                                        <td>{{ $result->semester }}</td>
                                        <td>{{ $result->year }}</td>
                                        <td>{{ $result->result }}</td>
                                        <td>
                                            <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('ResultController@edit',['id'=>$result->id]) }}">
                                                Edit
                                            </a>&nbsp;
                                            {!! Form::open(['action'=>['ResultController@destroy',$result->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                                                <button title="Delete" class="btn btn-xs btn-danger delete-row" onclick="return confirm('Are You Sure')">
                                                    Delete
                                                </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $all_results->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection