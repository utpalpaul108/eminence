@extends('admin.partials.main')

@section('style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add New HSC Result
            </h1>
            @if(session()->has('error'))
                <br>
                <span class="text-danger">{{ session('error') }}</span>
            @endif
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Result</a></li>
                <li class="active">Add New HSC Result</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>'HscResultController@store','files'=>true]) !!}
                <div class="col-xs-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="year">Year</label>
                                <input type="text" name="year" value="{{ old('year') }}" placeholder="Enter Result Year" id="year" class="date-own form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="download_url">Upload Result</label>
                                <input type="file" name="download_url" id="download_url" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="about">About Result</label>
                                <textarea name="about" id="about" rows="5" class="editor form-control" >{{ old('about') }}</textarea>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
    <script type="text/javascript">
        $('.date-own').datepicker({
            minViewMode: 2,
            format: 'yyyy'
        });
    </script>
@endsection