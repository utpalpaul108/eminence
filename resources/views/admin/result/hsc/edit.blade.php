@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit HSC Result
            </h1>
            @if(session()->has('error'))
                <br>
                <span class="text-danger">{{ session('error') }}</span>
            @endif
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Result</a></li>
                <li class="active">Edit HSC Result</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>['HscResultController@update',$hsc_result->id],'method'=>'PUT','files'=>true]) !!}
                <div class="col-xs-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="year">Year</label>
                                <input type="text" name="year" value="{{ $hsc_result->year }}" placeholder="Enter Result Year" id="year" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="download_url">Upload Result</label>
                                <input type="file" name="download_url" id="download_url" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="about">About Result</label>
                                <textarea name="about" id="about" rows="5" class="editor form-control" >{{ $hsc_result->about }}</textarea>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
@endsection