@extends('admin.partials.main')

@section('style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add New Result
            </h1>
            @if(session()->has('error'))
                <br>
                <span class="text-danger">{{ session('error') }}</span>
            @endif
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Result</a></li>
                <li class="active">Add New Result</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-9">
                    @if(session()->has('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @elseif(session()->has('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ success('error') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                {!! Form::open(['action'=>'ResultController@store', 'files'=>true]) !!}
                <div class="col-xs-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="year">Department</label>
                                <select class="form-control" name="department_id" id="department_id" required>
                                    <option value="">-- Select Department --</option>
                                    @foreach($all_departments as $department)
                                        <option value="{{ $department->id }}">{{ $department->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="group_id">Group</label>
                                <select class="form-control" id="group_id" name="group_id" required></select>
                            </div>
                            <div class="form-group">
                                <label for="section_id">Section</label>
                                <select class="form-control" name="section_id" id="section_id" required></select>
                            </div>
                            <div class="form-group">
                                <label for="session_id">Session</label>
                                <select class="form-control" name="session_id" id="session_id" required>
                                    <option value="">-- Select Session --</option>
                                    @foreach($all_sessions as $session)
                                        <option value="{{ $session->id }}">{{ $session->session_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exam_type_id">Exam Type</label>
                                <select class="form-control" name="exam_type_id" id="exam_type_id" required>
                                    <option value="">-- Select Exam Type --</option>
                                    @foreach($exam_types as $exam_type)
                                        <option value="{{ $exam_type->id }}">{{ $exam_type->exam_type }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="subject_id">Subject</label>
                                <select class="form-control" name="subject_id" id="subject_id" required></select>
                            </div>

                            <div class="form-group">
                                <label for="results">Upload Excel File</label>
                                <input type="file" id="results" name="results" required>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
    <script>
        $(document).ready(function () {
            $('#department_id').change(function (e) {
                $('#section_id').html('');
                $('#exam_type_id').val('');
                $('#subject_id').html('');
                var department=$('#department_id').val();
                $.ajax({
                    type:'GET',
                    url:'/ajax_call/department_wise_data',
                    data:{
                        'department_id':department
                    },
                    success:function (response) {
                        var res=JSON.parse(response);
                        $('#group_id').html(res.groups);
                    }
                })
            });

            $('#group_id').change(function (e) {
                $('#exam_type_id').val('');
                $('#subject_id').html('');

                var department=$('#department_id').val();
                var group_id=$('#group_id').val();

                $.ajax({
                    type:'GET',
                    url:'/admin/group_wise_data',
                    data:{
                        department_id:department,
                        group_id:group_id,
                    },
                    success:function (response) {
                        var res=JSON.parse(response);
                        $('#section_id').html(res.sections);
                    }
                })
            });

            $('#section_id').change(function (e) {
                $('#exam_type_id').val('');
                $('#subject_id').html('');
            });

            $('#exam_type_id').change(function (e) {
                $('#subject_id').html('');
                var department=$('#department_id').val();
                var group_id=$('#group_id').val();
                var section_id=$('#section_id').val();
                var exam_type_id=$('#exam_type_id').val();

                $.ajax({
                    type:'GET',
                    url:'/ajax_call/exam_wise_subject',
                    data:{
                        department_id:department,
                        group_id:group_id,
                        section_id:section_id,
                        exam_type_id:exam_type_id,
                    },
                    success:function (response) {
                        var res=JSON.parse(response);
                        $('#subject_id').html(res.subjects);
                    }
                })
            });


            // $('#exam_type_id').change(function (e) {
            //     var department=$('#department_id').val();
            //     var group_id=$('#group_id').val();
            //     var section_id=$('#section_id').val();
            //     var subject_id=$('#subject_id').val();
            //     var exam_type_id=$('#exam_type_id').val();
            //
            //     $.ajax({
            //         type:'GET',
            //         url:'/ajax_call/get_exams',
            //         data:{
            //             department_id:department,
            //             group_id:group_id,
            //             section_id:section_id,
            //             subject_id:subject_id,
            //             exam_type_id:exam_type_id,
            //         },
            //         success:function (response) {
            //             var res=JSON.parse(response);
            //             $('#exam_id').html(res.exams);
            //         }
            //     })
            // });

        })
    </script>
@endsection