@extends('admin.partials.main')

@section('style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Result
            </h1>
            @if(session()->has('error'))
                <br>
                <span class="text-danger">{{ session('error') }}</span>
            @endif
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Result</a></li>
                <li class="active">Edit Result</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>['ResultController@update',$result->id],'method'=>'PUT']) !!}
                <div class="col-xs-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="year">Department</label>
                                <select class="form-control" name="department_id">
                                    @foreach($all_departments as $department)
                                        <option value="{{ $department->id }}" @if($result->department_id == $department->id){{ 'selected' }} @endif >{{ $department->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="semester">Semester</label>
                                {{--<select class="" name="semester" id="semester">--}}
                                    {{--<option value="1"></option>--}}
                                    {{--<option value="2"></option>--}}
                                    {{--<option value="3"></option>--}}
                                    {{--<option value="4"></option>--}}
                                {{--</select>--}}
                                {!! Form::select('semester', ['1' => '1st Semester', '2' => '2nd Semester', '3' => '3rd Semester', '4' => '4th Semester'], $result->semester, ['class' => 'form-control', 'id'=>'semester']) !!}
                            </div>
                            <div class="form-group">
                                <label for="student_id">Student ID</label>
                                <input type="text" name="student_id" value="{{ $result->student_id }}" placeholder="Enter Student ID" id="student_id" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="year">Year</label>
                                <input type="text" name="year" value="{{ $result->year }}" placeholder="Enter Result Year" id="year" class="date-own form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="result">Result</label>
                                <input type="text" name="result" value="{{ $result->result }}" placeholder="Enter Result" id="result" class="form-control" required>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
    <script type="text/javascript">
        $('.date-own').datepicker({
            minViewMode: 2,
            format: 'yyyy'
        });
    </script>
@endsection