@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Result
                <small>Result Report</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a href="#">Dashboard</a>
                </li>
                <li class="active">Result Report</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    <div class="box">
                        <div class="box-header">
                            <div class="pull-left">
                                <button type="button" class="btn btn-default" onclick="window.history.back()"><i class="fa fa-caret-left"></i> Back</button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Student Name</th>
                                    <th class="text-center">Student ID</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{--@php($sl=(($all_students->currentPage()-1)*$all_students->perPage())+1)--}}
                                @php($sl=1)
                                @foreach($all_students as $student)
                                        <tr class="text-center">
                                            <td>{{ $sl }}</td>
                                            <td>{{ $student->name }}</td>
                                            <td>{{ $student->student_id }}</td>
                                            <td>&nbsp;
                                                <a title="Print" class="btn btn-xs btn-success edit-row" href="{{ action('ResultReportController@print_report',['student_id'=>$student->id,'section_id'=>$report_info['section_id'],'exam_type'=>$report_info['exam_type']]) }}">
                                                    View
                                                </a>&nbsp;
                                                <a title="Download" class="btn btn-xs btn-default edit-row" href="{{ action('ResultReportController@download_report',['student_id'=>$student->id,'section_id'=>$report_info['section_id'],'exam_type'=>$report_info['exam_type']]) }}">
                                                    Download
                                                </a>&nbsp;
                                            </td>
                                        </tr>
                                        @php($sl++)
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        {{--<div class="box-footer clearfix">--}}
                            {{--{{ $all_students->links() }}--}}
                        {{--</div>--}}
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection