<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <title>Eminence College | Report</title>

    <style>
        @media print {
            /* style sheet for print goes here */
            .print-button {
                display: none;
            }
            .print-top {
                margin-top: 0px;
            }
            @page{
                margin-top: 50px;
            }
        }
        table{
            font-size: 14px;
        }

        .table-bordered thead td, .table-bordered thead th {
            border-bottom-width: 1px !important;
        }
        .table thead th {
            border-bottom: 1px solid #000000 !important;
        }

        .table-bordered td, .table-bordered th {
            border: 1px solid #000000 !important;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row mt-3 print-button">
        <div class="col-md-12">
            <button type="button" class="btn btn-primary float-right ml-2" id="printPreview">Print Report</button>
            <button type="button" class="btn btn-default float-right" onclick="window.history.back()">Back</button>
        </div>
    </div>
    <div class="row mt-4 print-top">
        <div class="col-md-2">
            <img src="{{ asset('/images/eminence_college_logo.png') }}" class="img-fluid">
        </div>
        <div class="col-md-6">
            <h2 class="text-center font-weight-bold">
                Eminence College<br>
                Uttara,  Dhaka-1230<br>
                Report Card
            </h2>
        </div>
        <div class="col-md-4">
            <table class="table table-sm table-bordered" style="font-size: 10px">
                <thead>
                <tr class="text-center">
                    <th colspan="3">Grading Policy</th>
                </tr>
                <tr class="text-center">
                    <th scope="col" class="align-middle">Letter Grade</th>
                    <th scope="col" class="align-middle">Class Interval<br>(%)</th>
                    <th scope="col" class="align-middle">Grade Point</th>
                </tr>
                </thead>
                <tbody>
                @foreach($grading_systems as $grading_system)
                    <tr class="text-center">
                        <td>{{ $grading_system->grade }}</td>
                        <td>{{ $grading_system->mark_range_from .' - ' .$grading_system->mark_range_to }}</td>
                        <td>{{ $grading_system->gpa }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row mt-1">
        <div class="col-md-12">
            <div>
                <table class="table table-bordered">
                    <tr>
                        <th colspan="2">Student Name</th>
                        <td colspan="4">{{ $student_info->name }}</td>
                    </tr>
                    <tr>
                        <th colspan="2">Father's / Mother's Name</th>
                        <td colspan="4">@if(isset($student_info->student_info['father_name'])){{ $student_info->student_info['father_name'] }} @endif</td>
                    </tr>
                    <tr>
                        <th>Class</th>
                        <td>{{ $student_info->department['name'] }}</td>
                        <th>Group</th>
                        <td>{{ $student_info->group['group_name'] }}</td>
                        <th>Section</th>
                        <td>{{ $student_info->section['section_name'] }}</td>
                    </tr>
                    <tr>
                        <th>Roll</th>
                        <td>{{ $student_info->student_id }}</td>
                        <th>Academic Year</th>
                        <td colspan="2">{{ $student_info->session['session_name'] }}</td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-md-12">
            <div>
                <table class="table table-sm table-bordered">
                    <thead>
                        <tr class="text-center">
                            <th class="align-middle" rowspan="2">Subject</th>
                            <th class="align-middle" colspan="{{ count($exam_categories) }}">{{ $exam_type->exam_type }}</th>
                            <th class="align-middle" rowspan="2">Total Marks(100)</th>
                            <th class="align-middle" rowspan="2">CTM(75)</th>
                            <th class="align-middle" rowspan="2">TE(25)</th>
                            <th class="align-middle" rowspan="2">FM=CTM+TE(100)</th>
                            <th class="align-middle" rowspan="2">Highest Mark</th>
                            <th class="align-middle" rowspan="2">Grade</th>
                            <th class="align-middle" rowspan="2">GPA</th>
                        </tr>
                        <tr>
                            @foreach($exam_categories as $exam_id=>$exam_name)
                                <th class="text-center">{{ $exam_name }}</th>
                            @endforeach
                        </tr>
                    </thead>

                    <tbody>
                        @php
                            $is_failed=0;
                        @endphp
                        @foreach($all_subjects as $subject )
                            <tr  class="text-center">
                                <td class="align-middle">{{ $subject['subject_name'] }}</td>
                                @foreach($exam_categories as $exam_id=>$exam_name)
                                    <td class="align-middle" style="@if(isset($subject['result_color'][$exam_id])){{ 'color: ' .$subject['result_color'][$exam_id] }} @endif ">@if(isset($subject['marks'][$exam_id])){{ $subject['marks'][$exam_id] }} @else{{ '0' }} @endif</td>
                                @endforeach
                                <td class="align-middle">{{ $subject['total_marks'] }}</td>
                                <td class="align-middle">{{ $subject['converted_mark'] }}</td>
                                <td class="align-middle">@if($subject['tutorial_mark']>0){{ $subject['tutorial_mark'] }} @else {{ '0' }} @endif</td>
                                <td class="align-middle" style="@if( !isset($subject['result_color']) && $subject['gpa'] <= 0){{ 'color: red' }} @endif ">{{ $subject['full_mark'] }}</td>
                                <td class="align-middle">{{ $subject['height_mark'] }}</td>
                                <td class="align-middle" style="@if(isset($subject['color'])){{ 'color: ' .$subject['color'] }} @endif " >{{ $subject['grade'] }}</td>
                                <td class="align-middle">{{ $subject['gpa'] }}</td>
                            </tr>
                            @php
                                if ($subject['gpa'] == 0){
                                    $is_failed++;
                                }
                            @endphp
                        @endforeach
                        <tr  class="text-center">
                            <td class="align-middle">{{ $fourth_subject['subject_name'] }}</td>
                            @foreach($exam_categories as $exam_id=>$exam_name)
                                <td class="align-middle" style="@if(isset($fourth_subject['result_color'][$exam_id])){{ 'color: ' .$fourth_subject['result_color'][$exam_id] }} @endif">@if(isset($fourth_subject['marks'][$exam_id])){{ $fourth_subject['marks'][$exam_id] }} @else{{ '0' }} @endif</td>
                            @endforeach
                            <td class="align-middle">{{ $fourth_subject['total_marks'] }}</td>
                            <td class="align-middle">{{ $fourth_subject['converted_mark'] }}</td>
                            <td class="align-middle">@if($fourth_subject['tutorial_mark']>0){{ $fourth_subject['tutorial_mark'] }} @else {{ '0' }} @endif</td>
                            <td class="align-middle"  style="@if( !isset($fourth_subject['result_color']) && $fourth_subject['gpa'] <= 0){{ 'color: red' }} @endif ">{{ $fourth_subject['full_mark'] }}</td>
                            <td class="align-middle">{{ $fourth_subject['height_mark'] }}</td>
                            <td class="align-middle" style="@if($fourth_subject['gpa'] <=0){{ 'color: red' }} @endif ">{{ $fourth_subject['grade'] }}</td>
                            <td class="align-middle">{{ $fourth_subject['gpa'] }}</td>
                        </tr>
                        <tr class="text-center">
                            <th colspan="{{ count($exam_categories) +1 }}">Total</th>
                            <td>{{ $total_marks }}</td>
                            <td>{{ $converted_marks }}</td>
                            <td></td>
                            <td>{{ $full_marks }}</td>
                            <td></td>
                            <td style="@if($is_failed > 0){{ 'color: red' }} @endif">@if($is_failed == 0){{ $grade }} @else{{ 'F' }} @endif</td>
                            <td>@if($is_failed == 0){{ $gpa }} @else{{ '0' }} @endif</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-md-12">
            <div>
                <table class="table table-bordered">
                    <thead>
                        <tr class="text-center">
                            <th>Section Position</th>
                            <th>Working Days</th>
                            <th>Present</th>
                            <th>Late Present</th>
                            <th>Absent</th>
                            <th>% P</th>
                            <th>Guardian Sign</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $total_attendance = 0;
                        @endphp

                        <tr class="text-center">
                            <th>&nbsp;</th>
                            <th>{{ array_sum($student_attendance) }}</th>
                            <th>@if(isset($student_attendance['P'])){{ $student_attendance['P'] }} @php  $total_attendance= $total_attendance+$student_attendance['P'] @endphp @else{{ '0' }} @endif</th>
                            <th>@if(isset($student_attendance['L'] )){{ $student_attendance['L'] }} @php $total_attendance= $total_attendance+$student_attendance['L'] @endphp @else{{ '0' }} @endif</th>
                            <th>@if(isset($student_attendance['A'])){{ $student_attendance['A'] }} @php $total_attendance= $total_attendance+$student_attendance['A'] @endphp @else{{ '0' }} @endif</th>
                            <th>
                                @if (isset($student_attendance['P']) && count($student_attendance['P']) >0)
                                    {{ round(($student_attendance['P']/$total_attendance)*100) }}
                                @else
                                    {{ '0' }}
                                @endif
                            </th>
                            <th>&nbsp;</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-md-12">
            <div>
                <table class="table table-bordered">
                    <tr>
                        <th colspan="4">Discipline</th>
                        <td colspan="8">&#9744; Excellent &nbsp;&nbsp; &#9744; Good &nbsp;&nbsp; &#9744; Average &nbsp;&nbsp; &#9744; Below Average &nbsp;&nbsp; &#9744; Not Satisfactory</td>
                    </tr>
                    <tr>
                        <th colspan="4">Punctuality</th>
                        <td colspan="8">&#9744; Excellent &nbsp;&nbsp; &#9744; Good &nbsp;&nbsp; &#9744; Average &nbsp;&nbsp; &#9744; Below Average &nbsp;&nbsp; &#9744; Not Satisfactory</td>
                    </tr>
                    <tr>
                        <th colspan="4">Cleanliness</th>
                        <td colspan="8">&#9744; Excellent &nbsp;&nbsp; &#9744; Good &nbsp;&nbsp; &#9744; Average &nbsp;&nbsp; &#9744; Below Average &nbsp;&nbsp; &#9744; Not Satisfactory</td>
                    </tr>
                    <tr>
                        <th colspan="4">Class Teacher’s Comments</th>
                        <td colspan="8"></td>
                    </tr>
                    <tr>
                        <th colspan="4">Principal’s Comments</th>
                        <td colspan="8"></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-md-6">
            <span><b>-----------------</b></span><br>
            <span><b>Class Teacher</b></span>
        </div>
        <div class="col-md-6">
            <span class="float-right"><b>-------------</b></span><br>
            <span class="float-right"><b>Principal</b></span>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-md-12">
            <div>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <b>
                                CQ= Creative Question<br>
                                MCQ= Multiple Choice Question
                            </b>
                        </td>
                        <td>
                            <b>
                                Prac= Practical<br>
                                TE= Tutorial Exam
                            </b>
                        </td>
                        <td>
                            <b>
                                CTM= Converted Total Marks<br>
                                FM= Full Marks
                            </b>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $('#printPreview').click(function () {
            window.print();
        });
    })
</script>
</body>
</html>