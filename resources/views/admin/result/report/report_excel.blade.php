<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('ic_admin/css/custom.css') }}">
    <title>Eminence College</title>
    <style>
        table {
            width: 100%;
        }

        /*thead, tbody, tr, td, th { display: block; }*/

        tr:after {
            content: ' ';
            display: block;
            visibility: hidden;
            clear: both;
        }

        thead th {
            height: 30px;

            /*text-align: left;*/
        }

        tbody {
            height: 120px;
            overflow-y: auto;
        }

        thead {
            /* fallback */
        }


        /*tbody td, thead th {*/
            /*width: 19.2%;*/
            /*float: left;*/
        /*}*/
    </style>
</head>
<body>
<div class="container">
    <div class="row mt-5">
        <div class="col-md-10 mt-3">
            <h1 class="text-center">Eminence College</h1>
            <h5 class="text-center">Combined Result Report</h5>
        </div>
        <div class="col-md-2 mt-5 text-right">
            <a class="btn btn-primary" href="{{ action('ResultReportController@download_excel',['department_id'=>$input_data['department_id'],'group_id'=>$input_data['group_id'],'section_id'=>$input_data['section_id'],'exam_type'=>$input_data['exam_type'],'session_id'=>$input_data['session_id'] ]) }}" role="button">Export Excel</a>
        </div>
        <div class="table-responsive col-md-12 mt-5">
            <table class="table table-sm table-bordered">
                <thead>
                    <tr class="text-center">
                        <th rowspan="3" class="align-middle">Roll</th>
                        <th rowspan="3" class="align-middle">Students Name</th>
                        @foreach($all_subjects as $subject)
                            <th colspan="{{ count($subject['exams']) + 3 }}" class="align-middle">{{ $subject['subject_name'] }}</th>
                        @endforeach
                        <th rowspan="2" class="align-middle">4th Subject Name</th>
                        <th class="align-middle">Total Marks</th>
                        <th rowspan="3" class="align-middle">Remarks</th>
                    </tr>
                    <tr>
                        @foreach($all_subjects as $subject)
                            <th rowspan="2" class="align-middle rotate">Tutorial</th>
                            @if($subject['exams'] != null)
                                @foreach($subject['exams'] as $exam)
                                    <th rowspan="2" class="align-middle">{{ $exam['exam_category']['exam_name'] }}</th>
                                @endforeach
                            @endif
                            <th>TM</th>
                            <th>FM</th>
                        @endforeach
                        <th rowspan="2" class="align-middle">GPA</th>
                    </tr>

                    <tr>
                        @foreach($all_subjects as $subject)
                            <th>CTM</th>
                            <th>HM</th>
                        @endforeach

                        <th>GPA</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($all_students as $student)
                        <tr>
                            <td rowspan="2" class="text-center align-middle">{{ $student['student_id'] }}</td>
                            <td rowspan="2" class="text-center align-middle">{{ $student['name'] }}</td>

                            @foreach($all_subjects as $subject)
                                <td rowspan="2" class="text-center align-middle">{{ $student['marks'][$subject['id']]['tutorial'] }}</td>
                                @if($subject['exams'] != null)
                                    @foreach($subject['exams'] as $exam)
                                        <td rowspan="2" class="text-center align-middle" style="@if(isset($student['result_color'][$subject['id']][$exam['id']])){{ 'color: ' .$student['result_color'][$subject['id']][$exam['id']] }} @endif ">@if(isset($student['marks'][$subject['id']][$exam['id']])){{ $student['marks'][$subject['id']][$exam['id']] }} @else {{ '0' }} @endif</td>
                                    @endforeach
                                @endif
                                <td class="text-center align-middle">{{ $student['marks'][$subject['id']]['tm'] }}</td>
                                <td class="text-center align-middle" style="@if(!isset($student['result_color'][$subject['id']]) && $student['marks'][$subject['id']]['gpa'] <=0){{ 'color:red' }} @endif">{{ $student['marks'][$subject['id']]['fm'] }}</td>
                            @endforeach

                            <td class="text-center align-middle">{{ $student['fourth_subject'] }}</td>
                            <td class="text-center align-middle">{{ $student['total_marks'] }}</td>

                            <td rowspan="2" class="text-center align-middle"></td>
                        </tr>
                        <tr>
                            @foreach($all_subjects as $subject)
                                <td class="text-center align-middle">{{ $student['marks'][$subject['id']]['ctm'] }}</td>
                                <td class="text-center align-middle">{{ $subject['height_mark'] }}</td>
                            @endforeach

                            <td class="text-center align-middle">{{ $student['fourth_subject_gpa'] }}</td>
                            <td class="text-center align-middle" style="@if($student['gpa']<=0){{ 'color:red' }} @endif">{{ $student['gpa'] }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>