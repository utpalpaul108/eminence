@php
    $file_name='Combined_Result_'.date('H_m_s_').'.xls';
    header( "Content-Type: application/vnd.ms-excel" );
    header( "Content-disposition: attachment; filename=$file_name" );
@endphp

<html>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    {{ Html::style(asset('ic_admin/css/excel_report.css')) }}

    <div>
        <h1 style="text-align: center">Eminence College<br><span style="font-size: 15px">Combined Result Report</span></h1>
    </div>

    <table border=1>
    <thead>
        <tr>
            <th rowspan="3" style="text-align: center; vertical-align: middle;">Roll</th>
            <th rowspan="3" style="text-align: center">Students Name</th>
            @foreach($all_subjects as $subject)
                <th colspan="{{ count($subject['exams']) + 3 }}" style="text-align: center">{{ $subject['subject_name'] }}</th>
            @endforeach
            <th rowspan="2" style="text-align: center; vertical-align: middle">4th Subject Name</th>
            <th style="text-align: center; vertical-align: middle">Total Marks</th>
            <th rowspan="3" style="text-align: center; vertical-align: middle">Remarks</th>
        </tr>
        <tr>
            @foreach($all_subjects as $subject)
                <th rowspan="2" style="text-align: center; vert-align: middle;" class="rotate_text">Tutorial</th>
                @if($subject['exams'] != null)
                    @foreach($subject['exams'] as $exam)
                        <th rowspan="2" style="text-align: center; vertical-align: middle">{{ $exam['exam_category']['exam_name'] }}</th>
                    @endforeach
                @endif
                <th style="text-align: center; vertical-align: middle">TM</th>
                <th style="text-align: center; vertical-align: middle">FM</th>
            @endforeach
            <th rowspan="2" style="text-align: center; vertical-align: middle">GPA</th>
        </tr>

        <tr>
            @foreach($all_subjects as $subject)
                <th style="text-align: center; vertical-align: middle">CTM</th>
                <th style="text-align: center; vertical-align: middle">HM</th>
            @endforeach

            <th style="text-align: center; vertical-align: middle">GPA</th>
        </tr>
    </thead>
    <tbody>
        @foreach($all_students as $student)
            <tr>
                <td rowspan="2" style="text-align: center; vertical-align: middle">{{ $student['student_id'] }}</td>
                <td rowspan="2" style="text-align: center; vertical-align: middle">{{ $student['name'] }}</td>

                @foreach($all_subjects as $subject)
                    <td rowspan="2" style="text-align: center; vertical-align: middle">{{ $student['marks'][$subject['id']]['tutorial'] }}</td>
                    @if($subject['exams'] != null)
                        @foreach($subject['exams'] as $exam)
                            <td rowspan="2" style="text-align: center; vertical-align: middle; @if(isset($student['result_color'][$subject['id']][$exam['id']])){{ 'color: ' .$student['result_color'][$subject['id']][$exam['id']] }} @endif ">@if(isset($student['marks'][$subject['id']][$exam['id']])){{ $student['marks'][$subject['id']][$exam['id']] }} @else {{ '0' }} @endif</td>
                        @endforeach
                    @endif
                    <td style="text-align: center;">{{ $student['marks'][$subject['id']]['tm'] }}</td>
                    <td style="text-align: center; @if(!isset($student['result_color'][$subject['id']]) && $student['marks'][$subject['id']]['gpa'] <=0){{ 'color:red' }} @endif">{{ $student['marks'][$subject['id']]['fm'] }}</td>
                @endforeach

                <td style="text-align: center; vertical-align: middle">{{ $student['fourth_subject'] }}</td>
                <td style="text-align: center; vertical-align: middle">{{ $student['total_marks'] }}</td>

                <td rowspan="2"></td>
            </tr>
            <tr>
                @foreach($all_subjects as $subject)
                    <td style="text-align: center; vertical-align: middle">{{ $student['marks'][$subject['id']]['ctm'] }}</td>
                    <td style="text-align: center; vertical-align: middle">{{ $subject['height_mark'] }}</td>
                @endforeach

                <td style="text-align: center; vertical-align: middle">{{ $student['fourth_subject_gpa'] }}</td>
                <td style="text-align: center; vertical-align: middle; @if($student['gpa']<=0){{ 'color:red' }} @endif">{{ $student['gpa'] }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
</html>