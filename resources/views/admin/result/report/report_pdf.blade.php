<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            text-align: center;
        }
        table{
            font-size: 14px;
        }
        th, td {
            padding: 3px;
        }
    </style>
</head>
<body>
    <div style="">
        <div style="width: 33%;display: inline-block;" >
            <img src="{{ $_SERVER['DOCUMENT_ROOT'].'/images/eminence_college_logo.png' }}" style="width: 120px; height: 120px">
        </div>
        <div style="width: 33%; display: inline-block;">
            <h3 style="text-align: center; font-weight: bold">
                Eminence College<br>
                Uttara,  Dhaka-1230<br>
                Report Card
            </h3>
        </div>
        <div style="width: 33%;display: inline-block;">
            <table style="font-size: 11px">
                <thead>
                    <tr>
                        <th colspan="3">Grading Policy</th>
                    </tr>
                    <tr class="text-center">
                        <th scope="col">Letter Grade</th>
                        <th scope="col">Class Interval<br>(%)</th>
                        <th scope="col">Grade Point</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($grading_systems as $grading_system)
                        <tr class="text-center">
                            <td>{{ $grading_system->grade }}</td>
                            <td>{{ $grading_system->mark_range_from .' - ' .$grading_system->mark_range_to }}</td>
                            <td>{{ $grading_system->gpa }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div style="margin-top: -80px">
        <table>
            <tr>
                <th colspan="2">Student Name</th>
                <td colspan="4">{{ $student_info->name }}</td>
            </tr>
            <tr>
                <th colspan="2">Father's / Mother's Name</th>
                <td colspan="4">@if(isset($student_info->student_info['father_name'])){{ $student_info->student_info['father_name'] }} @endif</td>
            </tr>
            <tr>
                <th>Class</th>
                <td>{{ $student_info->department['name'] }}</td>
                <th>Group</th>
                <td>{{ $student_info->group['group_name'] }}</td>
                <th>Section</th>
                <td>{{ $student_info->section['section_name'] }}</td>
            </tr>
            <tr>
                <th>Roll</th>
                <td>{{ $student_info->student_id }}</td>
                <th>Academic Year</th>
                <td colspan="2">{{ $student_info->session['session_name'] }}</td>
                <td></td>
            </tr>
        </table>
    </div>
    <br>
    <div>
        <table>
            <thead>
            <tr>
                <th rowspan="2">Subject</th>
                <th colspan="{{ count($exam_categories) }}">{{ $exam_type->exam_type }}</th>
                <th rowspan="2">Total<br> Marks(100)</th>
                <th rowspan="2">CTM(75)</th>
                <th rowspan="2">TE(25)</th>
                <th rowspan="2">FM=<br>CTM+TE(100)</th>
                <th rowspan="2">Highest<br>Mark</th>
                <th rowspan="2">Grade</th>
                <th rowspan="2">GPA</th>
            </tr>
            <tr>
                @foreach($exam_categories as $exam_id=>$exam_name)
                    <th>{{ $exam_name }}</th>
                @endforeach
            </tr>
            </thead>

            <tbody>
            @php
                $is_failed=0;
            @endphp
            @foreach($all_subjects as $subject )
                <tr>
                    <td>{{ $subject['subject_name'] }}</td>
                    @foreach($exam_categories as $exam_id=>$exam_name)
                        <td class="align-middle">@if(isset($subject['marks'][$exam_id])){{ $subject['marks'][$exam_id] }} @else{{ '0' }} @endif</td>
                    @endforeach
                    <td>{{ $subject['total_marks'] }}</td>
                    <td>{{ $subject['converted_mark'] }}</td>
                    <td>@if($subject['tutorial_mark']>0){{ $subject['tutorial_mark'] }} @else {{ '0' }} @endif</td>
                    <td>{{ $subject['full_mark'] }}</td>
                    <td>{{ $subject['height_mark'] }}</td>
                    <td>{{ $subject['grade'] }}</td>
                    <td>{{ $subject['gpa'] }}</td>
                </tr>
                @php
                    if ($subject['gpa'] == 0){
                        $is_failed++;
                    }
                @endphp
            @endforeach
            <tr>
                <td>{{ $fourth_subject['subject_name'] }}</td>
                @foreach($exam_categories as $exam_id=>$exam_name)
                    <td>@if(isset($fourth_subject['marks'][$exam_id])){{ $fourth_subject['marks'][$exam_id] }} @else{{ '0' }} @endif</td>
                @endforeach
                <td>{{ $fourth_subject['total_marks'] }}</td>
                <td>{{ $fourth_subject['converted_mark'] }}</td>
                <td>@if($fourth_subject['tutorial_mark']>0){{ $fourth_subject['tutorial_mark'] }} @else {{ '0' }} @endif</td>
                <td>{{ $fourth_subject['full_mark'] }}</td>
                <td>{{ $fourth_subject['height_mark'] }}</td>
                <td>{{ $fourth_subject['grade'] }}</td>
                <td>{{ $fourth_subject['gpa'] }}</td>
            </tr>
            <tr>
                <th colspan="{{ count($exam_categories) +1 }}">Total</th>
                <td>{{ $total_marks }}</td>
                <td>{{ $converted_marks }}</td>
                <td></td>
                <td>{{ $full_marks }}</td>
                <td></td>
                <td>@if($is_failed == 0){{ $grade }} @else{{ 'F' }} @endif</td>
                <td>@if($is_failed == 0){{ $gpa }} @else{{ '0' }} @endif</td>
            </tr>
            </tbody>
        </table>
    </div>
    <br>
    <div>
        <table>
            <tr>
                <th>Section Position</th>
                <th>Working Days</th>
                <th>Present</th>
                <th>Late Present</th>
                <th>Absent</th>
                <th>Guardian Sign</th>
            </tr>
            <tr>
                <th>&nbsp;</th>
                <th>{{ array_sum($student_attendance) }}</th>
                <th>@if(isset($student_attendance['P'])){{ $student_attendance['P'] }} @else{{ '0' }} @endif</th>
                <th>@if(isset($student_attendance['L'] )){{ $student_attendance['L'] }} @else{{ '0' }} @endif</th>
                <th>@if(isset($student_attendance['A'])){{ $student_attendance['A'] }} @else{{ '0' }} @endif</th>
                <th>&nbsp;</th>
            </tr>
        </table>
    </div>
    <br>
    <div>
        <table style="">
            <tr>
                <th colspan="3">Discipline</th>
                <th colspan="8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
            </tr>
            <tr>
                <th colspan="3">Punctuality</th>
                <th colspan="8"></th>
            </tr>
            <tr>
                <th colspan="3">Cleanliness</th>
                <th colspan="8"></th>
            </tr>
            <tr>
                <th colspan="3">Class Teacher’s Comments</th>
                <th colspan="8"></th>
            </tr>
            <tr>
                <th colspan="3">Principal’s Comments</th>
                <th colspan="8"></th>
            </tr>
        </table>
    </div>
    <br><br>
    <div>
        <div style="float: left">
            <span><b>-----------------</b></span><br>
            <span><b>Class Teacher</b></span>
        </div>
        <div style="padding-left: 410px">
            <span><b>-------------</b></span><br>
            <span><b>Principal</b></span>
        </div>
    </div>
    <br><br>
    <div>
        <table>
            <tr>
                <td>
                    <b>
                        CQ= Creative Question<br>
                        MCQ= Multiple Choice Question
                    </b>
                </td>
                <td>
                    <b>
                        Prac= Practical<br>
                        TE= Tutorial Exam
                    </b>
                </td>
                <td>
                    <b>
                        CTM= Converted Total Marks<br>
                        FM= Full Marks
                    </b>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>