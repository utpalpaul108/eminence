@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Home Page
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Page</a></li>
                <li class="active">Edit Home Page</li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>['AdminPageController@update',$page->id],'method'=>'PUT','files'=>true]) !!}
                <div class="col-md-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group col-sm-12">
                                <label for="title">Page Title</label>
                                <input type="text" name="contents[page_title]" value="@if(isset($page->contents['page_title'])){{ $page->contents['page_title'] }} @endif" placeholder="Page Title" id="title" class="form-control" required>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="subtitle">Page Subtitle</label>
                                <input type="text" name="contents[page_subtitle]" value="@if(isset($page->contents['page_subtitle'])){{ $page->contents['page_subtitle'] }} @endif" placeholder="Page Subtitle" id="subtitle" class="form-control" required>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="details">Page Details</label>
                                <textarea name="contents[page_details]" id="details" rows="5" class="editor form-control" required>@if(isset($page->contents['page_details'])){{ $page->contents['page_details'] }} @endif</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-body">
                            <div class="form-group col-sm-12">
                                <label for="latest_news_title">Title For Latest News</label>
                                <input type="text" name="contents[latest_news_title]" value="@if(isset($page->contents['latest_news_title'])){{ $page->contents['latest_news_title'] }} @endif" placeholder="Latest News Title" id="latest_news_title" class="form-control" required>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="notice_board_title">Title For Notice Board</label>
                                <input type="text" name="contents[notice_board_title]" value="@if(isset($page->contents['notice_board_title'])){{ $page->contents['notice_board_title'] }} @endif" placeholder="Notice Board Title" id="notice_board_title" class="form-control" required>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="photo_gallery_title">Title For Photo Gallery</label>
                                <input type="text" name="contents[photo_gallery_title]" value="@if(isset($page->contents['photo_gallery_title'])){{ $page->contents['photo_gallery_title'] }} @endif" placeholder="Photo Gallery Title" id="photo_gallery_title" class="form-control" required>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="photo_gallery_subtitle">Subtitle For Photo Gallery</label>
                                <input type="text" name="contents[photo_gallery_subtitle]" value="@if(isset($page->contents['photo_gallery_subtitle'])){{ $page->contents['photo_gallery_subtitle'] }} @endif" placeholder="Photo Gallery Subtitle" id="photo_gallery_subtitle" class="form-control">
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer ic_margin_left15">
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Page Image</h3>
                        </div>
                        <div class="box-body text-center">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                                    <img src="@if(isset($page->contents['page_image']) && $page->contents['page_image'] != ''){{ Storage::url($page->contents['page_image']) }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="...">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                                <div>
                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                    <input type="file" name="page_image">
                                </span>
                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
@endsection