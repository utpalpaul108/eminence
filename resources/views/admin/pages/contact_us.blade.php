@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Contact Us Page
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Page</a></li>
                <li class="active">Edit Contact Us Page</li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>['AdminPageController@update',$page->id],'method'=>'PUT','files'=>true]) !!}
                <div class="col-md-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group col-sm-12">
                                <label for="contact_us_title">Title For Contact Us Page</label>
                                <input type="text" name="contents[contact_us_title]" value="@if(isset($page->contents['contact_us_title'])){{ $page->contents['contact_us_title'] }} @endif" placeholder="Contact Us Title" id="contact_us_title" class="form-control" required>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="contact_address">Address</label>
                                <textarea name="contents[contact_address]" id="contact_address" rows="5" class="form-control" required>@if(isset($page->contents['contact_address'])){{ $page->contents['contact_address'] }} @endif</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-body">
                            <div class="form-group col-sm-12">
                                <label for="phone_number_title">Title For Phone Number</label>
                                <input type="text" name="contents[phone_number_title]" value="@if(isset($page->contents['phone_number_title'])){{ $page->contents['phone_number_title'] }} @endif" placeholder="Phone Number Title" id="phone_number_title" class="form-control" required>
                            </div>
                            <div class="PhoneNumberGroup">
                                @if(isset($page->contents['phone_number']))
                                    @php($sl=0)
                                    @foreach($page->contents['phone_number'] as $phone_number)
                                        <div class="form-group eachPhoneNumber ic_clear ic_padding_top col-sm-12">
                                            <div class="col-sm-10 ic_padding_left_null" style="padding-left: 0px">
                                                <input name="contents[phone_number][{{ $sl }}]" value="{{ $phone_number }}" class="form-control input-sm" placeholder="Phone Number" type="text">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button" class="btn btn-xs btn-danger" onclick="if (confirm('Are You Sure ?')){ $(this).parent().parent().remove();}"><i class="fa fa-trash-o"></i></button>
                                            </div>
                                        </div>
                                        @php($sl++)
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group col-sm-12 pull-left ic_padding_left_null" style="padding-top: 15px; padding-left: 15px">
                                <button type="button" id="addPhone" class="btn btn-xs btn-success"><i class="fa fa-plus-circle"></i> Add Phone</button>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-body">
                            <div class="form-group col-sm-12">
                                <label for="mobile_number_title">Title For Mobile Number</label>
                                <input type="text" name="contents[mobile_number_title]" value="@if(isset($page->contents['mobile_number_title'])){{ $page->contents['mobile_number_title'] }} @endif" placeholder="Mobile Number Title" id="mobile_number_title" class="form-control" required>
                            </div>
                            <div class="MobileNumberGroup">
                                @if(isset($page->contents['mobile_number']))
                                    @php($sl=0)
                                    @foreach($page->contents['mobile_number'] as $mobile_number)
                                        <div class="form-group eachMobileNumber ic_clear ic_padding_top col-sm-12">
                                            <div class="col-sm-10 ic_padding_left_null" style="padding-left: 0px">
                                                <input name="contents[mobile_number][{{ $sl }}]" value="{{ $mobile_number }}" class="form-control input-sm" placeholder="Phone Number" type="text">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button" class="btn btn-xs btn-danger" onclick="if (confirm('Are You Sure ?')){ $(this).parent().parent().remove();}"><i class="fa fa-trash-o"></i></button>
                                            </div>
                                        </div>
                                        @php($sl++)
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group col-sm-12 pull-left ic_padding_left_null" style="padding-top: 15px;padding-left: 15px">
                                <button type="button" id="addMobile" class="btn btn-xs btn-success"><i class="fa fa-plus-circle"></i> Add Mobile</button>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-body">
                            <div class="form-group col-sm-12">
                                <label for="mail_address_title">Title For Mail Address</label>
                                <input type="text" name="contents[mail_address_title]" value="@if(isset($page->contents['mail_address_title'])){{ $page->contents['mail_address_title'] }} @endif" placeholder="Mail Address Title" id="mail_address_title" class="form-control" required>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="mail_address">Mail Address</label>
                                <input type="text" name="contents[mail_address]" value="@if(isset($page->contents['mail_address'])){{ $page->contents['mail_address'] }} @endif" placeholder="Mail Address" id="mail_address" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-body">
                            <div class="form-group col-sm-12">
                                <label for="message_us_title">Title For Message Us</label>
                                <input type="text" name="contents[message_us_title]" value="@if(isset($page->contents['message_us_title'])){{ $page->contents['message_us_title'] }} @endif" placeholder="Message Us Title" id="message_us_title" class="form-control" required>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="message_us_subtitle">Subtitle For Message Us</label>
                                <input type="text" name="contents[message_us_subtitle]" value="@if(isset($page->contents['message_us_subtitle'])){{ $page->contents['message_us_subtitle'] }} @endif" placeholder="Message Us Subtitle" id="message_us_subtitle" class="form-control" required>
                            </div>
                        </div>
                        <div class="box-footer ic_margin_left15">
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Header Image</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group col-sm-12 TriSea-technologies-Switch" style="padding-left: 0px">
                                <span style="font-weight: bold">Header Status &nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <input id="TriSeaSuccess" name="contents[header_status]" type="checkbox" @if(isset($page->contents['header_status']) && $page->contents['header_status'] == 'on' ){{ 'checked' }} @endif>
                                <label for="TriSeaSuccess" class="label-success"></label>
                            </div>
                            <div class="form-group">
                                <label for="published_at">Header Title</label>
                                <input type="text" name="contents[header_title]" value="@if(isset($page->contents['header_title'])){{ $page->contents['header_title'] }} @endif" class="form-control">
                            </div>
                        </div>
                        <div class="box-body text-center">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                                    <img src="@if(isset($page->contents['header_image']) && $page->contents['header_image'] != ''){{ Storage::url($page->contents['header_image']) }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="...">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                                <div>
                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                    <input type="file" name="header_image">
                                </span>
                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
    <script>

        //    For Adding Phone Number
        $('#addPhone').click(function () {
            var recentCounter = $('.eachPhoneNumber').length;

            var data='<div class="form-group eachPhoneNumber ic_clear ic_padding_top col-sm-12">\n' +
                '                                            <div class="col-sm-10 ic_padding_left_null" style="padding-left: 0px">\n' +
                '                                                <input name="contents[phone_number]['+recentCounter+']"  class="form-control input-sm" placeholder="Phone Number" type="text">\n' +
                '                                            </div>\n' +
                '                                            <div class="col-sm-2 pull-left">\n' +
                '                                                <button type="button" class="btn btn-xs btn-danger" onclick="if (confirm(\'Are You Sure ?\')){ $(this).parent().parent().remove();}"><i class="fa fa-trash-o"></i></button>\n' +
                '                                            </div>\n' +
                '                                        </div>';
            $('.PhoneNumberGroup').append(data);
        });

        //    For Adding Mobile Number
        $('#addMobile').click(function () {
            var recentCounter = $('.eachMobileNumber').length;

            var data='<div class="form-group eachMobileNumber ic_clear ic_padding_top col-sm-12">\n' +
                '                                            <div class="col-sm-10 ic_padding_left_null" style="padding-left: 0px">\n' +
                '                                                <input name="contents[mobile_number]['+recentCounter+']"  class="form-control input-sm" placeholder="Mobile Number" type="text">\n' +
                '                                            </div>\n' +
                '                                            <div class="col-sm-2">\n' +
                '                                                <button type="button" class="btn btn-xs btn-danger" onclick="if (confirm(\'Are You Sure ?\')){ $(this).parent().parent().remove();}"><i class="fa fa-trash-o"></i></button>\n' +
                '                                            </div>\n' +
                '                                        </div>';
            $('.MobileNumberGroup').append(data);
        });
    </script>
@endsection