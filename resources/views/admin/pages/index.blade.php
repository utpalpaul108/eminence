@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Pages
                <small>All Pages</small>
            </h1>
            <ol class="breadcrumb">
                <li><i class="fa fa-dashboard"></i> <a href="#">Dashboard</a></li>
                <li class="active">Pages</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        @can('add_page')
                        {{--<div class="box-header">--}}
                            {{--<div class="pull-left">--}}
                                {{--<a id="add-button" title="Add New Page" class="btn btn-success" href="{{ action('AdminUserController@create') }}"><i class="fa fa-plus-circle"></i> Add New User</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        @endcan
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Page Name</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($pages->currentPage()-1)*$pages->perPage())+1)
                                @foreach($pages as $page)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $page->name }}</td>
                                        <td>
                                            @can('edit_page')
                                                <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('AdminPageController@edit',['id'=>$page->id]) }}">
                                                    Edit
                                                </a>&nbsp;
                                            @endcan
                                            @can('delete_page')
                                                <a title="Delete" class="btn btn-xs btn-danger delete-row" href="#">
                                                    Delete
                                                </a>
                                            @endcan
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $pages->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection