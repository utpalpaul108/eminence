@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Examination System Page
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Page</a></li>
                <li class="active">Edit Examination System Page</li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>['AdminPageController@update',$page->id],'method'=>'PUT','files'=>true]) !!}
                <div class="col-md-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="page_title">Page Title</label>
                                <input type="text" name="contents[page_title]" value="@if(isset($page->contents['page_title'])){{ $page->contents['page_title'] }} @endif" placeholder="Page Title" id="page_title" class="form-control" required>
                            </div>
                            <div class="ExaminationSystemGroup">
                                @if(isset($page->contents['examination_system']))
                                    @php($sl=0)
                                    @foreach($page->contents['examination_system'] as $examination_system)
                                        <div class="form-group eachExaminationSystem ic_clear ic_padding_top">
                                            <div class="col-sm-3 ic_padding_left_null" style="padding-left: 0px">
                                                <input name="contents[examination_system][{{ $sl }}][name]" value="{{ $examination_system['name'] }}" class="form-control input-sm" placeholder="Exam Name" type="text">
                                            </div>
                                            <div class="col-sm-3">
                                                <input name="contents[examination_system][{{ $sl }}][duration]" value="{{ $examination_system['duration'] }}" class="form-control input-sm" placeholder="Exam Duration" type="text">
                                            </div>
                                            <div class="col-sm-2">
                                                <input name="contents[examination_system][{{ $sl }}][marks]" value="{{ $examination_system['marks'] }}" class="form-control input-sm" placeholder="Exam Mark" type="text">
                                            </div>
                                            <div class="col-sm-2">
                                                <input name="contents[examination_system][{{ $sl }}][class]" value="{{ $examination_system['class'] }}" class="form-control input-sm" placeholder="Class" type="text">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button" class="btn btn-xs btn-danger" onclick="if (confirm('Are You Sure ?')){ $(this).parent().parent().remove();}"><i class="fa fa-trash-o"></i></button>
                                            </div>
                                        </div>
                                        @php($sl++)
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group col-sm-12 pull-left ic_padding_left_null" style="padding-top: 15px; padding-left: 0px">
                                <button type="button" id="addExamSystem" class="btn btn-xs btn-success"><i class="fa fa-plus-circle"></i> Add Examination System</button>
                            </div>

                            <div class="form-group col-sm-12" style="padding-left: 0px; padding-right: 0px">
                                <label for="page_details">Page Details</label>
                                <textarea name="contents[page_details]" id="page_details" rows="5" class="editor form-control" placeholder="Page Details" required>@if(isset($page->contents['page_details'])){{ $page->contents['page_details'] }} @endif</textarea>
                            </div>
                        </div>
                        <div class="box-footer ic_margin_left15">
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Header</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group col-sm-12 TriSea-technologies-Switch" style="padding-left: 0px">
                                <span style="font-weight: bold">Header Status &nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <input id="TriSeaSuccess" name="contents[header_status]" type="checkbox" @if(isset($page->contents['header_status']) && $page->contents['header_status'] == 'on' ){{ 'checked' }} @endif>
                                <label for="TriSeaSuccess" class="label-success"></label>
                            </div>
                            <div class="form-group">
                                <label for="published_at">Header Title</label>
                                <input type="text" name="contents[header_title]" value="@if(isset($page->contents['header_title'])){{ $page->contents['header_title'] }} @endif" class="form-control">
                            </div>
                        </div>
                        {{--<label for="published_at" style="padding-left: 10px">Header Image</label>--}}
                        <div class="box-body text-center">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                                    <img src="@if(isset($page->contents['header_image']) && $page->contents['header_image'] != ''){{ Storage::url($page->contents['header_image']) }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="...">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                                <div>
                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                    <input type="file" name="header_image">
                                </span>
                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
    <script>
        $('#addExamSystem').click(function () {
            var recentCounter = $('.eachExaminationSystem').length;

            var data='<div class="form-group eachExaminationSystem ic_clear ic_padding_top">\n' +
                '                                            <div class="col-sm-3 ic_padding_left_null" style="padding-left: 0px">\n' +
                '                                                <input name="contents[examination_system]['+recentCounter+'][name]" class="form-control input-sm" placeholder="Exam Name" type="text">\n' +
                '                                            </div>\n' +
                '                                            <div class="col-sm-3">\n' +
                '                                                <input name="contents[examination_system]['+recentCounter+'][duration]" class="form-control input-sm" placeholder="Exam Duration" type="text">\n' +
                '                                            </div>\n' +
                '                                            <div class="col-sm-2">\n' +
                '                                                <input name="contents[examination_system]['+recentCounter+'][marks]" class="form-control input-sm" placeholder="Exam Mark" type="text">\n' +
                '                                            </div>\n' +
                '                                            <div class="col-sm-2">\n' +
                '                                                <input name="contents[examination_system]['+recentCounter+'][class]" class="form-control input-sm" placeholder="Class" type="text">\n' +
                '                                            </div>\n' +
                '                                            <div class="col-sm-2">\n' +
                '                                                <button type="button" class="btn btn-xs btn-danger" onclick="if (confirm(\'Are You Sure ?\')){ $(this).parent().parent().remove();}"><i class="fa fa-trash-o"></i></button>\n' +
                '                                            </div>\n' +
                '                                        </div>';
            $('.ExaminationSystemGroup').append(data);
        });
    </script>
@endsection