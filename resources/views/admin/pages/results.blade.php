@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Result Page
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Page</a></li>
                <li class="active">Result Page</li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>['AdminPageController@update',$page->id],'method'=>'PUT','files'=>true]) !!}
                <div class="col-md-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group col-sm-12">
                                <label for="result_title">All Result Title</label>
                                <input type="text" name="contents[result_title]" value="@if(isset($page->contents['result_title'])){{ $page->contents['result_title'] }} @endif" placeholder="Result Title" id="result_title" class="form-control" required>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="result_details">All Result Details</label>
                                <textarea name="contents[result_details]" id="result_details" rows="5" class="editor form-control" placeholder="Result Details" required>@if(isset($page->contents['result_details'])){{ $page->contents['result_details'] }} @endif</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group col-sm-12">
                                <label for="hsc_result_title">HSC Result Title</label>
                                <input type="text" name="contents[hsc_result_title]" value="@if(isset($page->contents['hsc_result_title'])){{ $page->contents['hsc_result_title'] }} @endif" placeholder="HSC Result Title" id="hsc_result_title" class="form-control" required>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="hsc_result_details">HSC Result Details</label>
                                <textarea name="contents[hsc_result_details]" id="hsc_result_details" rows="5" class="editor form-control" placeholder="HSC Result Details" required>@if(isset($page->contents['hsc_result_details'])){{ $page->contents['hsc_result_details'] }} @endif</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-body">
                            <div class="form-group col-sm-12">
                                <label for="latest_news_title">Title For Latest News</label>
                                <input type="text" name="contents[latest_news_title]" value="@if(isset($page->contents['latest_news_title'])){{ $page->contents['latest_news_title'] }} @endif" placeholder="Latest News Title" id="latest_news_title" class="form-control" required>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="notice_board_title">Title For Notice Board</label>
                                <input type="text" name="contents[notice_board_title]" value="@if(isset($page->contents['notice_board_title'])){{ $page->contents['notice_board_title'] }} @endif" placeholder="Notice Board Title" id="notice_board_title" class="form-control" required>
                            </div>
                        </div>
                        <div class="box-footer ic_margin_left15">
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Header</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group col-sm-12 TriSea-technologies-Switch" style="padding-left: 0px">
                                <span style="font-weight: bold">Header Status &nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <input id="TriSeaSuccess" name="contents[header_status]" type="checkbox" @if(isset($page->contents['header_status']) && $page->contents['header_status'] == 'on' ){{ 'checked' }} @endif>
                                <label for="TriSeaSuccess" class="label-success"></label>
                            </div>
                            <div class="form-group">
                                <label for="published_at">Header Title</label>
                                <input type="text" name="contents[header_title]" value="@if(isset($page->contents['header_title'])){{ $page->contents['header_title'] }} @endif" class="form-control">
                            </div>
                        </div>
                        {{--<label for="published_at" style="padding-left: 10px">Header Image</label>--}}
                        <div class="box-body text-center">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                                    <img src="@if(isset($page->contents['header_image']) && $page->contents['header_image'] != ''){{ Storage::url($page->contents['header_image']) }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="...">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                                <div>
                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                    <input type="file" name="header_image">
                                </span>
                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
@endsection
