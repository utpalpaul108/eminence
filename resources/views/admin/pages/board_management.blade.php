@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Board Management Page
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Page</a></li>
                <li class="active">Edit Board Management Page</li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>['AdminPageController@update',$page->id],'method'=>'PUT','files'=>true]) !!}
                <div class="col-md-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group col-sm-12 TriSea-technologies-Switch">
                                <span style="font-weight: bold">Header Status &nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <input id="TriSeaSuccess" name="contents[header_status]" type="checkbox" @if(isset($page->contents['header_status']) && $page->contents['header_status'] == 'on' ){{ 'checked' }} @endif>
                                <label for="TriSeaSuccess" class="label-success"></label>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="title">Header Title</label>
                                <input type="text" name="contents[header_title]" value="@if(isset($page->contents['header_title'])){{ $page->contents['header_title'] }} @endif" placeholder="Header Title" id="title" class="form-control" required>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer ic_margin_left15">
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Header Image</h3>
                        </div>
                        <div class="box-body text-center">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                                    <img src="@if(isset($page->contents['header_image']) && $page->contents['header_image'] != ''){{ Storage::url($page->contents['header_image']) }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="...">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                                <div>
                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                    <input type="file" name="header_image">
                                </span>
                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection
