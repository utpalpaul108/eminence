@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                All Students
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Students</a></li>
                <li class="active">All Students</li>
            </ol>
        </section>

        @can('send_sms')
            <section class="content" id="send_sms" style="display: none">
                <div class="box box-default color-palette-box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-tag"></i> Send SMS</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::open(['action'=>'AdminStudentController@sendSMS']) !!}
                                    <input type="hidden" name="department_id" value="{{ $input_data['department_id'] }}">
                                    <input type="hidden" name="group_id" value="{{ $input_data['group_id'] }}">
                                    <input type="hidden" name="section_id" value="{{ $input_data['section_id'] }}">
                                    <input type="hidden" name="session_id" value="{{ $input_data['session_id'] }}">
                                    <div class="form-group">
                                        <textarea name="sms_text" class="form-control" rows="3" placeholder="Enter Text ..."></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success pull-right">Send</button>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </section>
        @endcan

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @elseif(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="box">
                        <div class="box-header">
                            @can('add_student')
                                <div class="pull-left col-md-6">
                                    <a id="back-button" title="Back" class="btn btn-default" href="" onclick="window.history.back();"><i class="fa fa-caret-left"></i> Back</a>&nbsp;&nbsp;
                                    <a id="add-button" title="Add New Student" class="btn btn-success" href="{{ action('AdminStudentController@create') }}"><i class="fa fa-plus-circle"></i><span class="hidden-xs"> Add New Student</span></a>
                                </div>
                            @endcan
                            @can('send_sms')
                                <div class="col-md-6">
                                    <button type="button" id="show_sms" class="btn btn-info pull-right"><i class="fa fa-paper-plane"></i> &nbsp Send SMS</button>
                                </div>
                            @endcan
                        </div>
                        <div class="box-body">
                            @foreach($students as $student)
                                <div class="col-lg-3 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-yellow">
                                        <div class="inner ic_inner">
                                            <h4><span class="visible-xs">{{ str_limit($student->name,10,'...') }}</span><span class="hidden-xs">{{ str_limit($student->name,15,'...') }}</span></h4>
                                            <p>{{ $student->student_id }}</p>
                                        </div>
                                        <div class="icon ic_students">
                                            <img src="{{ Storage::url($student->student_image) }}" class="img-responsive img-thumbnail" style="max-height: 60px; max-width: 60px">
                                        </div>
                                        @can('view_student')
                                            <a href="{{ action('AdminStudentController@show',['id'=>$student->id]) }}" class="small-box-footer">
                                                More info <i class="fa fa-arrow-circle-right"></i>
                                            </a>
                                        @endcan
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
    <!-- /.content -->
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('#show_sms').click(function () {
                $('#send_sms').toggle("slow");
            })
        })
    </script>
@endsection