@extends('admin.partials.main')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('ic_admin/css/progress_bar.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add New Student
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Student</a></li>
                <li class="active">Add New Student</li>
            </ol>
        </section>

        <!-- Main content -->

        <section class="content">
            <div class="">
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @elseif(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <div class="stepwizard">
                    <div class="stepwizard-row setup-panel">
                        <div class="stepwizard-step col-xs-2">
                            <a href="#step-1" type="button" class="btn btn-success btn-circle">1</a>
                            <p><small>Basic Info</small></p>
                        </div>
                        <div class="stepwizard-step col-xs-2">
                            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                            <p><small>Guardian Info</small></p>
                        </div>
                        <div class="stepwizard-step col-xs-2">
                            <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                            <p><small>Address</small></p>
                        </div>
                        <div class="stepwizard-step col-xs-2">
                            <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                            <p><small>SSC & HSC Info</small></p>
                        </div>
                        <div class="stepwizard-step col-xs-2">
                            <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                            <p><small>Department Info</small></p>
                        </div>
                        <div class="stepwizard-step col-xs-2">
                            <a href="#step-6" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a>
                            <p><small>Student Image</small></p>
                        </div>
                    </div>
                </div>

                {!! Form::open(['action'=>['AdminStudentController@update',$student->id],'files'=>true, 'method'=>'PUT']) !!}
                <div class="panel panel-primary setup-content" id="step-1">
                    <div class="panel-heading">
                        <h3 class="panel-title">Basic Info</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="name_bn">Name (In Bangla)</label>
                            <input type="text" name="student_info[name_bn]" value="{{ $student->student_info['name_bn'] }}" placeholder="Enter Name(In Bangla)" id="name_bn" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="name">Name (In English)</label>
                            <input type="text" name="name" id="name" value="{{ $student->name }}" placeholder="Enter Name(In English)" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="gender">Gender</label>
                            <select name="student_info[gender]" id="gender" class="form-control">
                                <option value="male" @if($student->student_info['gender'] == 'male'){{ 'selected' }} @endif >Male</option>
                                <option value="female" @if($student->student_info['gender'] == 'female'){{ 'selected' }} @endif  >Female</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="date_of_birth">Date Of Birth</label>
                            <input type="text" name="student_info[date_of_birth]" value="{{ $student->student_info['date_of_birth'] }}" placeholder="Date Of Birth" id="date_of_birth" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="nationality">Nationality</label>
                            <input type="text" name="student_info[nationality]" value="{{ $student->student_info['nationality'] }}" placeholder="Nationality" id="nationality" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="religion">Religion</label>
                            <input type="text" name="student_info[religion]" value="{{ $student->student_info['religion'] }}" placeholder="Religion" id="religion" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="blood_group">Blood Group</label>
                            <input type="text" name="student_info[blood_group]" value="{{ $student->student_info['blood_group'] }}" placeholder="Blood Group" id="blood_group" class="form-control">
                        </div>
                        <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                    </div>
                </div>

                <div class="panel panel-primary setup-content" id="step-2">
                    <div class="panel-heading">
                        <h3 class="panel-title">Father & Mother Info</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="father_name">Father Name</label>
                            <input type="text" name="student_info[father_name]" value="{{ $student->student_info['father_name'] }}" placeholder="Enter Father Name" id="father_name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="father_contact">Father Contact Number</label>
                            <input type="text" id="father_contact" name="student_info[father_contact]" value="{{ $student->student_info['father_contact'] }}" placeholder="Father Contact Number" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="father_nid">Father NID Card No</label>
                            <input type="text" name="student_info[father_nid]" value="{{ $student->student_info['father_nid'] }}" placeholder="Father NID Card No" id="father_nid" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="father_occupation">Father Occupation</label>
                            <input type="text" id="father_occupation" name="student_info[father_occupation]" value="{{ $student->student_info['father_occupation'] }}" placeholder="Father Occupation" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="mother_name">Mother Name</label>
                            <input type="text" name="student_info[mother_name]" value="{{ $student->student_info['mother_name'] }}" placeholder="Enter Mother Name" id="mother_name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="mother_contact">Mother Contact Number</label>
                            <input type="text" name="student_info[mother_contact]" value="{{ $student->student_info['mother_contact'] }}" placeholder="Mother Contact No" id="mother_contact" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="mother_nid">Mother NID Card No</label>
                            <input type="text" name="student_info[mother_nid]" value="{{ $student->student_info['mother_nid'] }}" placeholder="Mother NID Card No" id="mother_nid" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="mother_occupation">Mother Occupation</label>
                            <input type="text" id="mother_occupation" name="student_info[mother_occupation]" value="{{ $student->student_info['mother_occupation'] }}" placeholder="Mother Occupation" class="form-control">
                        </div>
                        <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                    </div>
                </div>

                <div class="panel panel-primary setup-content" id="step-3">
                    <div class="panel-heading">
                        <h3 class="panel-title">Address</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="gurdian_name">Guardian's Name</label>
                            <input type="text" name="student_info[gurdian_name]" value="{{ $student->student_info['gurdian_name'] }}" placeholder="Guardian's Name" id="gurdian_name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="relation_with_gurdian">Relation With Guardian</label>
                            <input type="text" name="student_info[relation_with_gurdian]" value="{{ $student->student_info['relation_with_gurdian'] }}" placeholder="Relation With Guardian" id="relation_with_gurdian" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="relation_with_gurdian">Guardian NID Card No</label>
                            <input type="text" name="student_info[guardian_nid]" value="{{ $student->student_info['guardian_nid'] }}" placeholder="Guardian NID Card No" id="guardian_nid" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="guardian_occupation">Guardian Occupation</label>
                            <input type="text" name="student_info[guardian_occupation]" value="{{ $student->student_info['guardian_occupation'] }}" placeholder="Guardian Occupation" id="guardian_occupation" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="gurdian_contact">Gurdian's Contact Number</label>
                            <input type="text" name="student_info[gurdian_contact]" value="{{ $student->student_info['gurdian_contact'] }}" id="gurdian_contact" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="sms_number">SMS Sending Number</label>
                            <input type="text" name="student_info[sms_number]" value="{{ $student->student_info['sms_number'] }}" id="sms_number" placeholder="SMS Sending Number" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="present_address">Present Address</label>
                            <textarea name="student_info[present_address]" id="present_address" rows="5" class="form-control" placeholder="Present Address">{{ $student->student_info['present_address'] }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="permanent_address">Permanent Address</label>
                            <textarea name="student_info[permanent_address]" value="" id="permanent_address" rows="5" class="form-control" placeholder="Permanent Address">{{ $student->student_info['permanent_address'] }}</textarea>
                        </div>
                        <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                    </div>
                </div>

                <div class="panel panel-primary setup-content" id="step-4">
                    <div class="panel-heading">
                        <h3 class="panel-title">SSC & HSC Info</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="ssc_passing_year">SSC Passing Year</label>
                            <input type="text" name="student_info[ssc_passing_year]" value="{{ $student->student_info['ssc_passing_year'] }}" id="ssc_passing_year" class="form-control" placeholder="SSC Passing Year">
                        </div>
                        <div class="form-group">
                            <label for="ssc_reg_no">SSC Reg No</label>
                            <input type="text" name="student_info[ssc_reg_no]" value="{{ $student->student_info['ssc_reg_no'] }}" id="ssc_reg_no" class="form-control" placeholder="SSC Reg No">
                        </div>
                        <div class="form-group">
                            <label for="ssc_roll_no">SSC Roll No</label>
                            <input type="text" name="student_info[ssc_roll_no]" value="{{ $student->student_info['ssc_roll_no'] }}" id="ssc_roll_no" class="form-control" placeholder="SSC Roll No">
                        </div>
                        <div class="form-group">
                            <label for="hsc_passing_year">HSC Passing Year</label>
                            <input type="text" name="student_info[hsc_passing_year]" value="{{ $student->student_info['hsc_passing_year'] }}" id="hsc_passing_year" class="form-control" placeholder="HSC Passing Year">
                        </div>
                        <div class="form-group">
                            <label for="hsc_reg_no">HSC Reg No</label>
                            <input type="text" name="student_info[hsc_reg_no]" value="{{ $student->student_info['hsc_reg_no'] }}" id="hsc_reg_no" class="form-control" placeholder="HSC Reg No">
                        </div>
                        <div class="form-group">
                            <label for="hsc_roll_no">HSC Roll No</label>
                            <input type="text" name="student_info[hsc_roll_no]" value="{{ $student->student_info['hsc_roll_no'] }}" id="hsc_roll_no" class="form-control" placeholder="HSC Roll No">
                        </div>
                        <div class="form-group">
                            <label for="nu_reg_no">NU Reg No</label>
                            <input type="text" name="student_info[nu_reg_no]" value="{{ $student->student_info['nu_reg_no'] }}" id="nu_reg_no" class="form-control" placeholder="NU Reg No">
                        </div>
                        <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                    </div>
                </div>

                <div class="panel panel-primary setup-content" id="step-5">
                    <div class="panel-heading">
                        <h3 class="panel-title">Department Info</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="body">Department</label>
                            <select class="form-control" id="department_id" name="department_id" required>
                                <option value=""> -- Select Department --</option>
                                @foreach($departments as $department)
                                    <option value="{{ $department->id }}" @if($department->id == $student->department_id){{ 'selected' }} @endif>{{ $department->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="body">Group</label>
                            <select class="form-control" name="group_id" id="group_id" required>
                                @foreach($groups as $group)
                                    <option value="{{ $group->id }}" @if($group->id == $student->group_id){{ 'selected' }} @endif>{{ $group->group_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="body">Section</label>
                            <select class="form-control" name="section_id" id="section_id" required>
                                @foreach($sections as $section)
                                    <option value="{{ $section->id }}" @if($section->id == $student->section_id){{ 'selected' }} @endif>{{ $section->section_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="slug">Session</label>
                            <select name="session_id" class="form-control" required>
                                @foreach($sessions as $session)
                                    <option value="{{ $session->id }}" @if($session->id == $student->session_id){{ 'selected' }} @endif>{{ $session->session_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="slug">Student ID</label>
                            <input type="text" name="student_id" value="{{ $student->student_id }}" id="slug" class="form-control" placeholder="Student ID" required>
                        </div>
                        <div class="form-group">
                            <label for="fourth_subject">Fourth Subject</label>
                            <select name="fourth_subject" class="form-control">
                                <option value=""> -- Select Fourth Subject --</option>
                                @foreach($all_subjects as $subject)
                                    <option value="{{ $subject->id }}" @if($subject->id == $student->fourth_subject){{ 'selected' }} @endif>{{ $subject->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                    </div>
                </div>

                <div class="panel panel-primary setup-content" id="step-6">
                    <div class="panel-heading">
                        <h3 class="panel-title">Student Image</h3>
                    </div>
                    <div class="panel-body">
                        <div class="box-body text-center">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="@if($student->student_image != null){{ Storage::url($student->student_image ) }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="...">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                <div>
                            <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                            <input type="file" name="student_image">
                            </span>
                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                        @can('edit_student')
                            <button class="btn btn-success pull-right" type="submit">Update</button>
                        @endcan
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
    </div>
@endsection

@section('js')
    <script src="{{ asset('ic_admin/js/progress_bar.js') }}"></script>
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    {{--<script>--}}
    {{--CKEDITOR.replace( '.editor' );--}}
    {{--</script>--}}
    <script>
        $(document).ready(function () {
            $('#department_id').change(function (e) {
                var department=$('#department_id').val();
                $.ajax({
                    type:'GET',
                    url:'/ajax_call/department_wise_data',
                    data:{
                        'department_id':department
                    },
                    success:function (response) {
                        var res=JSON.parse(response)
                        $('#group_id').html(res.groups);
                    }
                })
            });

            $('#group_id').change(function (e) {
                var department=$('#department_id').val();
                var group_id=$('#group_id').val();

                $.ajax({
                    type:'GET',
                    url:'/admin/group_wise_data',
                    data:{
                        department_id:department,
                        group_id:group_id,
                    },
                    success:function (response) {
                        var res=JSON.parse(response);
                        $('#section_id').html(res.sections);
                    }
                })
            });
        })
    </script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
        } );
    </script>
@endsection