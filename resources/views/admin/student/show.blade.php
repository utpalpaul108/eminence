@extends('admin.partials.main')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Student Profile
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Student</a></li>
                <li class="active">Student profile</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-3">
                    <!-- Profile Image -->
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <img class="profile-user-img img-responsive img-circle" src="{{ Storage::url($student->student_image) }}" alt="Student profile picture" style="height: 100px">

                            <h3 class="profile-username text-center">{{ $student->name }}</h3>

                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>Department</b> <a class="pull-right">{{ $student->department['name'] }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Group</b> <a class="pull-right">{{ $student->group['group_name'] }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Section</b> <a class="pull-right">{{ $student->section['section_name'] }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Session</b> <a class="pull-right">{{ $student->session['session_name'] }}</a>
                                </li>
                            </ul>

                            @can('edit_student')
                                <div class="col-md-6">
                                    <a href="{{ action('AdminStudentController@edit',['id'=>$student->id]) }}" class="btn btn-primary btn-block"><b>Edit</b></a>
                                </div>
                            @endcan
                            @can('delete_student')
                                <div class="col-md-6">
                                    {!! Form::open(['action'=>['AdminStudentController@destroy',$student->id],'method'=>'delete','style'=>'display:inline']) !!}
                                        <button type="submit" class="btn btn-danger btn-block" onclick="return confirm('Are You Sure ?')"><b>Delete</b></button>
                                    {!! Form::close() !!}
                                </div>
                            @endcan

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @elseif(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#basic_info" data-toggle="tab">Basic Info</a></li>
                            <li><a href="#guardian_info" data-toggle="tab">Guardian Info</a></li>
                            <li><a href="#contact_info" data-toggle="tab">Contact Info</a></li>
                            <li><a href="#ssc_hsc_info" data-toggle="tab">SSC & HSC Info</a></li>
                            <li><a href="#department_info" data-toggle="tab">Department Info</a></li>
                            <li><a href="#payment_info" data-toggle="tab">Payment Info</a></li>
                            <li><a href="#message" data-toggle="tab">Send SMS</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="student_info active tab-pane ml-lg" id="basic_info">
                                <p><strong>Name (In Bangla) : </strong>{{ $student->student_info['name_bn'] }}</p>
                                <p><strong>Name (In English) : </strong>{{ $student->name }}</p>
                                <p><strong>Gander : </strong>{{ $student->student_info['gender'] }}</p>
                                <p><strong>Date Of Birth : </strong>{{ $student->student_info['date_of_birth'] }}</p>
                                <p><strong>Nationality : </strong>{{ $student->student_info['nationality'] }}</p>
                                <p><strong>Religion : </strong>{{ $student->student_info['religion'] }}</p>
                                <p><strong>Blood Group : </strong>{{ $student->student_info['blood_group'] }}</p>
                            </div>
                            <div class="student_info tab-pane ml-lg" id="guardian_info">
                                <p><strong>Father Name : </strong>{{ $student->student_info['father_name'] }}</p>
                                <p><strong>Father Contact No : </strong>{{ $student->student_info['father_contact'] }}</p>
                                <p><strong>Father NID Card No : </strong>{{ $student->student_info['father_nid'] }}</p>
                                <p><strong>Father Occupation : </strong>{{ $student->student_info['father_occupation'] }}</p>
                                <p><strong>Mother Name : </strong>{{ $student->student_info['mother_name'] }}</p>
                                <p><strong>Mother Contact No  : </strong>{{ $student->student_info['mother_contact'] }}</p>
                                <p><strong>Mother NID Card No : </strong>{{ $student->student_info['mother_nid']}}</p>
                                <p><strong>Mother Occupation  : </strong>{{ $student->student_info['mother_occupation'] }}</p>
                            </div>
                            <div class="student_info tab-pane ml-lg" id="contact_info">
                                <p><strong>Guardian's Name : </strong>{{ $student->student_info['gurdian_name'] }}</p>
                                <p><strong>Relation With Guardian : </strong>{!!  $student->student_info['relation_with_gurdian'] !!}</p>
                                <p><strong>Guardian NID Card No : </strong>{!!  $student->student_info['guardian_nid'] !!}</p>
                                <p><strong>Guardian Occupation : </strong>{!!  $student->student_info['guardian_occupation'] !!}</p>
                                <p><strong>Gurdian's Contact Number : </strong>{!!  $student->student_info['gurdian_contact'] !!}</p>
                                <p><strong>SMS Sending Number : </strong>{!!  $student->student_info['sms_number'] !!}</p>
                                <p><strong>Present Address : </strong>{!!  $student->student_info['present_address'] !!}</p>
                                <p><strong>Permanent Address : </strong>{!!  $student->student_info['permanent_address'] !!}</p>
                            </div>
                            <div class="student_info tab-pane ml-lg" id="ssc_hsc_info">
                                <p><strong>SSC Passing Year : </strong>{{ $student->student_info['ssc_passing_year'] }}</p>
                                <p><strong>SSC Reg No : </strong>{!!  $student->student_info['ssc_reg_no'] !!}</p>
                                <p><strong>SSC Roll No : </strong>{!!  $student->student_info['ssc_roll_no'] !!}</p>
                                <p><strong>HSC Passing Year : </strong>{!!  $student->student_info['hsc_passing_year'] !!}</p>
                                <p><strong>HSC Reg No : </strong>{!!  $student->student_info['hsc_reg_no'] !!}</p>
                                <p><strong>HSC Roll No : </strong>{!!  $student->student_info['hsc_roll_no'] !!}</p>
                                <p><strong>NU Reg No : </strong>{!!  $student->student_info['nu_reg_no'] !!}</p>
                            </div>
                            <div class="student_info tab-pane ml-lg" id="department_info">
                                <p><strong>Department : </strong>{{ $student->department['name'] }}</p>
                                <p><strong>Group : </strong>{!!  $student->group['group_name'] !!}</p>
                                <p><strong>Section : </strong>{!!  $student->section['section_name'] !!}</p>
                                <p><strong>Session : </strong>{!!  $student->session['session_name'] !!}</p>
                                <p><strong>Student ID : </strong>{!!  $student->student_id !!}</p>
                                <p><strong>Fourth Subject : </strong>{!!  $student->student_fourth_subject['name'] !!}</p>
                            </div>
                            <div class="student_info tab-pane ml-lg" id="payment_info">
                                <p><strong>Last Payment Year : </strong>@if(!is_null($student_payment)){{ $student_payment->payment_year }}@endif</p>
                                <p><strong>Last Payment Month : </strong>@if(!is_null($student_payment)){{ DateTime::createFromFormat('!m', $student_payment->payment_month)->format('F') }}@endif</p>
                                <p><strong>Payment Amount : </strong>@if(!is_null($student_payment)){{ $student_payment->payment_amount }}@endif</p>
                                <p><strong>Due Amount : </strong>@if(!is_null($student_payment)){{ $student_payment->due_amount }}@endif</p>
                            </div>
                            <div class="tab-pane" id="message">
                                {!! Form::open(['action'=>'AdminStudentController@sendSingleSMS', 'class'=>'form-horizontal']) !!}
                                    <input type="hidden" name="student_id" value="{{ $student->id }}">
                                    <div class="form-group">
                                        <label for="message" class="col-sm-2 control-label">SMS Text</label>

                                        <div class="col-sm-10">
                                            <textarea class="form-control" name="message" rows="8" id="message" placeholder="SMS Text &nbsp;. . . . ."></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-success">Send SMS</button>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>

                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->
    </div>
@endsection