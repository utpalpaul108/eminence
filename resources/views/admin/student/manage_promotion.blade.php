@extends('admin.partials.main')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Manage Promotion
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Students</a></li>
                <li class="active">All Students</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <!-- form start -->
                        {!! Form::open(['action'=>'AdminStudentController@manage_promotion','method'=>'GET']) !!}
                        <div class="box-body">
                            <div class="form-group col-md-2">
                                <label>Department</label>
                                <select name="department" class="form-control" id="department_id" required>
                                    @if(isset($departments) && $departments != '')
                                        @foreach($departments as $department)
                                            <option value="{{ $department->id }}" @if($department->id == $input_data['department']){{ 'selected' }} @endif >{{ $department->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label>Group</label>
                                <select name="group" class="form-control" id="group_id" required>
                                    @if(isset($groups) && $groups != '')
                                        @foreach($groups as $group)
                                            <option value="{{ $group->id }}" @if($group->id == $input_data['group']){{ 'selected' }} @endif >{{ $group->group_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label>Session</label>
                                <select name="session_id" class="form-control" required>
                                    @if(isset($sessions) && $sessions != '')
                                        @foreach($sessions as $session)
                                            <option value="{{ $session->id }}" @if($session->id == $input_data['session_id']){{ 'selected' }} @endif>{{ $session->session_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label>Section</label>
                                <select name="section" class="form-control section_id" id="section_id" required>
                                    @if(isset($sections) && $sections != '')
                                        @foreach($sections as $section)
                                            <option value="{{ $section->id }}" @if($section->id == $input_data['section']){{ 'selected' }} @endif >{{ $section->section_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label>To</label>
                                <select name="promote_to" class="form-control section_id" id="" required>
                                    @if(isset($sections) && $sections != '')
                                        @foreach($sections as $section)
                                            <option value="{{ $section->id }}" @if($section->id == $input_data['promote_to']){{ 'selected' }} @endif >{{ $section->section_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <button class="btn btn-primary" style="margin-top: 24px" type="submit">Manage Promotion</button>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        {!! Form::close() !!}
                    </div>
                    <!-- /.box -->
                </div>
                <div class="col-md-6">
                    <div class="box" id="{{ $promote_from->id }}">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ $promote_from->section_name }}</h3>
                        </div>
                        <div class="box-body connectedSortable sortable"  style="height: 500px; overflow-y: scroll" id="sortable1">
                            @foreach($students as $student)
                                <div class="col-lg-4 col-xs-6" style="cursor: move" id="{{ $student->id }}">
                                    <!-- small box -->
                                    <div class="small-box bg-yellow">
                                        <div class="ic-inner-promotion">
                                            <p class="text-center ic-promotion-about">{{ $student->name }} <br> {{ $student->student_id }}</p>
                                        </div>
                                        <div class="icon ic-student-promotion">
                                            <img src="{{ Storage::url($student->student_image) }}" class="img-responsive img-thumbnail" style="height: 70px; width: 75px">
                                            {{--<i class="ion ion-person-add"></i>--}}
                                        </div>
                                        {{--<div class="ic-check">--}}
                                            {{--<input type="checkbox">--}}
                                        {{--</div>--}}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box" id="{{ $promote_to->id }}">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ $promote_to->section_name }}</h3>
                        </div>
                        <div class="box-body connectedSortable sortable" id="sortable2" style="height: 500px; overflow-y: scroll"></div>
                    </div>
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    {{--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>--}}
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('js/jquery.toaster.js') }}"></script>
    <script>
        $(function() {
            var oldList, newList, item;
            $('.sortable').sortable({
                start: function(event, ui) {
                    item = ui.item;
                    newList = oldList = ui.item.parent().parent();
                },
                stop: function(event, ui) {
                    var student_id=item.attr('id');
                    var section_id=newList.attr('id');
                    $.ajax({
                        type:'GET',
                        url:'/admin/student/promotion/update',
                        data:{'student_id':student_id, 'section_id':section_id},
                        success:function (response) {
                            $.toaster({ message : 'Student Promotion Successfully', title : 'Status', priority : 'success' });
                        },
                        error:function (response) {
                            $.toaster({ message : 'Student Promotion Failed', title : 'Status', priority : 'danger' });
                        }
                    })
                },
                change: function(event, ui) {
                    if(ui.sender) newList = ui.placeholder.parent().parent();
                },
                connectWith: ".sortable"
            }).disableSelection();
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#department_id').change(function (e) {
                $('#subject_id').html('');
                $('.section_id').html('');
                var department=$('#department_id').val();
                $.ajax({
                    type:'GET',
                    url:'/ajax_call/department_wise_data',
                    data:{
                        'department_id':department
                    },
                    success:function (response) {
                        var res=JSON.parse(response);
                        $('#group_id').html(res.groups);
                    }
                })
            });
            $('#group_id').change(function (e) {
                var department=$('#department_id').val();
                var group_id=$('#group_id').val();

                $.ajax({
                    type:'GET',
                    url:'/admin/group_wise_data',
                    data:{
                        department_id:department,
                        group_id:group_id,
                    },
                    success:function (response) {
                        console.log(response);
                        var res=JSON.parse(response);
                        $('#subject_id').html(res.subjects);
                        $('.section_id').html(res.sections);
                    }
                })
            })
        })
    </script>
@endsection