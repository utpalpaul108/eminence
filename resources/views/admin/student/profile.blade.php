@extends('admin.partials.main')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                User Profile
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">User</a></li>
                <li class="active">User profile</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-3">

                    {{--<div class="box-body text-center">--}}
                        {{--<div class="fileinput fileinput-new" data-provides="fileinput">--}}
                            {{--<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">--}}
                                {{--<img src="@if($message->profile_image != ''){{ Storage::url($message->profile_image) }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="...">--}}
                            {{--</div>--}}
                            {{--<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>--}}
                            {{--<div>--}}
                                {{--<span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>--}}
                                    {{--<input name="profile_image" type="file" >--}}
                                {{--</span>--}}
                                {{--<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <!-- Profile Image -->
                    {{--<div class="box box-primary">--}}
                        {{--<div class="box-body box-profile">--}}
                            {{--<img class="profile-user-img img-responsive img-circle" src="{{ Storage::url($student['student_image']) }}" alt="User profile picture">--}}
                            {{--<h3 class="profile-username text-center">{{ $student['name'] }}</h3>--}}
                            {{--<p class="text-muted text-center">Software Engineer</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                {!! Form::open(['action'=>'StudentProfileController@update','files'=>true]) !!}

                    <div class="box box-primary text-center" style="padding-top: 10px">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; max-height: 150px;">
                                <img src="@if($student['student_image'] != ''){{ Storage::url($student['student_image']) }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="...">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                    <input name="profile_image" type="file" >
                                </span>
                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#settings" data-toggle="tab">Settings</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane form-horizontal active" id="settings">
                                    <input type="hidden" name="student_id" value="{{ $student->id }}">
                                    <div class="form-group">
                                        <label for="name_bang" class="col-sm-2 control-label">Name (Bangla)</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="student_info[name_bn]" value="{{ $student->student_info['name_bn'] }}" class="form-control" id="name_bang" placeholder="Name In Bangla">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name_eng" class="col-sm-2 control-label">Name (English)</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="name" value="{{ $student['name'] }}" class="form-control" id="name_eng" placeholder="Name In English">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="blood_group" class="col-sm-2 control-label">Blood Group</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="student_info[blood_group]" value="{{ $student->student_info['blood_group'] }}" id="blood_group" placeholder="Blood Group">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail" class="col-sm-2 control-label">Father Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="student_info[father_name]" value="{{ $student->student_info['father_name'] }}" id="inputEmail" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail" class="col-sm-2 control-label">Mother Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="student_info[mother_name]" value="{{ $student->student_info['mother_name'] }}" id="inputEmail" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputExperience" class="col-sm-2 control-label">Present Address</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" name="student_info[present_address]" id="inputExperience" placeholder="Present Address">{!! $student->student_info['present_address'] !!}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputExperience" class="col-sm-2 control-label">Permanent Address</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" name="student_info[permanent_address]" id="inputExperience" placeholder="Permanent Address">{!! $student->student_info['permanent_address']  !!}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-success">Update</button>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->
    </div>
@endsection