@extends('admin.partials.main')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Student Promotion
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Student</a></li>
                <li class="active">Promotion</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <!-- form start -->
                        {!! Form::open(['action'=>'AdminStudentController@manage_promotion','method'=>'GET']) !!}
                        <div class="box-body">
                            <div class="form-group col-md-2">
                                <label>Department</label>
                                <select name="department" class="form-control" id="department_id" required>
                                    <option value=""> - Select -</option>
                                    @if(isset($departments) && $departments != '')
                                        @foreach($departments as $department)
                                            <option value="{{ $department->id }}">{{ $department->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label>Group</label>
                                <select name="group" class="form-control" id="group_id" required></select>
                            </div>
                            <div class="form-group col-md-2">
                                <label>Section</label>
                                <select name="section" class="form-control section_id" id="section_id" required></select>
                            </div>
                            <div class="form-group col-md-2">
                                <label>Session</label>
                                <select name="session_id" class="form-control" id="session_id" required>
                                    @if(isset($sessions) && $sessions != '')
                                        @foreach($sessions as $session)
                                            <option value="{{ $session->id }}">{{ $session->session_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label>To</label>
                                <select name="promote_to" class="form-control section_id" id="" required></select>
                            </div>
                            <div class="form-group col-md-2">
                                <button class="btn btn-primary" style="margin-top: 24px" type="submit">Manage Promotion</button>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            $('#department_id').change(function (e) {
                $('#subject_id').html('');
                $('.section_id').html('');
                var department=$('#department_id').val();
                $.ajax({
                    type:'GET',
                    url:'/ajax_call/department_wise_data',
                    data:{
                        'department_id':department
                    },
                    success:function (response) {
                        var res=JSON.parse(response);
                        $('#group_id').html(res.groups);
                    }
                })
            });
            $('#group_id').change(function (e) {
                var department=$('#department_id').val();
                var group_id=$('#group_id').val();

                $.ajax({
                    type:'GET',
                    url:'/admin/group_wise_data',
                    data:{
                        department_id:department,
                        group_id:group_id,
                    },
                    success:function (response) {
                        console.log(response);
                        var res=JSON.parse(response);
                        $('#subject_id').html(res.subjects);
                        $('.section_id').html(res.sections);
                    }
                })
            })
        })
    </script>
@endsection