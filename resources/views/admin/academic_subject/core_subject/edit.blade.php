@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Update Main Subject
            </h1>
            @if(session()->has('error'))
                <br>
                <span class="text-danger">{{ session('error') }}</span>
            @endif
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Main Subject</a></li>
                <li class="active">Update Main Subject</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>['MainSubjectsController@update',$main_subject->id], 'method'=>'put']) !!}
                <div class="col-xs-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Subject Name</label>
                                <input type="text" name="name" value="{{ $main_subject->name }}" placeholder="Enter Exam Name" id="name" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="details">About Exam</label>
                                <textarea id="details" name="details" class="form-control editor">{{ $main_subject->details }}</textarea>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        @can('edit_main_subject')
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Update</button>
                            </div>
                        @endcan
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
@endsection