
@extends('admin.partials.main')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Attendance Report
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Attendance</a></li>
                <li class="active">Attendance Report</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!-- form start -->
                        {!! Form::open(['action'=>'StudentAttendanceController@show_report','method'=>'get']) !!}
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group col-md-2">
                                    <label>Department</label>
                                    <select name="department" class="form-control" id="department_id" required>
                                        @if(isset($departments) && $departments != '')
                                            @foreach($departments as $department)
                                                <option value="{{ $department->id }}" @if($department->id == $input_data['department']){{ 'selected' }} @endif >{{ $department->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label>Group</label>
                                    <select name="group" id="group_id" class="form-control" required>
                                        @if(isset($groups) && $groups != '')
                                            @foreach($groups as $group)
                                                <option value="{{ $group->id }}" @if($group->id == $input_data['group']){{ 'selected' }} @endif >{{ $group->group_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label>Section</label>
                                    <select name="section" id="section_id" class="form-control" required>
                                        @if(isset($sections) && $sections != '')
                                            @foreach($sections as $section)
                                                <option value="{{ $section->id }}" @if($section->id == $input_data['section']){{ 'selected' }} @endif >{{ $section->section_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label>Session</label>
                                    <select name="student_session" class="form-control" required>
                                        @if(isset($sessions) && $sessions != '')
                                            @foreach($sessions as $session)
                                                <option value="{{ $session->id }}" @if($session->id == $input_data['student_session']){{ 'selected' }} @endif>{{ $session->session_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label>Subject</label>
                                    <select name="subject_id" class="form-control" id="subject_id" required>
                                        @if(isset($subjects) && $subjects != '')
                                            @foreach($subjects as $subject)
                                                <option value="{{ $subject->id }}" @if($subject->id == $input_data['subject_id']){{ 'selected' }} @endif >{{ $subject->subject_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="datepicker">Year</label>
                                    <select name="year" class="form-control" required>
                                        @for($i=2018;$i<2050;$i++)
                                            <option value="{{ $i }}" @if($previous_data['year'] == $i){{ 'selected' }} @endif>{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="datepicker">Monthe</label>
                                    <select name="month" class="form-control" required>
                                        <option value="01" @if($previous_data['month'] == '01'){{ 'selected' }} @endif>Jan</option>
                                        <option value="02" @if($previous_data['month'] == '02'){{ 'selected' }} @endif>Feb</option>
                                        <option value="03" @if($previous_data['month'] == '03'){{ 'selected' }} @endif>Mar</option>
                                        <option value="04" @if($previous_data['month'] == '04'){{ 'selected' }} @endif>April</option>
                                        <option value="05" @if($previous_data['month'] == '05'){{ 'selected' }} @endif>May</option>
                                        <option value="06" @if($previous_data['month'] == '06'){{ 'selected' }} @endif>June</option>
                                        <option value="07" @if($previous_data['month'] == '07'){{ 'selected' }} @endif>July</option>
                                        <option value="08" @if($previous_data['month'] == '08'){{ 'selected' }} @endif>August</option>
                                        <option value="09" @if($previous_data['month'] == '09'){{ 'selected' }} @endif>September</option>
                                        <option value="10" @if($previous_data['month'] == '10'){{ 'selected' }} @endif>October</option>
                                        <option value="11" @if($previous_data['month'] == '11'){{ 'selected' }} @endif>November</option>
                                        <option value="12" @if($previous_data['month'] == '12'){{ 'selected' }} @endif>December</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-1">
                                    <button class="btn btn-primary" style="margin-top: 24px" type="submit">Show</button>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        {!! Form::close() !!}
                    </div>
                    <div class="box">
                        <div class="box-header">
                            {!! Form::open(['action'=>'StudentAttendanceController@download_pdf_report']) !!}

                                <input type="hidden" name="date_from" value="{{ $date_from }}">
                                <input type="hidden" name="subject_id" value="{{ $input_data['subject_id'] }}">

                                <input type="hidden" name="department" value="{{ $input_data['department'] }}">
                                <input type="hidden" name="group" value="{{ $input_data['group'] }}">
                                <input type="hidden" name="student_session" value="{{ $input_data['student_session'] }}">
                                <input type="hidden" name="section" value="{{ $input_data['section'] }}">

                                <button class="btn btn-success pull-right" id="print_data" type="submit">Print</button>
                            {!! Form::close() !!}
                        </div>
                        <div class="box-body">
                            <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap" style="overflow: hidden">
                                <div class="row">
                                    <div class="col-sm-12 table-responsive">
                                        <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                            <thead>
                                                <tr role="row">
                                                    <th>Student | Date</th>
                                                    <th>1</th>
                                                    <th>2</th>
                                                    <th>3</th>
                                                    <th>4</th>
                                                    <th>5</th>
                                                    <th>6</th>
                                                    <th>7</th>
                                                    <th>8</th>
                                                    <th>9</th>
                                                    <th>10</th>
                                                    <th>11</th>
                                                    <th>12</th>
                                                    <th>13</th>
                                                    <th>14</th>
                                                    <th>15</th>
                                                    <th>16</th>
                                                    <th>17</th>
                                                    <th>18</th>
                                                    <th>19</th>
                                                    <th>20</th>
                                                    <th>21</th>
                                                    <th>22</th>
                                                    <th>23</th>
                                                    <th>24</th>
                                                    <th>25</th>
                                                    <th>26</th>
                                                    <th>27</th>
                                                    <th>28</th>
                                                    <th>29</th>
                                                    <th>30</th>
                                                    <th>31</th>
                                                    <th>P</th>
                                                    <th>L</th>
                                                    <th>A</th>
                                                    <th>P%</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php($sl=1)
                                                @foreach($attendances as $attendance)
                                                    <?php
                                                        $att_data=$attendance->attendance->toArray();
                                                        $att_data=array_column($att_data,'status','date');
                                                        $count_attendance=array_count_values($att_data);
                                                        if (isset($count_attendance['P'])){
                                                            $total_present=$count_attendance['P'];
                                                        }
                                                        else{
                                                            $total_present=0;
                                                        }

                                                        if (isset($count_attendance['A'])){
                                                            $total_absent=$count_attendance['A'];
                                                        }
                                                        else{
                                                            $total_absent=0;
                                                        }

                                                        if (isset($count_attendance['L'])){
                                                            $total_late=$count_attendance['L'];
                                                        }
                                                        else{
                                                            $total_late=0;
                                                        }

                                                        if (count($att_data) >0){
	                                                        $present_percent=round(($total_present/count($att_data))*100);
                                                        }
                                                        else{
	                                                        $present_percent=0;
                                                        }
                                                    ?>

                                                    <tr role="row" class="@if($sl%2 == 0){{ 'even' }} @else{{ 'odd' }} @endif">
                                                    <td class="text-nowrap">{{ $attendance->name }}</td>
                                                    <td>@if(isset($att_data[$date_from .'-01'])){{ $att_data[$date_from .'-01'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-02'])){{ $att_data[$date_from .'-02'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-03'])){{ $att_data[$date_from .'-03'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-04'])){{ $att_data[$date_from .'-04'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-05'])){{ $att_data[$date_from .'-05'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-06'])){{ $att_data[$date_from .'-06'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-07'])){{ $att_data[$date_from .'-07'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-08'])){{ $att_data[$date_from .'-08'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-09'])){{ $att_data[$date_from .'-09'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-10'])){{ $att_data[$date_from .'-10'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-11'])){{ $att_data[$date_from .'-11'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-12'])){{ $att_data[$date_from .'-12'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-13'])){{ $att_data[$date_from .'-13'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-14'])){{ $att_data[$date_from .'-14'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-15'])){{ $att_data[$date_from .'-15'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-16'])){{ $att_data[$date_from .'-16'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-17'])){{ $att_data[$date_from .'-17'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-18'])){{ $att_data[$date_from .'-18'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-19'])){{ $att_data[$date_from .'-19'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-20'])){{ $att_data[$date_from .'-20'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-21'])){{ $att_data[$date_from .'-21'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-22'])){{ $att_data[$date_from .'-22'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-23'])){{ $att_data[$date_from .'-23'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-24'])){{ $att_data[$date_from .'-24'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-25'])){{ $att_data[$date_from .'-25'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-26'])){{ $att_data[$date_from .'-26'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-27'])){{ $att_data[$date_from .'-27'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-28'])){{ $att_data[$date_from .'-28'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-29'])){{ $att_data[$date_from .'-29'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-30'])){{ $att_data[$date_from .'-30'] }} @else {{ '-' }} @endif </td>
                                                    <td>@if(isset($att_data[$date_from .'-31'])){{ $att_data[$date_from .'-31'] }} @else {{ '-' }} @endif </td>
                                                    <td>{{ $total_present }}</td>
                                                    <td>{{ $total_late }}</td>
                                                    <td>{{ $total_absent }}</td>
                                                    <td>{{ $present_percent .'%' }}</td>
                                                </tr>
                                                    @php($sl++)
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
        } );
    </script>
    <script>
        $(document).ready(function () {
            $('#department_id').change(function (e) {
                $('#subject_id').html('');
                $('#section_id').html('');
                $('#group_id').html('');
                var department=$('#department_id').val();
                $.ajax({
                    type:'GET',
                    url:'/ajax_call/department_wise_data',
                    data:{
                        'department_id':department
                    },
                    success:function (response) {
                        var res=JSON.parse(response);
                        $('#group_id').html(res.groups);
                    }
                })
            });

            $('#group_id').change(function (e) {
                $('#subject_id').html('');
                $('#section_id').html('');

                var department=$('#department_id').val();
                var group_id=$('#group_id').val();

                $.ajax({
                    type:'GET',
                    url:'/admin/group_wise_data',
                    data:{
                        department_id:department,
                        group_id:group_id,
                    },
                    success:function (response) {
                        var res=JSON.parse(response);
                        $('#section_id').html(res.sections);
                    }
                })
            });

            $('#section_id').change(function (e) {
                $('#subject_id').html('');

                var department=$('#department_id').val();
                var group_id=$('#group_id').val();
                var section_id=$('#section_id').val();

                $.ajax({
                    type:'GET',
                    url:'/admin/section_wise_data',
                    data:{
                        department_id:department,
                        group_id:group_id,
                        'section_id':section_id,
                    },
                    success:function (response) {
                        console.log(response);
                        var res=JSON.parse(response);
                        $('#subject_id').html(res.subjects);
                    }
                })
            })
        })
    </script>
@endsection