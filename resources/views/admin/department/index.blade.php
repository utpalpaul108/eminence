@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Departments
                <small>All Departments</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a href="#">Dashboard</a>
                </li>
                <li class="active">All Department</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <div class="pull-left">
                                <a id="add-button" title="Add New Department" class="btn btn-success" href="{{ action('DepartmentController@create') }}"><i class="fa fa-plus-circle"></i> Add Department</a>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Department Name</th>
                                    <th class="text-center">About Department</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($all_departments->currentPage()-1)*$all_departments->perPage())+1)
                                @foreach($all_departments as $department)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $department->name }}</td>
                                        <td>{!! str_limit($department->details,150,'(...)') !!}</td>
                                        <td>
                                            <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('DepartmentController@edit',['id'=>$department->id]) }}">
                                                Edit
                                            </a>&nbsp;&nbsp;
                                            {!! Form::open(['action'=>['DepartmentController@destroy',$department->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                                                <button title="Delete" class="btn btn-xs btn-danger delete-row" onclick="return confirm('Are You Sure ?')">
                                                    Delete
                                                </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $all_departments->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection