@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Class Routine
                <small>Routine</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a href="#">Dashboard</a>
                </li>
                <li class="active">Class Routine</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <div class="pull-left">
                                <a id="add-button" title="Add New Routine" class="btn btn-success" href="{{ action('ClassRoutineController@create') }}"><i class="fa fa-plus-circle"></i> Add Routine</a>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Routine Title</th>
                                    <th class="text-center">About Routine</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($class_routines->currentPage()-1)*$class_routines->perPage())+1)
                                @foreach($class_routines as $routine)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $routine->routine_title }}</td>
                                        <td>{!! str_limit($routine->about_routine,150,'(...)') !!}</td>
                                        <td>
                                            @can('edit_class_routine')
                                                <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('ClassRoutineController@edit',['id'=>$routine->id]) }}">
                                                    Edit
                                                </a>&nbsp;
                                            @endcan
                                            @can('delete_class_routine')
                                                {!! Form::open(['action'=>['ClassRoutineController@destroy',$routine->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                                                    <button title="Delete" class="btn btn-xs btn-danger delete-row" onclick="return confirm('Are You Sure')">
                                                        Delete
                                                    </button>
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $class_routines->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection