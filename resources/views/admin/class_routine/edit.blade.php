@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Routine
            </h1>
            @if(session()->has('error'))
                <br>
                <span class="text-danger">{{ session('error') }}</span>
            @endif
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Routine</a></li>
                <li class="active">Edit Routine</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>['ClassRoutineController@update', $routine->id], 'method'=>'PUT', 'files'=>true]) !!}
                <div class="col-xs-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="department_id">Department Name</label>
                                <select name="department_id" id="department_id" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                    @foreach($all_departments as $department)
                                        <option @if($department->id == $routine->department_id){{ 'selected' }} @endif value="{{ $department->id }}">{{ $department->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="routine_title">Routine Title</label>
                                <input type="text" name="routine_title" value="{{ $routine->routine_title }}" placeholder="Routine Title" id="routine_title" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="download_url">Upload Routine</label>
                                <input type="file" name="download_url"  id="download_url" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="about_routine">About Routine</label>
                                <textarea name="about_routine" id="about_routine" rows="5" class="editor form-control" >{{ $routine->about_routine }}</textarea>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        @can('edit_class_routine')
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Update</button>
                            </div>
                        @endcan
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
@endsection