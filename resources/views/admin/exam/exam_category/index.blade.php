@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Eaxm Categories
                <small>All Exam Categories</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a href="#">Dashboard</a>
                </li>
                <li class="active">All Exam Categories</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        @can('add_exam_category')
                            <div class="box-header">
                                <div class="pull-left">
                                    <a id="add-button" title="Add New Exam Category" class="btn btn-success" href="{{ action('ExamCategoryController@create') }}"><i class="fa fa-plus-circle"></i> Add New Exam Category</a>
                                </div>
                            </div>
                        @endcan
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Exam Name</th>
                                    <th class="text-center">About Exam</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($exam_categories->currentPage()-1)*$exam_categories->perPage())+1)
                                @foreach($exam_categories as $exam_category)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $exam_category->exam_name }}</td>
                                        <td>{!! $exam_category->details !!} </td>
                                        <td>
                                            @can('edit_exam_category')
                                                <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('ExamCategoryController@edit',['id'=>$exam_category->id]) }}">
                                                    Edit
                                                </a>&nbsp;
                                            @endcan
                                            @can('delete_exam_category')
                                                {!! Form::open(['action'=>['ExamCategoryController@destroy',$exam_category->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                                                    <button title="Delete" class="btn btn-xs btn-danger delete-row" onclick="return confirm('Are You Sure ?')">
                                                        Delete
                                                    </button>
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $exam_categories->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection