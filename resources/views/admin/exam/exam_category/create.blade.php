@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add New Exam Category
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Exam</a></li>
                <li class="active">Add New Exam Category</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>'ExamCategoryController@store']) !!}
                    <div class="col-xs-9">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="box">
                            <!-- form start -->
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exam_name">Exam Category</label>
                                    <input type="text" name="exam_name" value="{{ old('exam_name') }}" placeholder="Enter Exam Category" id="exam_name" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="exam_type">Exam Type</label>
                                    <select name="exam_type" id="exam_type" class="form-control">
                                        <option value="general">General</option>
                                        <option value="tutorial">Tutorial</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="about">About Exam</label>
                                    <textarea id="about" name="details" class="form-control editor"></textarea>
                                </div>

                            </div>
                            <!-- /.box-body -->
                            @can('add_exam_category')
                                <div class="box-footer">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            @endcan
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
@endsection