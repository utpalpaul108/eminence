@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Eaxms
                <small>All Exams</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a href="#">Dashboard</a>
                </li>
                <li class="active">All Exams</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        @can('add_exam')
                            <div class="box-header">
                                <div class="pull-left">
                                    <a id="add-button" title="Add New Exam" class="btn btn-success" href="{{ action('ExamController@create') }}"><i class="fa fa-plus-circle"></i> Add New Exam</a>
                                </div>
                            </div>
                        @endcan
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Exam Category</th>
                                    <th class="text-center">Exam Type</th>
                                    <th class="text-center">Department</th>
                                    <th class="text-center">Group</th>
                                    <th class="text-center">Section</th>
                                    <th class="text-center">Subject</th>
                                    <th class="text-center">Pass Mark</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($all_exams->currentPage()-1)*$all_exams->perPage())+1)
                                @foreach($all_exams as $exam)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $exam->exam_category['exam_name'] }}</td>
                                        <td>{{ $exam->exam_type['exam_type'] }}</td>
                                        <td>{{ $exam->department['name'] }}</td>
                                        <td>{{ $exam->group['group_name'] }}</td>
                                        <td>{{ $exam->section['section_name'] }}</td>
                                        <td>{{ $exam->subject['subject_name'] }}</td>
                                        <td>{{ $exam->pass_mark }}</td>
                                        <td>
                                            @can('edit_exam')
                                                <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('ExamController@edit',['id'=>$exam->id]) }}">
                                                    Edit
                                                </a>&nbsp;
                                            @endcan
                                            @can('delete_exam')
                                                {!! Form::open(['action'=>['ExamController@destroy',$exam->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                                                    <button title="Delete" class="btn btn-xs btn-danger delete-row" onclick="return confirm('Are You Sure ?')">
                                                        Delete
                                                    </button>
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $all_exams->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection