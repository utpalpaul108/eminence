@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Eaxm Types
                <small>All Exam Types</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a href="#">Dashboard</a>
                </li>
                <li class="active">All Exam Types</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        @can('add_exam_type')
                            <div class="box-header">
                                <div class="pull-left">
                                    <a id="add-button" title="Add New Exam Type" class="btn btn-success" href="{{ action('ExamTypesController@create') }}"><i class="fa fa-plus-circle"></i> Add New Exam Type</a>
                                </div>
                            </div>
                        @endcan
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Exam Type</th>
                                    <th class="text-center">About Exam Type</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($exam_types->currentPage()-1)*$exam_types->perPage())+1)
                                @foreach($exam_types as $exam_type)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $exam_type->exam_type }}</td>
                                        <td>{!! $exam_type->about !!} </td>
                                        <td>
                                            @can('edit_exam_type')
                                                <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('ExamTypesController@edit',['id'=>$exam_type->id]) }}">
                                                    Edit
                                                </a>&nbsp;
                                            @endcan
                                            @can('delete_exam_type')
                                                {!! Form::open(['action'=>['ExamTypesController@destroy',$exam_type->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                                                    <button title="Delete" class="btn btn-xs btn-danger delete-row" onclick="return confirm('Are You Sure ?')">
                                                        Delete
                                                    </button>
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $exam_types->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection