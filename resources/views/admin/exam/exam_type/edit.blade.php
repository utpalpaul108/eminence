@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Update Exam Type
            </h1>
            @if(session()->has('error'))
                <br>
                <span class="text-danger">{{ session('error') }}</span>
            @endif
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Exam Type</a></li>
                <li class="active">Update Exam Type</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>['ExamTypesController@update',$exam_type->id],'method'=>'put']) !!}
                <div class="col-xs-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exam_type">Exam Type</label>
                                <input type="text" name="exam_type" value="{{ $exam_type->exam_type }}" placeholder="Enter Exam Type" id="exam_type" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="about">About Exam Type</label>
                                <textarea name="about" id="about" class="form-control editor">{{ $exam_type->about }}</textarea>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        @can('edit_exam_type')
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Update</button>
                            </div>
                        @endcan
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
@endsection