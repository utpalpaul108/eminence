@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add New Exam
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Exam</a></li>
                <li class="active">Add New Exam</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>'ExamController@store']) !!}
                <div class="col-xs-9">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="department_id">Department Name</label>
                                <select name="department_id" id="department_id" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                    <option value="">-- Select Department --</option>
                                    @foreach($departments as $department)
                                        <option value="{{ $department->id }}" >{{ $department->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="group_id">Group Name</label>
                                <select name="group_id" id="group_id" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" required></select>
                            </div>
                            <div class="form-group">
                                <label for="section_id">Section Name</label>
                                <select name="section_id" id="section_id" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" required></select>
                            </div>
                            <div class="form-group">
                                <label for="subject_id">Subject Name</label>
                                <select name="subject_id" id="subject_id" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" required></select>
                            </div>
                            <div class="form-group">
                                <label for="about_section">Exam Type</label>
                                <select name="exam_type_id" id="section_id" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    @foreach($exam_types as $exam_type)
                                        <option value="{{ $exam_type->id }}">{{ $exam_type->exam_type }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group add_exam">
                                <label for="about_section">Exam Name</label>
                                <div class="row" style="margin-right: 0px">
                                    <div class="col-xs-3">
                                        <input type="number" name="pass_mark[]" class="form-control" placeholder="Pass Mark">
                                    </div>
                                    <div class="col-xs-9 input-group">
                                        <select name="exam_name[]" class="form-control" required>
                                            {!! $exam_categories !!}
                                        </select>
                                        <span class="input-group-addon plus_exam"><i class="fa fa-plus"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        @can('add_exam')
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        @endcan
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
    <script>
        $(document).ready(function () {
            var exam_categories='{!! $exam_categories !!}';
            console.log(exam_categories);
            $('#department_id').change(function (e) {
                $('#subject_id').html('');
                $('#section_id').html('');
                var department=$('#department_id').val();
                $.ajax({
                    type:'GET',
                    url:'/ajax_call/department_wise_data',
                    data:{
                        'department_id':department
                    },
                    success:function (response) {
                        var res=JSON.parse(response);
                        $('#group_id').html(res.groups);
                    }
                })
            });

            $('#group_id').change(function (e) {
                var department=$('#department_id').val();
                var group_id=$('#group_id').val();

                $.ajax({
                    type:'GET',
                    url:'/admin/group_wise_data',
                    data:{
                        department_id:department,
                        group_id:group_id,
                    },
                    success:function (response) {
                        var res=JSON.parse(response);
                        $('#section_id').html(res.sections);
                    }
                })
            });

            $('#section_id').change(function (e) {
                var department=$('#department_id').val();
                var group_id=$('#group_id').val();
                var section_id=$('#section_id').val();

                $.ajax({
                    type:'GET',
                    url:'/ajax_call/section_wise_data',
                    data:{
                        department_id:department,
                        group_id:group_id,
                        section_id:section_id,
                    },
                    success:function (response) {
                        console.log(response);
                        var res=JSON.parse(response);
                        $('#subject_id').html(res.subjects);
                    }
                })
            });

            $('.plus_exam').click(function () {
                $('.add_exam').append('<div class="row" style="margin-right: 0px; margin-top: 10px">\n' +
                    '                                    <div class="col-xs-3">\n' +
                    '                                        <input type="number" name="pass_mark[]" class="form-control" placeholder="Pass Mark">\n' +
                    '                                    </div>\n' +
                    '                                    <div class="col-xs-9 input-group">\n' +
                    '                                        <select name="exam_name[]" class="form-control" required>\n' + exam_categories +
                    '                                        </select>\n' +
                    '                                        <span class="input-group-addon plus_exam" onclick="$(this).parent().parent().remove()"><i class="fa fa-minus"></i></span>\n' +
                    '                                    </div>\n' +
                    '                                </div>');
            });
        })
    </script>
@endsection