@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Examination Schedule
                <small>All Examinations</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a href="#">Dashboard</a>
                </li>
                <li class="active">All Examinations</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        @can('add_exam_schedule')
                            <div class="box-header">
                                <div class="pull-left">
                                    <a id="add-button" title="Add New Subject" class="btn btn-success" href="{{ action('ExamScheduleController@create') }}"><i class="fa fa-plus-circle"></i> Add New Exam Schedule</a>
                                </div>
                            </div>
                        @endcan
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Exam Name</th>
                                    <th class="text-center">Date</th>
                                    <th class="text-center">Time</th>
                                    <th class="text-center">Duration</th>
                                    <th class="text-center">Department</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($all_exams->currentPage()-1)*$all_exams->perPage())+1)
                                @foreach($all_exams as $exam)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $exam->exam_name }}</td>
                                        <td>{{ $exam->exam_date }}</td>
                                        <td>{{ $exam->exam_time }}</td>
                                        <td>{{ $exam->duration }}</td>
                                        <td>{{ $exam->department['name'] }}</td>
                                        <td>
                                            @can('edit_exam_schedule')
                                                <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('ExamScheduleController@edit',['id'=>$exam->id]) }}">
                                                    Edit
                                                </a>&nbsp;
                                            @endcan
                                            @can('delete_exam_schedule')
                                                {!! Form::open(['action'=>['ExamScheduleController@destroy',$exam->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                                                    <button title="Delete" class="btn btn-xs btn-danger delete-row" onclick="return confirm('Are You Sure')">
                                                        Delete
                                                    </button>
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $all_exams->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection