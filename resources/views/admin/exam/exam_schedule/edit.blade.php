@extends('admin.partials.main')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Exam
            </h1>
            @if(session()->has('error'))
                <br>
                <span class="text-danger">{{ session('error') }}</span>
            @endif
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Examination</a></li>
                <li class="active">Edit Exam</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>['ExamScheduleController@update', $exam->id], 'method'=>'PUT']) !!}
                <div class="col-xs-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="department_id">Department Name</label>
                                <select name="department_id" id="department_id" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    <option value="" >--- Select Department --- </option>
                                    @foreach($all_departments as $department)
                                        <option value="{{ $department->id }}" @if($department->id == $exam->department_id){{ 'selected' }} @endif>{{ $department->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="group_id">Group Name</label>
                                <select name="group_id" id="group_id" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    @foreach($all_groups as $group)
                                        <option value="{{ $group->id }}" @if($group->id == $exam->group_id){{ 'selected' }} @endif >{{ $group->group_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="section_id">Section Name</label>
                                <select name="section_id" id="section_id" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    @foreach($all_sections as $section)
                                        <option value="{{ $section->id }}" @if($section->id == $exam->section_id){{ 'selected' }} @endif >{{ $section->section_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exam_name">Exam Name</label>
                                <input type="text" name="exam_name" value="{{ $exam->exam_name }}" placeholder="Enter Exam Name" id="exam_name" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="exam_date">Exam Date</label>
                                <input type="text" name="exam_date" value="{{ $exam->exam_date }}" placeholder="Enter Exam Date" id="exam_date" class="form-control datepicker" required>
                            </div>
                            <div class="form-group">
                                <label for="exam_time">Exam Time</label>
                                <input type="text" name="exam_time" value="{{ $exam->exam_time }}" placeholder="Enter Exam Time" id="exam_time" class="form-control timepicker" required>
                            </div>
                            <div class="form-group">
                                <label for="duration">Duration</label>
                                <input type="text" name="duration" value="{{ $exam->duration }}" placeholder="Enter Exam Duration" id="duration" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="total_marks">Total Marks</label>
                                <input type="text" name="total_marks" value="{{ $exam->total_marks }}" placeholder="Enter Total Marks" id="total_marks" class="form-control" required>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        @can('edit_exam_schedule')
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Update</button>
                            </div>
                        @endcan
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(document).ready(function () {
            $( function() {
                $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
            } );

            $('#department_id').change(function (e) {
                $('#subject_id').html('');
                $('#section_id').html('');
                var department=$('#department_id').val();
                $.ajax({
                    type:'GET',
                    url:'/ajax_call/department_wise_data',
                    data:{
                        'department_id':department
                    },
                    success:function (response) {
                        var res=JSON.parse(response);
                        $('#group_id').html(res.groups);
                    }
                })
            });
            $('#group_id').change(function (e) {
                var department=$('#department_id').val();
                var group_id=$('#group_id').val();

                $.ajax({
                    type:'GET',
                    url:'/admin/group_wise_data',
                    data:{
                        department_id:department,
                        group_id:group_id,
                    },
                    success:function (response) {
                        console.log(response);
                        var res=JSON.parse(response);
                        $('#subject_id').html(res.subjects);
                        $('#section_id').html(res.sections);
                    }
                })
            })
        })
    </script>
@endsection