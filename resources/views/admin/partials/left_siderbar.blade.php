<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ Storage::url(Auth::user()->profile_image) }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li>
                <a href="#">
                    <i class="fa fa-home"></i> <span>Dashboard</span>
                </a>
            </li>

            {{-- For Website Management --}}
            {{-- ================================== --}}

            @can('manage_management')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-user"></i>
                        <span>Management</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    @can('manage_department')
                        <ul class="treeview-menu">
                            <li><a href="{{ action('DepartmentController@index') }}"><i class="fa fa-circle-o"></i> Department</a></li>
                        </ul>
                    @endcan
                    @can('manage_study_group')
                        <ul class="treeview-menu">
                            <li><a href="{{ action('StudyGroupController@index') }}"><i class="fa fa-circle-o"></i> Group</a></li>
                        </ul>
                    @endcan
                    @can('manage_section')
                        <ul class="treeview-menu">
                            <li><a href="{{ action('AcademicSectionController@index') }}"><i class="fa fa-circle-o"></i> Academic Section</a></li>
                        </ul>
                    @endcan
                    @can('manage_session')
                        <ul class="treeview-menu">
                            <li><a href="{{ action('AcademicSessionController@index') }}"><i class="fa fa-circle-o"></i> Academic Session</a></li>
                        </ul>
                    @endcan
                    @can('manage_main_subject')
                        <ul class="treeview-menu">
                            <li><a href="{{ action('MainSubjectsController@index') }}"><i class="fa fa-circle-o"></i> Main Subject</a></li>
                        </ul>
                    @endcan
                    @can('manage_subject')
                        <ul class="treeview-menu">
                            <li><a href="{{ action('SubjectController@index') }}"><i class="fa fa-circle-o"></i> Academic Subject</a></li>
                        </ul>
                    @endcan
                </li>
            @endcan
            @can('manage_users')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>Users</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ action('UserController@index') }}"><i class="fa fa-circle-o"></i> All Users</a></li>
                        @can('manage_role')
                            <li><a href="{{ action('RoleController@index') }}"><i class="fa fa-circle-o"></i> All Role</a></li>
                        @endcan
                        @can('assign_subject')
                            <li><a href="{{ action('UserController@assign_subject') }}"><i class="fa fa-circle-o"></i> Assign Subject</a></li>
                        @endcan
                            {{--@can('manage_permission')--}}
                            {{--<li><a href="{{ action('PermissionController@index') }}"><i class="fa fa-circle-o"></i> All Permissions</a></li>--}}
                        {{--@endcan--}}
                    </ul>
                </li>
            @endcan
            @can('manage_students')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-graduation-cap"></i>
                    <span>Student</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ action('AdminStudentController@index') }}"><i class="fa fa-circle-o"></i> All Student</a></li>
                    @can('add_student')
                        <li><a href="{{ action('AdminStudentController@create') }}"><i class="fa fa-circle-o"></i> Add New</a></li>
                    @endcan
                    @can('student_promotion')
                        <li><a href="{{ action('AdminStudentController@promotion') }}"><i class="fa fa-circle-o"></i> Manage Promotion</a></li>
                    @endcan
                </ul>
            </li>
            @endcan
            @can('student_attendance')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-check-square"></i>
                        <span>Attendance</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ action('StudentAttendanceController@index') }}"><i class="fa fa-circle-o"></i> Daily Attendance</a></li>
                        @can('attendance_report')
                            <li><a href="{{ action('StudentAttendanceController@report') }}"><i class="fa fa-circle-o"></i> Attendance Report</a></li>
                        @endcan
                    </ul>
                </li>
            @endcan
            @can('manage_student_payment')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-credit-card"></i>
                        <span>Student Payment</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    @can('add_payment')
                        <ul class="treeview-menu">
                            <li><a href="{{ action('StudentPaymentController@create') }}"><i class="fa fa-circle-o"></i> Upload Payment</a></li>
                        </ul>
                    @endcan

                    {{--<ul class="treeview-menu">--}}
                        {{--<li><a href="{{ action('StudentPaymentController@index') }}"><i class="fa fa-circle-o"></i> All Payments</a></li>--}}
                    {{--</ul>--}}

                    {{--@can('manage_payment_structure')--}}
                        {{--<ul class="treeview-menu">--}}
                            {{--<li><a href="{{ action('PaymentStructureController@index') }}"><i class="fa fa-circle-o"></i> Payment Structure</a></li>--}}
                        {{--</ul>--}}
                    {{--@endcan--}}
                </li>
            @endcan
            @can('manage_exam')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-file"></i>
                    <span>Exam</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                @can('manage_exam_type')
                    <ul class="treeview-menu">
                        <li><a href="{{ action('ExamTypesController@index') }}"><i class="fa fa-circle-o"></i> Exam Types</a></li>
                    </ul>
                @endcan
                @can('manage_exam_category')
                    <ul class="treeview-menu">
                        <li><a href="{{ action('ExamCategoryController@index') }}"><i class="fa fa-circle-o"></i> Exam Category</a></li>
                    </ul>
                @endcan
                <ul class="treeview-menu">
                    <li><a href="{{ action('ExamController@index') }}"><i class="fa fa-circle-o"></i> All Exams</a></li>
                </ul>

                {{--<ul class="treeview-menu">--}}
                {{--<li><a href="{{ action('ExamScheduleController@index') }}"><i class="fa fa-circle-o"></i> All Exam Schedule</a></li>--}}
                {{--</ul>--}}
            </li>
            @endcan

            @can('manage_result')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-check"></i>
                        <span>Result</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="{{ action('HscResultController@index') }}"><i class="fa fa-circle-o"></i> HSC Result</a></li>--}}
                    {{--</ul>--}}
                    {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="{{ action('ResultController@index') }}"><i class="fa fa-circle-o"></i> All Results</a></li>--}}
                    {{--</ul>--}}
                    @can('manage_result_grade')
                        <ul class="treeview-menu">
                            <li><a href="{{ action('ResultGradeController@index') }}"><i class="fa fa-circle-o"></i> Result Grade</a></li>
                        </ul>
                    @endcan
                    @can('upload_result')
                        <ul class="treeview-menu">
                            <li><a href="{{ action('ResultController@create') }}"><i class="fa fa-circle-o"></i> Upload Result</a></li>
                        </ul>
                    @endcan
                </li>
                @can('result_report')
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-flag-checkered"></i>
                            <span>Report</span>
                            <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ action('ResultReportController@index') }}"><i class="fa fa-circle-o"></i> Result Report</a></li>
                        </ul>
                    </li>
                @endcan
            @endcan

            {{--      For Admin Panel Management     --}}
            {{-- ================================== --}}

            @can('manage_administration')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-unlock"></i>
                        <span>Administration</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    @can('manage_config')
                        <ul class="treeview-menu">
                            <li class="treeview">
                                <a href="#"><i class="fa fa-circle-o"></i> Config
                                    <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                                </a>
                                <ul class="treeview-menu">
                                    @can('edit_header')
                                        <li><a href="{{ action('AdminSectionController@show',['id'=>'header']) }}"><i class="fa fa-circle-o"></i> Header</a></li>
                                    @endcan
                                    @can('edit_footer')
                                        <li><a href="{{ action('AdminSectionController@show',['id'=>'footer']) }}"><i class="fa fa-circle-o"></i> Footer</a></li>
                                    @endcan
                                </ul>
                            </li>
                        </ul>
                    @endcan
                    @can('manage_latest_news')
                        <ul class="treeview-menu">
                            <li><a href="{{ action('LatestNewsController@index') }}"><i class="fa fa-circle-o"></i> Latest News</a></li>
                        </ul>
                    @endcan
                    @can('manage_hot_news')
                        <ul class="treeview-menu">
                            <li><a href="{{ action('HotNewsController@index') }}"><i class="fa fa-circle-o"></i> Hot News</a></li>
                        </ul>
                    @endcan
                    @can('manage_notice_board')
                        <ul class="treeview-menu">
                            <li><a href="{{ action('NoticeBoardController@index') }}"><i class="fa fa-circle-o"></i> All Notice</a></li>
                        </ul>
                    @endcan
                    @can('manage_message')
                        <ul class="treeview-menu">
                            <li><a href="{{ action('MessageForUniversityController@index') }}"><i class="fa fa-circle-o"></i> Message For University</a></li>
                        </ul>
                    @endcan
                </li>
            @endcan
            @can('manage_photo_gallery')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-picture-o"></i>
                        <span>Photo Gallery</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    @can('manage_photo_gallery')
                        <ul class="treeview-menu">
                            <li><a href="{{ action('PhotoCategoryController@index') }}"><i class="fa fa-circle-o"></i> Photo Category</a></li>
                        </ul>
                    @endcan
                    <ul class="treeview-menu">
                        <li><a href="{{ action('PhotoGalleryController@index') }}"><i class="fa fa-circle-o"></i> Photo Gallery</a></li>
                    </ul>
                </li>
            @endcan
            @can('manage_pages')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-file-text"></i>
                        <span>Page</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ action('AdminPageController@index') }}"><i class="fa fa-circle-o"></i> All Pages</a></li>
                    </ul>
                </li>
            @endcan
            @can('manage_slider')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-sliders"></i>
                        <span>Slider</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ action('AdminSliderController@index') }}"><i class="fa fa-circle-o"></i> All Sliders</a></li>
                    </ul>
                </li>
            @endcan
            @can('manage_achievement')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-trophy"></i>
                        <span>Achievement</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ action('CollegeAchievementController@index') }}"><i class="fa fa-circle-o"></i> College Achievements</a></li>
                    </ul>
                </li>
            @endcan
            @can('manage_class_routine')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-calendar"></i>
                        <span>Class Routine</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ action('ClassRoutineController@index') }}"><i class="fa fa-circle-o"></i> All Class Routine</a></li>
                    </ul>
                </li>
            @endcan
            @can('manage_course_outline')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-tasks"></i>
                        <span>Course Outline</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ action('CourseOutlineController@index') }}"><i class="fa fa-circle-o"></i> All Courses</a></li>
                    </ul>
                </li>
            @endcan
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>