<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.3.6
    </div>
    <strong>Copyright &copy; 2018 <a href="http://itclanbd.com">ITClanBD</a>.</strong> All rights
    reserved.
</footer>