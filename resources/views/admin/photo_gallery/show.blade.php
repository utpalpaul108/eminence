@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Image Gallery
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Image Gallery</a></li>
                <li class="active">Image</li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>'PhotoGalleryController@store','files'=>true]) !!}
                <div class="col-xs-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Category Name</label>
                                <select class="form-control" name="category_id" id="category_id">
                                    <option value="all">All</option>
                                    @foreach($all_categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <label for="inputEmail3" class="col-sm-12 control-label ic_padding_left_null" style="padding-left: 0px">Gallery Images</label>
                            <div class="OurClientGroup ImageGroup" id="sortable">
                                @if(isset($photo_gallery) && $photo_gallery != null)
                                    @php($sl=0)
                                    @foreach($photo_gallery as $gallery)
                                        <div class="box-body eachClient curser_move" id="{{ $gallery->id }}" data-token="{{ csrf_token() }}">
                                            <div class="fileinput fileinput-new ic_our_clients" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="max-width: 150px; max-height: 150px;">
                                                    <img src="@if($gallery->photo != ''){{ Storage::url($gallery->photo) }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="...">
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                                                <div class="text-center">
                                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                                    <input name="gallery_photo{{ $sl }}" type="file">
                                                    <input type="hidden" name="previous_image[]" value="{{ $gallery->photo }}">
                                                    <input type="hidden" name="image_id[]" value="{{ $gallery->id }}">
                                                </span>
                                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                                <div class="form-group ic_remove_fig">
                                                    <i class="fa fa-minus-circle text-danger" aria-hidden="true" onclick="if (confirm('Are You Sure ?')){ deleteImage({{ $gallery->id }})}"></i>
                                                </div>
                                            </div>
                                        </div>
                                        @php($sl++)
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group col-sm-12 pull-left ic_padding_left_null" id="add_image" style="padding-top: 15px; padding-left: 0px; display: none">
                                <button type="button" id="addClient" class="btn btn-xs btn-success"><i class="fa fa-plus-circle"></i> Add New Image</button>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('ic_admin/js/jquery.toaster.js') }}"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
    <script>
        $('#addClient').click(function () {
            var recentCounter = $('.eachClient').length;

            var data='<div class="box-body eachClient">\n' +
                '                                    <div class="fileinput fileinput-new ic_our_clients" data-provides="fileinput">\n' +
                '                                        <div class="fileinput-new thumbnail" style="max-width: 150px; max-height: 150px;">\n' +
                '                                            <img src="http://placehold.it/200x200 " width="100%" alt="...">\n' +
                '                                        </div>\n' +
                '                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 150px; max-height: 150px;"></div>\n' +
                '                                        <div class="text-center">\n' +
                '                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>\n' +
                '                                    <input name="gallery_photo'+ recentCounter+'" type="file">\n' +
                '                                    <input type="hidden" name="previous_image[]" value="">\n' +
                '                                </span>\n' +
                '                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>\n' +
                '                                        </div>\n' +
                '                                        <div class="form-group ic_remove_fig">\n' +
                '                                            <i class="fa fa-minus-circle text-danger" aria-hidden="true" onclick="if (confirm(\'Are You Sure ?\')){ $(this).parent().parent().remove();}"></i>\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '                                </div>';
            $('.OurClientGroup').append(data);
        });

        function deleteImage(e) {
            var token=$('#'+e).data('token');
            $.ajax({
                type:'post',
                url:'/admin/delete_photo',
                data:{_token:token,id:e}
            })
            $('#'+e).remove();
        }

        $(document).ready(function () {
            $('#category_id').change(function () {
                var category_id=$('#category_id').val();
                if (category_id == 'all'){
                    $('#add_image').hide();
                }
                else {
                    $('#add_image').show();
                }

                $.ajax({
                    type:'get',
                    url:'/ajax_call/category_wise_image',
                    data:{
                        category_id:category_id
                    },
                    success:function (response) {
                        var res=JSON.parse(response);
                        $('.ImageGroup').html(res.all_images);
                    }
                })
            })
        })
    </script>
    <script>
        $( function() {
            $("#sortable").sortable({
                stop: function(){
                    $.map($(this).find('.curser_move'), function(el) {
                        var itemID = el.id;
                        var itemIndex = $(el).index();
                        var category_type=$('#category_id :selected').val();
                        $.ajax({
                            url:'{{URL::to("admin/gallery_image/order")}}',
                            type:'GET',
                            dataType:'json',
                            data: {itemID:itemID, itemIndex: itemIndex, category_type: category_type},
                        })
                    });
                    $.toaster('Order Changed Successfully!', 'Status');
                }
            });
            $("#sortable").disableSelection();
        } );
    </script>
@endsection