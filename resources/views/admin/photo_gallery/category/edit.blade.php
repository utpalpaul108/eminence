@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Category
            </h1>
            @if(session()->has('error'))
                <br>
                <span class="text-danger">{{ session('error') }}</span>
            @endif
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Photo Category</a></li>
                <li class="active">Edit Category</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>['PhotoCategoryController@update',$photo_category->id],'method'=>'PUT']) !!}
                <div class="col-xs-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Category Name</label>
                                <input type="text" name="name" value="{{ $photo_category['name'] }}" placeholder="Enter Category Name" id="name" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="category_class">Category Class</label>
                                <input type="text" name="category_class" value="{{ $photo_category['category_class'] }}" placeholder="Enter Category Class" id="category_class" class="form-control" required>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        @can('edit_photo_category')
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Update</button>
                            </div>
                        @endcan
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection