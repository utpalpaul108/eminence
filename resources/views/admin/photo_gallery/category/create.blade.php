@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add New Category
            </h1>
            @if(session()->has('error'))
                <br>
                <span class="text-danger">{{ session('error') }}</span>
            @endif
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Photo Category</a></li>
                <li class="active">Add New Category</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>'PhotoCategoryController@store']) !!}
                <div class="col-xs-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Category Name</label>
                                <input type="text" name="name" value="{{ old('name') }}" placeholder="Enter Category Name" id="name" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="category_class">Class Name</label>
                                <input type="text" name="category_class" value="{{ old('category_class') }}" placeholder="Enter Category Class" id="category_class" class="form-control" required>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        @can('add_photo_category')
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        @endcan
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
@endsection