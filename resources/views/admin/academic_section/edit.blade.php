@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Study Group
            </h1>
            @if(session()->has('error'))
                <br>
                <span class="text-danger">{{ session('error') }}</span>
            @endif
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Study Group</a></li>
                <li class="active">Edit Study Group</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>['AcademicSectionController@update',$section->id],'method'=>'PUT']) !!}
                <div class="col-xs-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="department_id">Department Name</label>
                                <select name="department_id" id="department_id" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                    @foreach($all_departments as $department)
                                        <option value="{{ $department->id }}" @if($section->department_id == $department->id){{ 'selected' }} @endif >{{ $department->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="group_id">Group Name</label>
                                <select name="group_id" id="group_id" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                    @foreach($all_groups as $group)
                                        <option value="{{ $group->id }}" @if($section->group_id == $group->id){{ 'selected' }} @endif >{{ $group->group_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="section_name">Section Name</label>
                                <input type="text" name="section_name" value="{{ $section->section_name }}" placeholder="Enter Section Name" id="section_name" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="about_section">About Section</label>
                                <textarea name="about_section" id="about_section" rows="5" class="editor form-control" >{{ $section->about_section }}</textarea>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        @can('edit_section')
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Update</button>
                            </div>
                        @endcan
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
    <script>
        $('#department_id').change(function () {
            var department_id = $('#department_id').val();
            $.ajax({
                type:'GET',
                url:'/admin/department_wise_group',
                data:{department_id:department_id},
                success:function (response) {
                    var res=JSON.parse(response);
                    $('#group_id').html(res.groups)
                }
            })
        });
    </script>
@endsection