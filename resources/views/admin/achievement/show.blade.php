@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                College Achievement
                <small>All College Achievements</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a href="#">Dashboard</a>
                </li>
                <li class="active">All College Achievements</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <div class="pull-left">
                                <a id="add-button" title="Add New Achievement" class="btn btn-success" href="{{ action('CollegeAchievementController@create') }}"><i class="fa fa-plus-circle"></i> Add New Achievement</a>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Year</th>
                                    <th class="text-center">Appeared</th>
                                    <th class="text-center">Passed</th>
                                    <th class="text-center">Percentage</th>
                                    <th class="text-center">Total A+</th>
                                    <th class="text-center">Total Golden A+</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($achievements->currentPage()-1)*$achievements->perPage())+1)
                                @foreach($achievements as $achievement)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $achievement->year }}</td>
                                        <td>{{ $achievement->appeared }}</td>
                                        <td>{{ $achievement->passed }}</td>
                                        <td>{{ $achievement->percentage }}</td>
                                        <td>{{ $achievement->total_a_plus }}</td>
                                        <td>{{ $achievement->golden_a_plus }}</td>
                                        <td>
                                            @can('edit_achievement')
                                                <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('CollegeAchievementController@edit',['id'=>$achievement->id]) }}">
                                                    Edit
                                                </a>&nbsp;
                                            @endcan
                                            @can('delete_achievement')
                                                {!! Form::open(['action'=>['CollegeAchievementController@destroy',$achievement->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                                                    <button title="Delete" class="btn btn-xs btn-danger delete-row" onclick="return confirm('Are You Sure')">
                                                        Delete
                                                    </button>
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $achievements->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection