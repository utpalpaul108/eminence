@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Achievement
            </h1>
            @if(session()->has('error'))
                <br>
                <span class="text-danger">{{ session('error') }}</span>
            @endif
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Achievement</a></li>
                <li class="active">Edit Achievement</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>['CollegeAchievementController@update',$achievement->id],'method'=>'PUT']) !!}
                    <div class="col-xs-9">
                        <div class="box">
                            <!-- form start -->
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="group_name">Group Name</label>
                                    <select name="group_id" class="form-control" tabindex="-1" aria-hidden="true">
                                        @foreach($all_groups as $group)
                                            <option value="{{ $group->id }}" @if($group->id == $achievement->group_id){{ 'selected' }} @endif>{{ $group->group_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="year">Year</label>
                                    <input type="text" name="year" value="{{ $achievement->year }}" placeholder="Enter Acievement Year" id="year" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="appeared">Appeared</label>
                                    <input type="text" name="appeared" value="{{ $achievement->appeared }}" placeholder="Enter Appeared Number" id="appeared" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="passed">Total Passed</label>
                                    <input type="text" name="passed" value="{{ $achievement->passed }}" placeholder="Enter Total Passed" id="passed" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="percentage">Percentage</label>
                                    <input type="text" name="percentage" value="{{ $achievement->percentage }}" placeholder="Enter Passed Percentage" id="percentage" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="total_a_plus">Total A+</label>
                                    <input type="text" name="total_a_plus" value="{{ $achievement->total_a_plus }}" placeholder="Enter Total A+" id="total_a_plus" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="golden_a_plus">Golden A+</label>
                                    <input type="text" name="golden_a_plus" value="{{ $achievement->golden_a_plus }}" placeholder="Enter Total Golden A+" id="total_golden_a_plus" class="form-control" required>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            @can('edit_achievement')
                                <div class="box-footer">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            @endcan
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection
