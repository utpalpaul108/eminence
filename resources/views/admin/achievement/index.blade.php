@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                College Achievements
                <small>Group Wise Achievements</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a href="#">Dashboard</a>
                </li>
                <li class="active">Group Wise Achievements</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        @can('add_achievement')
                            <div class="box-header">
                                <div class="pull-left">
                                    <a id="add-button" title="Add New Achievement" class="btn btn-success" href="{{ action('CollegeAchievementController@create') }}"><i class="fa fa-plus-circle"></i> Add New Achievement</a>
                                </div>
                            </div>
                        @endcan
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Group Name</th>
                                    <th class="text-center">About Group</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($all_study_groups->currentPage()-1)*$all_study_groups->perPage())+1)
                                @foreach($all_study_groups as $study_group)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $study_group->group_name }}</td>
                                        <td>{!! str_limit($study_group->about_group,150,'(...)') !!}</td>
                                        @can('view_achievement')
                                            <td>
                                                <a title="View" class="btn btn-xs btn-primary edit-row" href="{{ action('CollegeAchievementController@show',['id'=>$study_group->id]) }}">
                                                    View
                                                </a>&nbsp;
                                            </td>
                                        @endcan
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $all_study_groups->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection