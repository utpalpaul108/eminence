@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Message
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Message</a></li>
                <li class="active">Edit Message</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>['MessageForUniversityController@update',$message->id],'method'=>'PUT','files'=>true]) !!}
                <div class="col-md-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <div class="form-group">
                                                <h5>{{ $error }}</h5>
                                            </div>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="message_title">Message Title</label>
                                <input type="text" name="message_title" value="{{ $message->message_title }}" placeholder="Enter Message Title" id="message_title" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="provide_by">Message Provide By</label>
                                <input type="text" name="provide_by" value="{{ $message->provide_by }}" placeholder="Enter Provide By" id="provide_by" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="qualification">Messenger Qualification </label>
                                <input type="text" name="qualification" value="{{ $message->qualification }}" placeholder="Enter Qualification" id="qualification" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="designation">Messenger Designation </label>
                                <input type="text" name="designation" value="{{ $message->designation }}" placeholder="Enter Designation" id="designation" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="message">Message</label>
                                <textarea name="message" id="message" rows="5" class="editor form-control" required>{{ $message->message }}</textarea>
                            </div>
                        </div>
                        @can('edit_message')
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Update</button>
                            </div>
                        @endcan
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Profile Image</h3>
                        </div>
                        <div class="box-body text-center">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="@if($message->profile_image != ''){{ Storage::url($message->profile_image) }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="...">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                <div>
                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                    <input name="profile_image" type="file" >
                                </span>
                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
@endsection