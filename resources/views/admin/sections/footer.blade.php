@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Footer Section
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Section</a></li>
                <li class="active">Footer</li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>['AdminSectionController@update',$section->id],'method'=>'PUT']) !!}
                <div class="col-xs-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="first_section_title">Title For First Section</label>
                                <input type="text" name="contents[first_section][title]" value="@if(isset($section->contents['first_section']['title'])){{ $section->contents['first_section']['title'] }} @endif" placeholder="First Section Title" id="first_section_title" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="address">Address</label>
                                <textarea class="form-control" rows="3" name="contents[first_section][address]" placeholder="Enter Address" id="address" required>@if(isset($section->contents['first_section']['address'])){{ $section->contents['first_section']['address'] }} @endif</textarea>
                            </div>
                            <div class="PhoneNumberGroup">
                                @if(isset($section->contents['phone_number']))
                                    @php($sl=0)
                                    @foreach($section->contents['phone_number'] as $phone_number)
                                        <div class="form-group eachPhoneNumber ic_clear ic_padding_top">
                                            <div class="col-sm-10 ic_padding_left_null" style="padding-left: 0px">
                                                <input name="contents[phone_number][{{ $sl }}]" value="{{ $phone_number }}" class="form-control input-sm" placeholder="Phone Number" type="text">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button" class="btn btn-xs btn-danger" onclick="if (confirm('Are You Sure ?')){ $(this).parent().parent().remove();}"><i class="fa fa-trash-o"></i></button>
                                            </div>
                                        </div>
                                        @php($sl++)
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group col-sm-12 pull-left ic_padding_left_null" style="padding-top: 15px; padding-left: 0px">
                                <button type="button" id="addPhone" class="btn btn-xs btn-success"><i class="fa fa-plus-circle"></i> Add Phone</button>
                            </div>
                            <div class="MobileNumberGroup">
                                @if(isset($section->contents['mobile_number']))
                                    @php($sl=0)
                                    @foreach($section->contents['mobile_number'] as $mobile_number)
                                        <div class="form-group eachMobileNumber ic_clear ic_padding_top">
                                            <div class="col-sm-10 ic_padding_left_null" style="padding-left: 0px">
                                                <input name="contents[mobile_number][{{ $sl }}]" value="{{ $mobile_number }}" class="form-control input-sm" placeholder="Phone Number" type="text">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button" class="btn btn-xs btn-danger" onclick="if (confirm('Are You Sure ?')){ $(this).parent().parent().remove();}"><i class="fa fa-trash-o"></i></button>
                                            </div>
                                        </div>
                                        @php($sl++)
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group col-sm-12 pull-left ic_padding_left_null" style="padding-top: 15px; padding-left: 0px">
                                <button type="button" id="addMobile" class="btn btn-xs btn-success"><i class="fa fa-plus-circle"></i> Add Mobile</button>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                    <div class="box-body">
                            <div class="form-group">
                                <label for="second_section_title">Title For Second Section</label>
                                <input type="text" name="contents[second_section_title]" value="@if(isset($section->contents['second_section_title'])){{ $section->contents['second_section_title'] }} @endif" placeholder="Second Section Title" id="second_section_title" class="form-control" required>
                            </div>
                            <label for="inputEmail3" class="col-sm-12 control-label ic_padding_left_null" style="padding-left: 0px">Quick Links</label>
                            <div class="QuickLinksGroup">
                                @if(isset($section->contents['quick_links']))
                                    @php($sl=0)
                                    @foreach($section->contents['quick_links'] as $quick_link)
                                        <div class="form-group eachLink ic_clear ic_padding_top">
                                            <div class="col-sm-5 ic_padding_left_null" style="padding-left: 0px">
                                                <input name="contents[quick_links][{{ $sl }}][name]" value="{{ $quick_link['name'] }}" class="form-control input-sm" placeholder="Quick Link Name" type="text">
                                            </div>
                                            <div class="col-sm-5">
                                                <input name="contents[quick_links][{{ $sl }}][url]" value="{{ $quick_link['url'] }}" class="form-control input-sm" placeholder="Quick Link URL" type="text">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button" class="btn btn-xs btn-danger" onclick="if (confirm('Are You Sure ?')){ $(this).parent().parent().remove();}"><i class="fa fa-trash-o"></i></button>
                                            </div>
                                        </div>
                                        @php($sl++)
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group col-sm-12 pull-left ic_padding_left_null" style="padding-top: 15px; padding-left: 0px">
                                <button type="button" id="addLink" class="btn btn-xs btn-success"><i class="fa fa-plus-circle"></i> Add New Link</button>
                            </div>
                    </div>
                    </div>
                    <div class="box">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="third_section_title">Title For Third Section</label>
                                <input type="text" name="contents[third_section_title]" value="@if(isset($section->contents['third_section_title'])){{ $section->contents['third_section_title'] }} @endif" placeholder="Third Section Title" id="third_section_title" class="form-control" required>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="founder_name">Edit Location</label>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="latitudinal">Latitudinal </label>
                                <input type="text" name="contents[latitudinal]" value="@if(isset($section->contents['latitudinal'])){{ $section->contents['latitudinal'] }} @endif" placeholder="Latitudinal" id="latitudinal" class="form-control" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="longitudinal">Longitudinal</label>
                                <input type="text" name="contents[longitudinal]" value="@if(isset($section->contents['longitudinal'])){{ $section->contents['longitudinal'] }} @endif" placeholder="Longitudinal" id="longitudinal" class="form-control" required>
                            </div>

</div>
                    </div>
                    <div class="box">
                    <div class="box-body">
                            <div class="form-group">
                                <label for="bottom_section_title">Title For Bottom Section</label>
                                <input type="text" name="contents[bottom_title]" value="@if(isset($section->contents['bottom_title'])){{ $section->contents['bottom_title'] }} @endif" placeholder="Bottom Section Title" id="bottom_section_title" class="form-control" required>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
    <script>
        //    For Quick Link Section
        $('#addLink').click(function () {
            var recentCounter = $('.eachLink').length;

            var data='<div class="form-group eachLink ic_clear ic_padding_top">\n' +
                '                                            <div class="col-sm-5 ic_padding_left_null" style="padding-left: 0px">\n' +
                '                                                <input name="contents[quick_links]['+recentCounter+'][name]" class="form-control input-sm" placeholder="Quick Link Name" type="text">\n' +
                '                                            </div>\n' +
                '                                            <div class="col-sm-5">\n' +
                '                                                <input name="contents[quick_links]['+recentCounter+'][url]" class="form-control input-sm" placeholder="Quick Link URL" type="text">\n' +
                '                                            </div>\n' +
                '                                            <div class="col-sm-2">\n' +
                '                                                <button type="button" class="btn btn-xs btn-danger" onclick="if (confirm(\'Are You Sure ?\')){ $(this).parent().parent().remove();}"><i class="fa fa-trash-o"></i></button>\n' +
                '                                            </div>\n' +
                '                                        </div>';
            $('.QuickLinksGroup').append(data);
        });

        //    For Footer Service Section
        $('#addPhone').click(function () {
            var recentCounter = $('.eachPhoneNumber').length;

            var data='<div class="form-group eachPhoneNumber ic_clear ic_padding_top">\n' +
                '                                            <div class="col-sm-10 ic_padding_left_null" style="padding-left: 0px">\n' +
                '                                                <input name="contents[phone_number]['+recentCounter+']"  class="form-control input-sm" placeholder="Phone Number" type="text">\n' +
                '                                            </div>\n' +
                '                                            <div class="col-sm-2 pull-left">\n' +
                '                                                <button type="button" class="btn btn-xs btn-danger" onclick="if (confirm(\'Are You Sure ?\')){ $(this).parent().parent().remove();}"><i class="fa fa-trash-o"></i></button>\n' +
                '                                            </div>\n' +
                '                                        </div>';
            $('.PhoneNumberGroup').append(data);
        });

        //    For Social Links
        $('#addMobile').click(function () {
            var recentCounter = $('.eachMobileNumber').length;

            var data='<div class="form-group eachMobileNumber ic_clear ic_padding_top">\n' +
                '                                            <div class="col-sm-10 ic_padding_left_null" style="padding-left: 0px">\n' +
                '                                                <input name="contents[mobile_number]['+recentCounter+']"  class="form-control input-sm" placeholder="Phone Number" type="text">\n' +
                '                                            </div>\n' +
                '                                            <div class="col-sm-2">\n' +
                '                                                <button type="button" class="btn btn-xs btn-danger" onclick="if (confirm(\'Are You Sure ?\')){ $(this).parent().parent().remove();}"><i class="fa fa-trash-o"></i></button>\n' +
                '                                            </div>\n' +
                '                                        </div>';
            $('.MobileNumberGroup').append(data);
        });
    </script>
@endsection