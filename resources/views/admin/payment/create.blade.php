@extends('admin.partials.main')

@section('style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Upload Student Payment
            </h1>
            @if(session()->has('error'))
                <br>
                <span class="text-danger">{{ session('error') }}</span>
            @endif
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Student Payment</a></li>
                <li class="active">Upload Payment</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>'StudentPaymentController@store', 'files'=>true]) !!}
                    <div class="col-xs-9">
                        @include('flash::message')
                        <div class="box">
                            <!-- form start -->
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="payment_year">Select Year</label>
                                    <select class="form-control" name="payment_year" id="payment_year" required>
                                        @for($i=18; $i<30; $i++)
                                            <option value="{{ '20'.$i }}"  @if(date('y')==$i){{ 'selected' }} @endif >{{ '20'.$i }}</option>
                                        @endfor
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="payment_month">Select Month</label>
                                    <select class="form-control" name="payment_month" id="payment_month" required>
                                        <option value="1" @if(date('n')==1){{ 'selected' }} @endif >January</option>
                                        <option value="2" @if(date('n')==2){{ 'selected' }} @endif >February</option>
                                        <option value="3" @if(date('n')==3){{ 'selected' }} @endif >March</option>
                                        <option value="4" @if(date('n')==4){{ 'selected' }} @endif >April</option>
                                        <option value="5" @if(date('n')==5){{ 'selected' }} @endif >May</option>
                                        <option value="6" @if(date('n')==6){{ 'selected' }} @endif >June</option>
                                        <option value="7" @if(date('n')==7){{ 'selected' }} @endif >July</option>
                                        <option value="8" @if(date('n')==8){{ 'selected' }} @endif >August</option>
                                        <option value="9" @if(date('n')==9){{ 'selected' }} @endif >September</option>
                                        <option value="10" @if(date('n')==10){{ 'selected' }} @endif >October</option>
                                        <option value="11" @if(date('n')==11){{ 'selected' }} @endif >November</option>
                                        <option value="12" @if(date('n')==12){{ 'selected' }} @endif >December</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="payment">Upload Excel File</label>
                                    <input type="file" id="payment" name="student_payment" required>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script>
        $('div.alert').delay(3000).fadeOut(350);
    </script>
@endsection