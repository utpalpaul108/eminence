@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add New Role
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Role</a></li>
                <li class="active">Add New Role</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>'RoleController@store']) !!}
                    <div class="col-xs-9">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="box">
                            <!-- form start -->
                            <div class="box-body">
                                <div class="form-group add_exam">
                                    <label for="name">Role</label>
                                    <input type="text" name="name" class="form-control" placeholder="Roll Name" id="name">
                                </div>
                            </div>
                            @php
                                $old_permission='';
                                $sl=0;
                            @endphp
                            @foreach($permissions as $permission)
                                @if($old_permission != $permission->module_name)
                                    @if($sl != 0)
                                            </div>
                                        </div>
                                        <hr>
                                    @endif
                                    <div class="box-body">
                                        <label for="name">{{ $permission->module_name }}</label>
                                            <div class="form-group">

                                        @php($old_permission = $permission->module_name)
                                @endif

                                <div class="col-md-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="permissions[]" value="{{ $permission->name }}" > {{ $permission->name }}
                                        </label>
                                    </div>
                                </div>

                                @php($sl++)

                                @if($sl == $permissions->count())
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                            <!-- /.box-body -->
                            @can('add_role')
                                <div class="box-footer">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            @endcan
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection