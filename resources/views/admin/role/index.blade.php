@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Role
                <small>All Roles</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a href="#">Dashboard</a>
                </li>
                <li class="active">All Roles</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    @include('flash::message')
                    <div class="box">
                        @can('add_role')
                            <div class="box-header">
                                <div class="pull-left">
                                    <a id="add-button" title="Add New Role" class="btn btn-success" href="{{ action('RoleController@create') }}"><i class="fa fa-plus-circle"></i> Add New Role</a>
                                </div>
                            </div>
                        @endcan
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($roles->currentPage()-1)*$roles->perPage())+1)
                                @foreach($roles as $role)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $role->name }}</td>
                                        <td>
                                            @can('edit_role')
                                                <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('RoleController@edit',['id'=>$role->id]) }}">
                                                    Edit
                                                </a>&nbsp;
                                            @endcan
                                            @can('delete_role')
                                                {!! Form::open(['action'=>['RoleController@destroy',$role->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                                                    <button title="Delete" class="btn btn-xs btn-danger delete-row" onclick="return confirm('Are You Sure ?')">
                                                        Delete
                                                    </button>
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $roles->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection