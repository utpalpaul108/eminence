@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Permission
                <small>All Permissions</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a href="#">Dashboard</a>
                </li>
                <li class="active">All Permissions</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <div class="pull-left">
                                <a id="add-button" title="Add New Permission" class="btn btn-success" href="{{ action('PermissionController@create') }}"><i class="fa fa-plus-circle"></i> Add New Permission</a>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($permissions->currentPage()-1)*$permissions->perPage())+1)
                                @foreach($permissions as $permission)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $permission->name }}</td>
                                        <td>
                                            <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('PermissionController@edit',['id'=>$permission->id]) }}">
                                                Edit
                                            </a>&nbsp;
                                            {!! Form::open(['action'=>['PermissionController@destroy',$permission->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                                                <button title="Delete" class="btn btn-xs btn-danger delete-row" onclick="return confirm('Are You Sure ?')">
                                                    Delete
                                                </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $permissions->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection