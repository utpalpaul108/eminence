@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Course Outline
                <small>Course</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a href="#">Dashboard</a>
                </li>
                <li class="active">Course Outline</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <div class="pull-left">
                                <a id="add-button" title="Add New Course" class="btn btn-success" href="{{ action('CourseOutlineController@create') }}"><i class="fa fa-plus-circle"></i> Add Course Outline</a>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Course Title</th>
                                    <th class="text-center">About Course</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($all_courses->currentPage()-1)*$all_courses->perPage())+1)
                                @foreach($all_courses as $course)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $course->course_title }}</td>
                                        <td>{!! str_limit($course->about_course,150,'(...)') !!}</td>
                                        <td>
                                            @can('edit_course_outline')
                                                <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('CourseOutlineController@edit',['id'=>$course->id]) }}">
                                                    Edit
                                                </a>&nbsp;
                                            @endcan
                                            @can('delete_course_outline')
                                                {!! Form::open(['action'=>['CourseOutlineController@destroy',$course->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                                                    <button title="Delete" class="btn btn-xs btn-danger delete-row" onclick="return confirm('Are You Sure')">
                                                        Delete
                                                    </button>
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $all_courses->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection