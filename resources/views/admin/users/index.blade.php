@extends('admin.partials.main')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Users
            <small>All Users</small>
        </h1>
        <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> <a href="#">Dashboard</a></li>
            <li class="active">Users</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @can('add_user')
                            <div class="pull-left ic_margin_left15">
                                <a id="add-button" title="Add New User" class="btn btn-success" href="{{ action('UserController@create') }}"><i class="fa fa-plus-circle"></i><span class="hidden-xs"> Add New User</span></a>
                            </div>
                        @endcan
                        <div class="pull-right ic_margin_right15">
                            <select class="form-control select2 select2-hidden-accessible" id="user_type" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                <option value="all" selected="selected">All</option>
                                    @foreach($all_types as $type)
                                        <option value="{{ $type->type }}">{{ ucwords(str_replace('_',' ',$type->type)) }}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <section class="ic-admin-menu">
                            <div class="col-xs-12" id="all_value">
                                <div id="sortable">
                                    <input type="hidden" name="user_type" class="user_type_order" value="all">
                                    @foreach($users as $user)
                                        <div id="{{ $user->id }}" class="ic-single-menu ui-state-default">
                                            <div class="ic-left">
                                                <i class="fa fa-bars" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<strong>{{ $user->name }}</strong>
                                            </div>
                                            <div class="ic-right ic-btn">
                                                @can('edit_user')
                                                    <a title="Edit" class="edit btn btn-xs btn-default edit-row" href="{{ action('UserController@edit',['id'=>$user->id]) }}">
                                                        <i class="fa fa-edit"></i><span class="hidden-xs"> Edit</span>
                                                    </a>&nbsp;
                                                @endcan
                                                @can('delete_user')
                                                    {!! Form::open(['action'=>['UserController@destroy',$user->id],'method'=>'Delete','style'=>'display:inline']) !!}
                                                        <button type="submit" onclick="return confirm('Are You Sure ?')" class="delete btn btn-xs btn-danger delete-row"><i class="fa fa-times"></i><span class="hidden-xs"> Delete</span></button>
                                                    {!! Form::close() !!}
                                                @endcan
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- /.box-body -->
                    {{--<div class="box-footer clearfix">--}}
                        {{--{{ $users->links() }}--}}
                    {{--</div>--}}
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('#user_type').change(function () {
                var user_type=$('#user_type').val();
                $.ajax({
                    type:'GET',
                    url:'/ajax_call/select_type_wise_user',
                    data:{'user_type':user_type},
                    success:function (response) {
                        var res=JSON.parse(response);
                        $('#sortable').html(res.all_users);
                        // console.log(res.all_users);
                    }
                })
            });
        });
    </script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('ic_admin/js/jquery.toaster.js') }}"></script>
    <script>
        $( function() {
            $("#sortable").sortable({
                stop: function(){
                    $.map($(this).find('.ic-single-menu'), function(el) {
                        var itemID = el.id;
                        var itemIndex = $(el).index();
                        console.log(itemID);
                        var user_type=$('#user_type').val();
                        $.ajax({
                            url:'{{URL::to("admin/user/order")}}',
                            type:'GET',
                            dataType:'json',
                            data: {itemID:itemID, itemIndex: itemIndex, user_type:user_type},
                        })
                    });
                    $.toaster('Order Changed Successfully!', 'Status');
                }
            });
            $( "#sortable" ).disableSelection();
        } );
    </script>
@endsection