@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit User
            </h1>
            @if(session()->has('error'))
                <br>
                <span class="text-danger">{{ session('error') }}</span>
            @endif
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">User</a></li>
                <li class="active">Edit User</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>['UserController@update',$user->id],'method'=>'PUT','files'=>true]) !!}
                <div class="col-md-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <div class="form-group">
                                                <h5>{{ $error }}</h5>
                                            </div>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" value="{{ $user->name }}" placeholder="Enter User Name" id="name" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" name="email" value="{{ $user->email }}" placeholder="Enter Email Address" id="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password">Password( Optional )</label>
                                <input type="password" name="password"  placeholder="Enter Password" id="password" class="form-control" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="qualification">Qualification</label>
                                <input type="text" name="qualification" value="{{ $user->qualification }}" placeholder="Enter User Qualification" id="qualification" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="designation">Designation</label>
                                <input type="text" name="designation" value="{{ $user->designation }}" placeholder="Enter User Designation" id="designation" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>User Type</label>
                                {!! Form::select('type', ['board_member' => 'Board Member', 'faculty_member' => 'Faculty Member', 'administrative_staff'=>'Administrative Staff', 'office_staff'=>'Office Staff'], $user->type,['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                {!! Form::select('is_admin', ['0' => 'User', '1' => 'Admin'], $user->is_admin,['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                <label>Current Status</label>
                                {!! Form::select('status', ['1' => 'Active', '0' => 'Inactive'], $user->status,['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                <label>Select Roles</label>
                                <select name="roles[]" multiple="" class="form-control">
                                    @foreach($roles as $role)
                                        <option value="{{ $role->name }}" @if(in_array($role->name,$user_roles)){{ 'selected' }} @endif>{{ $role->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="biography">Biography</label>
                                <textarea name="biography" id="biography" rows="5" class="editor form-control" >{{ $user->biography }}</textarea>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        @can('edit_user')
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Update</button>
                            </div>
                        @endcan
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Profile Image</h3>
                        </div>
                        <div class="box-body text-center">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                                    <img src="@if($user->profile_image != ''){{ Storage::url($user->profile_image) }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="...">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                                <div>
                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                    <input name="profile_image" type="file" >
                                </span>
                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
@endsection