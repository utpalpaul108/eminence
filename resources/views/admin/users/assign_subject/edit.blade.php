@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Assign Subject
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Teacher</a></li>
                <li class="active">Edit Assign Subject</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                {!! Form::open(['action'=>['UserController@update_assign_subject',$teacher->id], 'method'=>'PUT']) !!}
                <div class="col-xs-9">
                    @include('flash::message')
                    <div class="box">
                        <div class="box-body">
                            <div class="form-group add_exam">
                                <h3 style="display: inline">Teacher Name : </h3><h4 style="display: inline">{{ $teacher->name }}</h4>
                                <h4 style="padding-top: 10px">Select Subjects</h4>
                            </div>
                        </div>

                        <!-- form start -->
                        @php
                            $old_department='';
                            $sl=0;
                        @endphp
                        @foreach($subjects as $subject)
                            @if($old_department != $subject->department_id)
                                @if($sl != 0)
                    </div>
                </div>
                <hr>
                @endif
                <div class="box-body">
                    <label for="name">{{ $subject->department['name'] }}</label>
                    <div class="form-group">

                        @php($old_department = $subject->department_id)
                        @endif

                        <div class="col-md-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="assign_subjects[]" value="{{ $subject->id }}" @if(!is_null($teacher->assign_subjects) && in_array($subject->id,$teacher->assign_subjects)){{ 'checked' }} @endif> {{ $subject->subject_name }}
                                </label>
                            </div>
                        </div>

                        @php($sl++)

                        @if($sl == $subjects->count())
                    </div>
                </div>
                @endif
                @endforeach
            <!-- /.box-body -->
                @can('assign_subject')
                    <div class="box-footer">
                        <button class="btn btn-primary" type="submit">Update</button>
                    </div>
                @endcan
            </div>
    </div>
    {!! Form::close() !!}
    </div>
    <!-- ./row -->
    </section>
    <!-- /.content -->
    </div>
@endsection

@section('js')
    <script>
        $('div.alert').delay(3000).fadeOut(350);
    </script>
@endsection