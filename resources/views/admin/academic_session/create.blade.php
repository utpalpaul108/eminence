@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add New Academic Session
            </h1>
            @if(session()->has('error'))
                <br>
                <span class="text-danger">{{ session('error') }}</span>
            @endif
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Academic Session</a></li>
                <li class="active">Add New Session</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>'AcademicSessionController@store']) !!}
                    <div class="col-xs-9">
                        <div class="box">
                            <!-- form start -->
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="session_name">Session Name</label>
                                    <input type="text" name="session_name" value="{{ old('session_name') }}" placeholder="Enter Session Name" id="session_name" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="session_details">Session Details</label>
                                    <textarea type="text" name="session_details" value="{{ old('session_details') }}" placeholder="Enter Session Details" id="session_details" class="editor form-control" required></textarea>
                                </div>
                            </div>

                            @can('add_session')
                                <div class="box-footer">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            @endcan
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
@endsection