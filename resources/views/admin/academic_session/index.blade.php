@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Academic Session
                <small>All Session</small>
            </h1>
            <ol class="breadcrumb">
                <li><i class="fa fa-dashboard"></i> <a href="#">Dashboard</a></li>
                <li class="active">All Session</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        @can('add_session')
                            <div class="box-header">
                                <div class="pull-left">
                                    <a id="add-button" title="Add New Session" class="btn btn-success" href="{{ action('AcademicSessionController@create') }}"><i class="fa fa-plus-circle"></i> Add New Session</a>
                                </div>
                            </div>
                        @endcan
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Session Name</th>
                                    <th class="text-center">Session Details</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($all_sessions->currentPage()-1)*$all_sessions->perPage())+1)
                                @foreach($all_sessions as $session)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $session->session_name }}</td>
                                        <td>{!! $session->session_details !!}</td>
                                        <td>
                                            @can('edit_session')
                                                <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('AcademicSessionController@edit',['id'=>$session->id]) }}">
                                                    Edit
                                                </a>&nbsp;
                                            @endcan
                                            @can('delete_session')
                                                {!! Form::open(['action'=>['AcademicSessionController@destroy',$session->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                                                    <button class="btn btn-xs btn-danger delete-row" type="submit" onclick="return confirm('Are You Sure ?')">
                                                        Delete
                                                    </button>
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $all_sessions->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection