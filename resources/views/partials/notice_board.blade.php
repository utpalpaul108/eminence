<div class="ic-ln-col notice-col">
    <ul>
        @foreach($all_notice as $notice)
            <li>
                <a href="{{ action('NoticeBoardController@show',['title'=>strtolower(str_replace(' ','_',$notice->title))]) }}">{{ $notice->title }}<br>{{ $notice->created_at->format('d M Y') }}</a>
            </li>
        @endforeach
    </ul>
</div>