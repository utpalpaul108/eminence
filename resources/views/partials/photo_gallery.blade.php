<div class="row ic-portfolio-home">
    @foreach($all_photos as $photo)
        <div class="col-sm-3 ic-xxs-6 col-xxs-12 ic-single-card">
            <figure>
                <img class="img-responsive" src="{{ Storage::url($photo->photo) }}" alt="" style="width: 262px; height: 292px">
                <div class="ic-card-overlay">
                    <a href="{{ Storage::url($photo->photo) }}" data-fancybox="group" data-caption="{{ $photo->name }}">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </a>
                </div>
            </figure>
        </div>
    @endforeach
    <div class="col-xs-12">
        <div class="portfolio-hbtn text-center">
            <a href="{{ route('page.show',['slug'=>'all_gallery']) }}" class="portfolio ic-btn">View all</a>
        </div>
    </div>
</div>