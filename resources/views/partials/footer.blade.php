<!-- footer -->
<footer>
    <div class="ic-top-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-xs-6 col-xxs-12">
                    <div class="ic-single-top-ftr">
                        <h3>{!! $footer['first_section']['title'] !!}</h3>
                        <div class="ic-single-contact-info">
                            <span><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                            <p>{!! $footer['first_section']['address'] !!}</p>
                        </div>
                        <div class="ic-single-contact-info">
                            <span><img src="{{ Storage::url('images/icons/land-phone.png') }}" alt=""></span>
                            <p>
                                @php($sl=1)
                                @foreach($footer['phone_number'] as $phone)
                                    @if($sl != 1){{ ' / ' }} @endif
                                    <a href="tel:{{ $phone }}">{{ $phone }}</a>
                                    @php($sl++)
                                @endforeach
                            </p>
                        </div>
                        <div class="ic-single-contact-info">
                            <span><i class="fa fa-phone" aria-hidden="true"></i></span>
                            <p>
                                @php($sl=1)
                                @foreach($footer['mobile_number'] as $mobile)
                                    @if($sl != 1){{ ' / ' }} @endif
                                    <a href="tel:{{ $mobile }}">{{ $mobile }}</a>
                                    @php($sl++)
                                @endforeach
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4  col-xs-6 col-xxs-12">
                    <div class="ic-single-top-ftr">
                        <h3>{{ $footer['second_section_title'] }}</h3>
                        <ul>
                            @foreach($footer['quick_links'] as $quick_link)
                                <li><a href="{{ $quick_link['url'] }}">{{ $quick_link['name'] }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4  col-xs-12">
                    <div class="ic-single-top-ftr">
                        <h3>Find Us With Google Map</h3>
                        <div class="ic-contact-map">
                            <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyBNrASOuKNnXr4a5rEqDC_8fo_isduYSeU&amp;sensor=false"></script>
                            <div id="map_div" style="height: 225px;color: black"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ic-bottom-footer text-center">
        {!! $footer['bottom_title'] !!} <br>Design &amp; Developed By <a href="http://itclanbd.com/" target="_blank">
                {!! $logo !!}
            </a>
    </div>
</footer>
<!-- footer /end -->