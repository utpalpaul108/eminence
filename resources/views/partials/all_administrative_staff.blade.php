<section class="ic-adminstrative-staff">
    <div class="container">
        <div class="ic-flex">
            <h2 class="hidden">hidden title</h2>
            @foreach($all_administrative_staff as $staff)
                <div class="ic-card">
                <div class="ic-card-fig">
                    <img class="img-responsive" style="width: 263px; height: 292px" src="@if($staff->profile_image != ''){{ Storage::url($staff->profile_image) }} @else{{ Storage::url('images/demo_user.png') }} @endif" alt="">
                    <div class="ic-card-caption">
                        <span>{{ $staff->name }}</span>
                        <span>{{ $staff->qualification}}</span>
                        <span>{{ $staff->designation }}</span>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>