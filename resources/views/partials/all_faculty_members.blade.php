<section class="ic-teachers">
    <div class="container">
        <div class="ic-flex">
            <h2 class="hidden">hidden title</h2>
            @foreach($all_faculty_members as $member)
                <div class="ic-card">
                    <div class="ic-card-fig">
                        <img class="img-responsive" style="width: 263px; height: 292px" src="@if($member->profile_image != ''){{ Storage::url($member->profile_image) }} @else{{ Storage::url('images/demo_user.png') }} @endif" alt="">
                        <div class="ic-card-caption">
                            <span>{{ $member->name }}</span>
                            <span>{{ $member->qualification }}</span>
                            <span>{{ $member->designation }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>