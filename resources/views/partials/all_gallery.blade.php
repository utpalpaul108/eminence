<section class="ic-gallery page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="ic-title text-center">
                    <h2>{{ $content['photo_gallery_title'] }}</h2>
                </div>
                <div class="ic-portfolio-nav">
                    <button type="button" data-filter="all">All</button>
                    @foreach($all_photo_categories as $category)
                        <button type="button" data-filter=".{{ $category->category_class }}">{{ $category->name }}</button>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row ic-portfolio-items">
            @foreach($all_photos as $photo)
                <div class="single-gallery-col col-sm-3 ic-xxs-6 col-xxs-12 ic-single-card mix {{ $photo->photo_category['category_class'] }}" data-order="1" style="display: none">
                    <figure>
                        <img class="img-responsive" src="{{ Storage::url($photo['photo']) }}" alt="" style="width: 262px; height: 292px">
                        <div class="ic-card-overlay">
                            <a href="{{ Storage::url($photo['photo']) }}" data-fancybox="group" data-caption="{{ $photo['name'] }}">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </a>
                        </div>
                    </figure>
                </div>
            @endforeach
            <div class="col-xs-12">
                <div class="portfolio-hbtn text-center">
                    <a href="#" id="loadMore" class="portfolio ic-btn">View all</a>
                </div>
            </div>
        </div>
    </div>
</section>