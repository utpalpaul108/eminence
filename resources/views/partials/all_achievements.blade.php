<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    @php($sl=1)
    @foreach($college_achievements as $all_achievements)
        <div class="panel panel-default ic-c-achivement-tbl">
        <div class="panel-heading" role="tab" id="headingOne">
            <h3 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $sl }}" aria-expanded="true" aria-controls="collapse{{ $sl }}">
                    {{ $all_achievements->group_name }}
                </a>
            </h3>
        </div>
        <div id="collapse{{ $sl }}" class="panel-collapse collapse @if($sl==1){{ 'in' }} @endif" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body table-responsive ic-narrow-screen-tbl">
                <table class="table">
                    <tr>
                        <th>Year</th>
                        <th>Appeared</th>
                        <th>Passed</th>
                        <th>Percentage</th>
                        <th>Total A+</th>
                        <th>Golden A+</th>
                    </tr>
                    @foreach($all_achievements->achievements as $achievement)
                        <tr>
                            <td>{{ $achievement['year'] }}</td>
                            <td>{{ $achievement['appeared'] }}</td>
                            <td>{{ $achievement['passed'] }}</td>
                            <td>{{ $achievement['percentage'] }}</td>
                            <td>{{ $achievement['total_a_plus'] }}</td>
                            <td>{{ $achievement['golden_a_plus'] }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
        @php($sl++)
    @endforeach
</div>