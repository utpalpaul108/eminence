
<header>
    <div class="ic-top-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-xxs-12">
                    @php($sl=1)
                    @foreach($header['phone_number'] as $phone)
                        @if($sl !=1){{ ' /' }} @endif <a href="telto:{{ $phone }}">{{ $phone }} </a>
                        @php($sl++)
                    @endforeach
                </div>
                <div class="col-xs-6 col-xxs-12 text-right">
                    <a href="mailto:{{ $header['email'] }}">{{ $header['email'] }}</a>
                </div>
            </div>
        </div>
    </div>
    <div class="ic-middle-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="ic-logo">
                        <a href="{{ route('page.show',['slug'=>'/']) }}">
                            <img class="img-responsive" src="{{ Storage::url($header['logo']) }}" alt="Eminence Logo" style="max-height: 71px">
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="ic-social-nav">
                        <ul>
                            @foreach($header['social_links'] as $social_link)
                                <li><a href="{{ $social_link['url'] }}"><i class="fa {{ $social_link['icon'] }}" aria-hidden="true"></i></a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partials.menu')
</header>