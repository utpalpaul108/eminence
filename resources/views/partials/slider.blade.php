<section class="ic-slider-wrapper">
    <div class="ic-slider">
        @foreach($sliders as $slider)
            <figure class="ic-slingle-slider">
                <img class="img-responsive" src="{{ Storage::url($slider->bg_image) }}" alt="">
                <figcaption>
                    <div class="ic-hslider-caption">
                        <div class="ic-hslider-caption-col">
                            <h1>{{ $slider->title }}</h1>
                            <h2>{!! $slider->subtitle !!}</h2>
                        </div>
                    </div>
                </figcaption>
            </figure>
        @endforeach
    </div>
</section>