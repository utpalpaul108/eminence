<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamCategory extends Model
{
    protected $table='ic_exam_categories';
    protected $fillable=['exam_name','exam_type','details'];
}
