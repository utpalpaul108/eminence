<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegeAchievements extends Model
{
    protected $table='ic_college_achievements';
    protected $fillable=['group_id','year','appeared','passed','percentage','total_a_plus','golden_a_plus'];
}
