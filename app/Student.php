<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Student extends Authenticatable
{
	protected $guard ='student';
	protected $table='ic_students';
	protected $fillable=['name','student_info','department_id','group_id','section_id','session_id','student_id','fourth_subject','student_image'];
    public function attendance(){
    	return $this->hasMany('App\StudentAttendance');
    }
    public function specific_attendance(){
    	return $this->hasOne('App\StudentAttendance');
    }
    public function single_result(){
    	return $this->hasOne('App\Result');
    }
    public function department(){
    	return $this->belongsTo('App\Department');
    }
    public function group(){
    	return $this->belongsTo('App\StudyGroup');
    }
    public function section(){
    	return $this->belongsTo('App\AcademicSection');
    }
	public function session(){
		return $this->belongsTo('App\AcademicSession');
	}
	public function student_fourth_subject(){
    	return $this->belongsTo('App\MainSubject','fourth_subject');
	}
	protected $casts = [
		'student_info' => 'array',
	];
}
