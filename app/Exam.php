<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $table='ic_exams';
    protected $fillable=['exam_category_id','exam_type_id','department_id','group_id','section_id','subject_id','pass_mark'];

    public function exam_type(){
    	return $this->belongsTo('App\ExamType');
    }

    public function exam_category(){
    	return $this->belongsTo('App\ExamCategory');
    }

	public function department(){
		return $this->belongsTo('App\Department');
	}

	public function group(){
		return $this->belongsTo('App\StudyGroup');
	}

	public function section(){
		return $this->belongsTo('App\AcademicSection');
	}

	public function session(){
		return $this->belongsTo('App\AcademicSession');
	}

	public function subject(){
		return $this->belongsTo('App\Subject');
	}
}
