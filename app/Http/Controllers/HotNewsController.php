<?php

namespace App\Http\Controllers;

use App\HotNews;
use Illuminate\Http\Request;

class HotNewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$all_news=HotNews::orderBy('news_order','ASC')->get();
        return view('admin.hot_news.index',['all_news'=>$all_news]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.hot_news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        HotNews::create($request->all());
        return redirect()->action('HotNewsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news=HotNews::find($id);
        return view('admin.hot_news.edit',['news'=>$news]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $news=HotNews::find($id);
        $news->news=$request->news;
        $news->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        HotNews::destroy($id);
        return redirect()->action('HotNewsController@index');
    }

	public function order_news(Request $request){
		$all_news = HotNews::orderBy('news_order','ASC')->get();
		$itemID = $request->itemID;
		$itemIndex = $request->itemIndex;

		foreach($all_news as $news){
			return HotNews::where('id','=',$itemID)->update(array('news_order'=> $itemIndex));
		}
	}
}
