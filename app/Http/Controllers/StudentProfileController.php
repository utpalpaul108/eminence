<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class StudentProfileController extends Controller
{
    public function edit(){
    	$student=Student::find(Auth::guard('student')->id());
    	return view('admin.student.profile',['student'=>$student]);
    }

    public function update(Request $request){
		$student=Student::find($request->student_id);
		$student_info=$student->student_info;
	    $student->name=$request->name;
		$student_info['name_bn']=$request->student_info['name_bn'];
		$student_info['blood_group']=$request->student_info['blood_group'];
		$student_info['father_name']=$request->student_info['father_name'];
		$student_info['mother_name']=$request->student_info['mother_name'];
		$student_info['present_address']=$request->student_info['present_address'];
		$student_info['permanent_address']=$request->student_info['permanent_address'];

		if ($request->hasFile('profile_image')){
			if (Storage::exists($student->student_image)){
				Storage::delete($student->student_image);
			}
			$student['student_image']=$request->file('profile_image')->store('images');
			$image = Image::make(Storage::get($student['student_image']))->resize(200, null, function ($constraint) {
				$constraint->aspectRatio();
			})->encode();
			Storage::put($student['student_image'], $image);
		}

		$student->student_info=$student_info;
		$student->save();

		return redirect()->back();
    }
}
