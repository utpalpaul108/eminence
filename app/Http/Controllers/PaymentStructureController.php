<?php

namespace App\Http\Controllers;

use App\AcademicSection;
use App\AcademicSession;
use App\Department;
use App\PaymentStructure;
use App\StudyGroup;
use Illuminate\Http\Request;

class PaymentStructureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_payments=PaymentStructure::with('department','group','section','session')->paginate(15);
        return view('admin.payment_structure.index',['all_payments'=>$all_payments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$all_departments=Department::all();
    	$all_sessions=AcademicSession::all();
        return view('admin.payment_structure.create',['all_departments'=>$all_departments, 'all_sessions'=>$all_sessions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        PaymentStructure::create($request->all());
        return redirect()->action('PaymentStructureController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    $payment=PaymentStructure::find($id);
	    $all_departments=Department::all();
	    $all_groups=StudyGroup::where('department_id',$payment->department_id)->get();
	    $all_sections=AcademicSection::where([['department_id',$payment->department_id],['group_id',$payment->group_id]])->get();
	    $all_sessions=AcademicSession::all();
        return view('admin.payment_structure.edit',['payment'=>$payment, 'all_departments'=>$all_departments, 'all_groups'=>$all_groups, 'all_sections'=>$all_sections, 'all_sessions'=>$all_sessions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $payment=PaymentStructure::find($id);
        $payment->department_id=$request->department_id;
        $payment->group_id=$request->group_id;
        $payment->section_id=$request->section_id;
        $payment->session_id=$request->session_id;
        $payment->payment_type=$request->payment_type;
        $payment->amount=$request->amount;
        $payment->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PaymentStructure::destroy($id);
        return redirect()->action('PaymentStructureController@index');
    }
}
