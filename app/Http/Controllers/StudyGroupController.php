<?php

namespace App\Http\Controllers;

use App\CollegeAchievements;
use App\Department;
use App\StudyGroup;
use Illuminate\Http\Request;

class StudyGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $all_study_groups=StudyGroup::paginate(15);
	    return view('admin.study_group.index',['all_study_groups'=>$all_study_groups]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$all_departments=Department::all();
        return view('admin.study_group.create',['all_departments'=>$all_departments]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        StudyGroup::create($request->all());
        return redirect()->action('StudyGroupController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$all_departments=Department::all();
    	$study_group=StudyGroup::find($id);
	    return view('admin.study_group.edit',['all_departments'=>$all_departments,'study_group'=>$study_group]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $study_group=StudyGroup::find($id);
        $study_group->group_name=$request->group_name;
        $study_group->about_group=$request->about_group;
        $study_group->department_id=$request->department_id;
        $study_group->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        StudyGroup::destroy($id);
        return redirect()->action('StudyGroupController@index');
    }
}
