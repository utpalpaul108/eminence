<?php

namespace App\Http\Controllers;

use App\ResultGrade;
use Illuminate\Http\Request;

class ResultGradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result_grades=ResultGrade::paginate(15);
        return view('admin.result.grade.index',['result_grades'=>$result_grades]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.result.grade.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ResultGrade::create($request->all());
        return redirect()->action('ResultGradeController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result_grade=ResultGrade::find($id);
        return view('admin.result.grade.edit',['result_grade'=>$result_grade]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $result_grade=ResultGrade::find($id);
	    $result_grade->mark_range_from=$request->mark_range_from;
	    $result_grade->mark_range_to=$request->mark_range_to;
	    $result_grade->gpa=$request->gpa;
	    $result_grade->grade=$request->grade;
	    $result_grade->save();

	    return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ResultGrade::destroy($id);
	    return redirect()->action('ResultGradeController@index');
    }
}
