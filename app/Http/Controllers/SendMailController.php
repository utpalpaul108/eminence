<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SendMailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function send(Request $request)
    {
	    Mail::to('utpalseu@gmail.com')->send(new ContactMail());
	    return redirect()->route('page.show', ['slug'=>'contact_us']);
    }
}
