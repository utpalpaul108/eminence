<?php

namespace App\Http\Controllers;

use App\AcademicSession;
use App\Department;
use App\StudyGroup;
use App\AcademicSection;
use App\Student;
use App\StudentAttendance;
use App\Subject;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;

class StudentAttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$departments=Department::all();
    	$sessions=AcademicSession::all();
	    return view('admin.attendance.index',['departments'=>$departments,'sessions'=>$sessions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $student_id=$request->student_id;
        $subject_id=$request->subject_id;
    	$status=$request->attendance_status;
    	$section=$request->section;

    	$date=$request->date;
    	for ($i=0;$i<count($student_id);$i++){
		    StudentAttendance::updateOrCreate(
			    ['student_id' => $student_id[$i],'subject_id' => $subject_id, 'section_id' => $section,'date' => $date],
			    ['status' => $status[$i]]
		    );
	    }
    	return redirect()->back()->with(['class'=>$request->class,'section'=>$request->section,'date'=>$request->date]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function manage(Request $request){
        $user=Auth::user();
    	$departments=Department::all();
    	$groups=StudyGroup::where('department_id',$request->department)->get();
    	$sessions=AcademicSession::all();
    	$sections=AcademicSection::where([['department_id',$request->department],['group_id',$request->group]])->get();
		$subjects=Subject::where([['department_id',$request->department],['group_id',$request->group],['section_id',$request->section]])->whereIn('id',$user->assign_subjects)->get();

		$students=Student::with(['specific_attendance'=> function($q) use ($request){
			$q->where([['date',$request->date],['subject_id',$request->subject_id]]);
		}])->where([['department_id',$request->department],['group_id',$request->group],['session_id',$request->student_session],['section_id',$request->section]])->orderBy('student_id')->get();

		return view('admin.attendance.manage',['departments'=>$departments,'subjects'=>$subjects,'groups'=>$groups,'sessions'=>$sessions,'sections'=>$sections,'students'=>$students,'input_data'=>$request->all()]);
    }

    public function report(){
    	$departments=Department::all();
    	$groups=StudyGroup::all();
    	$sessions=AcademicSession::all();
    	$sections=AcademicSection::all();
    	return view('admin.attendance.report.index',['departments'=>$departments,'groups'=>$groups, 'sessions'=>$sessions, 'sections'=>$sections]);
    }

    public function show_report(Request $request){
        $user=Auth::user();
    	$date=$request->year .'-' .$request->month;
    	$dateFrom=$date.'-'.'01';
    	$dateTo=$date.'-'.'30';
    	$departments=Department::all();
    	$groups=StudyGroup::where('department_id',$request->department)->get();
    	$sessions=AcademicSession::all();
    	$sections=AcademicSection::where([['department_id',$request->department],['group_id',$request->group]])->get();
        $subjects=Subject::where([['department_id',$request->department],['group_id',$request->group],['section_id',$request->section]])->whereIn('id',$user->assign_subjects)->get();

        $students_attendance=Student::with(['attendance'=>function($q)use ($dateFrom, $dateTo, $request){
	    	$q->whereBetween('date', [$dateFrom, $dateTo])->where('subject_id', $request->subject_id);
	    }])->where([['department_id',$request->department],['group_id',$request->group],['session_id',$request->student_session],['section_id',$request->section]])->orderBy('student_id')->get();

	    return view('admin.attendance.report.show',['input_data'=>$request->all(),'subjects'=>$subjects,'departments'=>$departments,'groups'=>$groups,'sessions'=>$sessions,'sections'=>$sections,'attendances'=>$students_attendance,'date_from'=>$date, 'previous_data'=>$request->all()]);
    }

    public function download_pdf_report(Request $request){

	    $date=$request->date_from;
	    $dateFrom=$date.'-'.'01';
	    $dateTo=$date.'-'.'30';

	    $student_attendance=Student::with(['attendance'=>function($q)use ($dateFrom, $dateTo, $request){
		    $q->whereBetween('date', [$dateFrom, $dateTo])->where('subject_id',$request->subject_id);
	    }])->where([['department_id',$request->department],['group_id',$request->group],['session_id',$request->student_session],['section_id',$request->section]])->orderBy('student_id')->get();

	    $current_date=date_create($date);
	    $month= date_format($current_date,"F");
	    $subject=Subject::find($request->subject_id);
	    $group=StudyGroup::find($request->group);
	    $session=AcademicSession::find($request->student_session);
	    $pdf = PDF::loadView('admin.attendance.report.download_pdf', ['subject'=>$subject, 'group'=>$group,'session'=>$session,'month'=>$month, 'attendances'=>$student_attendance,'date_from'=>$date]);
	    $pdf->setPaper('A4', 'landscape');
	    return $pdf->download('attendance_report.pdf');
    }

}
