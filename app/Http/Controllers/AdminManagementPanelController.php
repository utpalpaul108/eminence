<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminManagementPanelController extends Controller
{
	public function index(){
		if (Auth::check()){
			return redirect()->action('StudentAttendanceController@index');
		}
		else{
			return view('admin.login.management_login');
		}
	}
    public function login(){
    	return view('admin.login.management_login');
    }

    public function authenticate(Request $request){
	    if (Auth::attempt(['email'=>$request->email,'password'=>$request->password, 'status'=>1, 'is_admin'=>1])){
		    return redirect()->action('StudentAttendanceController@index');
	    }
	    else{
		    return redirect()->back()->with('error', '* You Are Not Permitted');
	    }
    }

    public function logout(){
	    Auth::logout();
	    return redirect()->action('AdminManagementPanelController@index');
    }
}
