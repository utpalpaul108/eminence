<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use Illuminate\Support\Facades\Storage;

class AdminPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $pages=Page::paginate(15);
	    return view('admin.pages.index',['pages'=>$pages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
	    return view('admin.pages.'.$page->parent_template,['page'=>$page]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $page=Page::find($id);
    	$allData=$request->contents;
    	if ($request->hasFile('page_image')){
		    if (isset($page->contents['page_image'])){
		    	if (Storage::exists($page->contents['page_image'])){
				    Storage::delete($page->contents['page_image']);
			    }
		    }
		    $allData['page_image']=$request->file('page_image')->store('images');
	    }
	    else{
		    if (isset($page->contents['page_image'])){
			    $allData['page_image'] = $page->contents['page_image'];
		    }
	    }

	    if ($request->hasFile('header_image')){
		    if (isset($page->contents['header_image'])){
			    if (Storage::exists($page->contents['header_image']))
				    Storage::delete($page->contents['header_image']);
		    }
		    $allData['header_image']=$request->file('header_image')->store('images');
	    }
	    else{
		    if (isset($page->contents['header_image'])){
			    $allData['header_image'] = $page->contents['header_image'];
		    }
	    }

	    if ($request->hasFile('course_outline')){
    		if (isset($page->contents['download_course_outline'])){
    			if (Storage::exists($page->contents['download_course_outline'])){
    				Storage::delete($page->contents['download_course_outline']);
			    }
		    }
		    $allData['download_course_outline'] = $request->file('course_outline')->store('files');
	    }

	    else{
    		if (isset($page->contents['download_course_outline'])){
			    $allData['download_course_outline']=$page->contents['download_course_outline'];
		    }
	    }

	    $page->contents=$allData;
    	$page->save();
    	return redirect()->back();

    }

    public function remove_file(Request $request){
	    $page=Page::find($request->id);
	    $allData=$page->contents;
	    if (isset($page->contents['download_course_outline'])){
		    if (Storage::exists($page->contents['download_course_outline'])){
			    Storage::delete($page->contents['download_course_outline']);
		    }
	    }
	    $allData['download_course_outline']='';
	    $page->contents=$allData;
	    $page->save();
	    flash('Course Outline Deleated Successfully')->important();
	    return redirect()->back();
    }

    public function download_file(Request $request){
	    $page=Page::find($request->id);
	    if (isset($page->contents['download_course_outline'])){
		    return response()->download(storage_path("app/public/{$page->contents['download_course_outline']}"));
	    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
