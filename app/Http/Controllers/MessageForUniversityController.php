<?php

namespace App\Http\Controllers;

use App\MessageForUniversity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class MessageForUniversityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages=MessageForUniversity::paginate(15);
        return view('admin.messages.index',['messages'=>$messages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.messages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$allData=$request->all();
    	if ($request->hasFile('profile_image')){
    		$allData['profile_image']=$request->file('profile_image')->store('images');

		    $image = Image::make(Storage::get($allData['profile_image']))->fit(270, 360, function ($constraint) {
			    $constraint->aspectRatio();
		    })->encode();
		    Storage::put($allData['profile_image'], $image);
	    }
        MessageForUniversity::create($allData);

    	return redirect()->action('MessageForUniversityController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($title)
    {
        $message=MessageForUniversity::where('message_title',str_replace('-',' ',$title))->first();
        return view('pages.single_message_for_university',['message'=>$message]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $message=MessageForUniversity::find($id);
        return view('admin.messages.edit',['message'=>$message]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $message=MessageForUniversity::find($id);
        if ($request->hasFile('profile_image')){
        	if (Storage::exists($message->profile_image)){
        		Storage::delete($message->profile_image);
	        }
	        $message['profile_image']=$request->file('profile_image')->store('images');

	        $image = Image::make(Storage::get($message['profile_image']))->fit(270, 360, function ($constraint) {
		        $constraint->aspectRatio();
	        })->encode();
	        Storage::put($message['profile_image'], $image);
        }
        $message->message_title=$request->message_title;
        $message->provide_by=$request->provide_by;
        $message->qualification=$request->qualification;
        $message->designation=$request->designation;
        $message->message=$request->message;
        $message->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $message=MessageForUniversity::find($id);
        if (Storage::exists($message->profile_image)){
        	Storage::delete($message->profile_image);
        }
        MessageForUniversity::destroy($id);
        return redirect()->action('MessageForUniversityController@index');
    }
}
