<?php

namespace App\Http\Controllers;

use App\CollegeAchievements;
use App\StudyGroup;
use Illuminate\Http\Request;

class CollegeAchievementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $all_study_groups=StudyGroup::paginate(15);
	    return view('admin.achievement.index',['all_study_groups'=>$all_study_groups]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$all_groups=StudyGroup::all();
        return view('admin.achievement.create',['all_groups'=>$all_groups]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        CollegeAchievements::create($request->all());
        return redirect()->action('CollegeAchievementController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
	    $achievements=CollegeAchievements::where('group_id',$id)->paginate(15);
	    return view('admin.achievement.show',['achievements'=>$achievements]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$all_groups=StudyGroup::all();
        $achievement=CollegeAchievements::find($id);
        return view('admin.achievement.edit',['achievement'=>$achievement,'all_groups'=>$all_groups]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $achievement=CollegeAchievements::find($id);
        $achievement->group_id=$request->group_id;
        $achievement->year=$request->year;
        $achievement->appeared=$request->appeared;
        $achievement->passed=$request->passed;
        $achievement->percentage=$request->percentage;
        $achievement->total_a_plus=$request->total_a_plus;
        $achievement->golden_a_plus=$request->golden_a_plus;
        $achievement->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CollegeAchievements::destroy($id);
        return redirect()->action('StudyGroupController@index');
    }
}
