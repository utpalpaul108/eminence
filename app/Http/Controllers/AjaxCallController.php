<?php

namespace App\Http\Controllers;

use App\Exam;
use App\PaymentStructure;
use App\PhotoGallery;
use App\Student;
use App\StudentPayment;
use App\Subject;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\AcademicSection;
use App\StudyGroup;

class AjaxCallController extends Controller
{

    public function send_sms(Request $request){

	    //create a json array of your sms
	    $cur_response['status']='failed';
	    $row_array['trxID'] = 	$this->udate('YmdHisu');
	    $row_array['trxTime'] = date('Y-m-d H:i:s');
	    if ($request->sms_number != null){
		    $mySMSArray [0]['smsID'] = $this->udate('YmdHisu');
		    $mySMSArray [0]['smsSendTime'] = date('Y-m-d H:i:s');
		    $mySMSArray [0]['mask'] = 'Eminence College';
		    $mySMSArray [0]['mobileNo'] = '88'.$request->sms_number;
		    $mySMSArray [0]['smsBody'] = $request->teacher_message;
	    }

	    $row_array['smsDatumArray'] = $mySMSArray;

	    $myJSonDatum = json_encode($row_array);

	    //specifi the url
	    $url="http://api.infobuzzer.net/v3.1/SendSMS/sendSmsInfoStore";

	    if($ch = curl_init($url))
	    {
		    //Your valid username & Password ----------Please update those field
		    $username = 'tamal@itclanbd.com';
		    $password = 'itclan';

		    curl_setopt( $ch , CURLOPT_HTTPAUTH , CURLAUTH_BASIC ) ;
		    curl_setopt( $ch, CURLOPT_USERPWD , $username . ':' . $password ) ;
		    curl_setopt( $ch , CURLOPT_CUSTOMREQUEST , 'POST' ) ;

		    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
			    'Content-Length: ' . strlen($myJSonDatum)));

		    curl_setopt($ch, CURLOPT_POST, true);
		    curl_setopt($ch, CURLOPT_POSTFIELDS,$myJSonDatum);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		    curl_setopt( $ch, CURLOPT_TIMEOUT , 300 ) ;
		    $response=curl_exec($ch);
		    curl_close($ch);
		    $cur_response['status']='success';
	    }
	    else
	    {
		    $cur_response['status']='failed';
	    }

	    echo json_encode($cur_response);
    }

	public function udate($format, $utimestamp = null)
	{
		$m = explode(' ',microtime());
		list($totalSeconds, $extraMilliseconds) = array($m[1], (int)round($m[0]*1000,3));
		return date("YmdHis", $totalSeconds) . sprintf('%03d',$extraMilliseconds) ;
	}

    public function select_user(Request $request){
    	$user_type=$request->user_type;
    	if ($user_type == 'all'){
    		$div_header='<input type="hidden" name="user_type" class="user_type_order" value="all">';
		    $all_users=User::orderBy('user_order','ASC')->get();
	    }
	    else{
		    $div_header='<input type="hidden" name="user_type" class="user_type_order" value="'.$user_type.'">';
		    $all_users=User::where('type',$user_type)->orderBy('user_type_order','ASC')->get();
	    }

    	$data='';
    	foreach ($all_users as $user){
    		$data=$data. '<div id="'.$user->id.'" class="ic-single-menu ui-state-default">
                                            <div class="ic-left">
                                                <i class="fa fa-bars" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<strong>'.$user->name.'</strong>
                                            </div>
                                            <div class="ic-right ic-btn">
                                                <a title="Edit" class="edit btn btn-xs btn-default edit-row" href="'.action('UserController@edit',['id'=>$user->id]).'">
                                                    <i class="fa fa-edit"></i> Edit
                                                </a>&nbsp;                            
                                                <form method="POST" action="'.action('UserController@destroy',['id'=>$user->id]).'" accept-charset="UTF-8" style="display:inline"><input name="_method" type="hidden" value="DELETE">'. csrf_field() .'
                                                    <button type="submit" onclick="return confirm(\'Are You Sure ?\')" class="delete btn btn-xs btn-danger delete-row"><i class="fa fa-times"></i> Delete</button>
                                                </form>
                                            </div>
                                        </div>';
	    }

	    $response['all_users']=$div_header.$data;
	    echo json_encode($response);
    }


    public function select_image(Request $request){
    	if ($request->category_id == 'all'){
		    $all_images=PhotoGallery::all();
	    }
	    else{
		    $all_images=PhotoGallery::where('category_id',$request->category_id)->get();
	    }

    	$data='';
    	$sl=0;
    	foreach ($all_images as $image){
    		$data=$data.'<div class="box-body eachClient curser_move" id="'.$image->id.'" data-token="'.csrf_token().'">
                                            <div class="fileinput fileinput-new ic_our_clients" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="max-width: 150px; max-height: 150px;">
                                                    <img src="'.Storage::url($image->photo).'" width="100%" alt="...">
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                                                <div class="text-center">
                                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                                    <input name="gallery_photo'.$sl.'" type="file">
                                                    <input type="hidden" name="previous_image[]" value="'.$image->photo.'">
                                                    <input type="hidden" name="image_id[]" value="'.$image->id.'">
                                                </span>
                                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                                <div class="form-group ic_remove_fig">
                                                    <i class="fa fa-minus-circle text-danger" aria-hidden="true" onclick="if (confirm(\'Are You Sure ?\')){ deleteImage('.$image->id.')}"></i>
                                                </div>
                                            </div>
                                        </div>';
    		$sl++;
	    }

	    $response['all_images']=$data;
    	echo json_encode($response);
    }

	public function department_wise_data(Request $request){
		$all_sections=AcademicSection::where('department_id',$request->department_id)->get();
		$all_groups=StudyGroup::where('department_id',$request->department_id)->get();

		$sections='';
		if ($all_sections != ''){
			$sections='<option value='.''.'>'.'-- Select Section --'.'</option>';
			foreach ($all_sections as $section){
				$sections=$sections.'<option value="'.$section['section_name'].'">'.$section['section_name'].'</option>';
			}
		}
		$groups='';
		if ($all_groups->count()>0){
			$groups='<option value='.''.'>'.'-- Select Group --'.'</option>';
			foreach ($all_groups as $group){
				$groups=$groups.'<option value="'.$group['id'].'">'.$group['group_name'].'</option>';
			}
		}

		$response['sections']=$sections;
		$response['groups']=$groups;

		echo json_encode($response);

	}

	public function section_wise_data(Request $request){
		$all_subjects=Subject::where([['department_id',$request->department_id],['group_id',$request->group_id],['section_id',$request->section_id]])->get();

		$subjects='';
		if ($all_subjects->count()>0){
			$subjects='<option value='.''.'>'.'-- Select Subject --'.'</option>';
			foreach ($all_subjects as $subject){
				$subjects=$subjects.'<option value="'.$subject['id'].'">'.$subject['subject_name'].'</option>';
			}
		}

		$response['subjects']=$subjects;

		echo json_encode($response);

	}

	public function get_student_sections(Request $request){
		$student_info=Student::where('student_id',$request->student_id)->first();
		if ($student_info === null){
			$response['sections']='';
		}
		else{
			$all_sections=AcademicSection::where([['department_id',$student_info->department_id],['group_id',$student_info->group_id]])->get();
			$sections='';
			if ($all_sections !== null){
				foreach ($all_sections as $section){
					$sections=$sections.'<option value="'.$section['id'].'">'.$section['section_name'].'</option>';
				}
			}

			$response['sections']=$sections;
		}
		echo json_encode($response);
	}

	public function get_tution_fee(Request $request){
    	$student=Student::where('student_id',$request->student_id)->first();
    	if ($student ==! null){
		    $payment_structure=PaymentStructure::where([['department_id',$student->department_id],['group_id',$student->group_id],['session_id',$student->session_id],['section_id',$request->section_id],['payment_type','tution_fee']])->first();
		    if ($payment_structure ==! null){
			    $response['tution_fee']=$payment_structure->amount;
		    }
		    else{
			    $response['tution_fee']=null;
		    }
	    }
	    else{
		    $response['tution_fee']=null;
	    }
		echo json_encode($response);
	}

	public function get_due_payment(Request $request){
    	$student=Student::where('student_id',$request->student_id)->first();
    	if ($student !== null){
		    $payment=StudentPayment::where([['student_id',$student->id],['section_id',$request->section_id],['payment_type','tuition']])->first();
		    if ($payment ==! null){
			    $response['due_payment']=$payment->due_payment;
		    }
		    else{
			    $response['due_payment']=null;
		    }
	    }
	    else{
		    $response['due_payment']=null;
	    }

		echo json_encode($response);
	}

	public function get_exams(Request $request){
		$all_exams=Exam::with('exam_category')->where([['department_id',$request->department_id],['group_id',$request->group_id],['section_id',$request->section_id],['subject_id',$request->subject_id],['exam_type_id',$request->exam_type_id]])->get();
		$exams='';
		if ($all_exams->count()>0){
			foreach ($all_exams as $exam){
				$exams=$exams.'<option value="'.$exam['id'].'">'.$exam->exam_category['exam_name'].'</option>';
			}
		}

		$response['exams']=$exams;

		echo json_encode($response);
	}

    public function exam_wise_subject(Request $request){
        $all_exams=Exam::with('subject')->where([['department_id',$request->department_id],['group_id',$request->group_id],['section_id',$request->section_id],['exam_type_id', $request->exam_type_id]])->get();

        $subjects='';
        if ($all_exams->count()>0){
            $subjects='<option value='.''.'>'.'-- Select Subject --'.'</option>';
            foreach ($all_exams as $exam){
                $subjects=$subjects.'<option value="'.$exam->subject['id'].'">'.$exam->subject['subject_name'].'</option>';
            }
        }

        $response['subjects']=$subjects;

        echo json_encode($response);

    }
}
