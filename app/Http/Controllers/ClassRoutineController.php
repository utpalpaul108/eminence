<?php

namespace App\Http\Controllers;

use App\ClassRoutine;
use App\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ClassRoutineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $all_departments=Department::paginate(15);
        return view('admin.class_routine.index',['all_departments'=>$all_departments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$all_departments=Department::all();
        return view('admin.class_routine.create',['all_departments'=>$all_departments]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$allData=$request->all();
    	if ($request->hasFile('download_url')){
		    ini_set('max_execution_time',180);
    		$allData['download_url']=$request->file('download_url')->store('files');
	    }
        ClassRoutine::create($allData);
        return redirect()->action('ClassRoutineController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
	    $class_routines=ClassRoutine::where('department_id',$id)->paginate(15);
	    return view('admin.class_routine.show',['class_routines'=>$class_routines]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$all_departments=Department::all();
        $routine=ClassRoutine::find($id);
        return view('admin.class_routine.edit',['all_departments'=>$all_departments,'routine'=>$routine]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $routine=ClassRoutine::find($id);
        if ($request->hasFile('download_url')){
	        ini_set('max_execution_time',180);
        	if (Storage::exists($routine->download_url)){
        		Storage::delete($routine->download_url);
	        }
	        $routine->download_url=$request->file('download_url')->store('files');
        }


        $routine->department_id=$request->department_id;
        $routine->routine_title=$request->routine_title;
        $routine->about_routine=$request->about_routine;
        $routine->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	$routine=ClassRoutine::find($id);
        if (Storage::exists($routine->download_url)){
        	Storage::delete($routine->download_url);
        }

        ClassRoutine::destroy($id);

        return redirect()->action('DepartmentController@index');
    }
}
