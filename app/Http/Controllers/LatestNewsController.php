<?php

namespace App\Http\Controllers;

use App\LatestNews;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class LatestNewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_news=LatestNews::with('user')->orderBy('news_order','ASC')->get();
//        dd($all_news);
        return view('admin.latest_news.index',['all_news'=>$all_news]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    return view('admin.latest_news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$allData=$request->all();
        if ($request->hasFile('news_image')){
        	$allData['news_image']=$request->file('news_image')->store('images');
        }

        $allData['user_id']=Auth::id();
        LatestNews::create($allData);
        return redirect()->action('LatestNewsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($title)
    {
    	$title=str_replace('_',' ',$title);
        $latest_news=LatestNews::where('title',$title)->first();
        return view('pages.show_latest_news',['latest_news'=>$latest_news]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news=LatestNews::find($id);
        return view('admin.latest_news.edit',['news'=>$news]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$news=LatestNews::find($id);
        if ($request->hasFile('news_image')){
        	if (Storage::exists($news->news_image)){
        		Storage::delete($news->news_image);
	        }
	        $news->news_image=$request->file('news_image')->store('images');
        }
        $news->title=$request->title;
        $news->details=$request->details;
        $news->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news=LatestNews::find($id);
        if (Storage::exists($news->news_image)){
        	Storage::delete($news->news_image);
        }
        LatestNews::destroy($id);

        return redirect()->action('LatestNewsController@index');
    }

	public function order_news(Request $request){
		$all_news = LatestNews::orderBy('news_order','ASC')->get();
		$itemID = $request->itemID;
		$itemIndex = $request->itemIndex;

		foreach($all_news as $news){
			return LatestNews::where('id','=',$itemID)->update(array('news_order'=> $itemIndex));
		}
	}
}
