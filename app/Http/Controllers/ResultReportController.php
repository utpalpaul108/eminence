<?php

namespace App\Http\Controllers;

use App\AcademicSession;
use App\Department;
use App\Exam;
use App\ExamCategory;
use App\ExamType;
use App\Result;
use App\ResultGrade;
use App\Student;
use App\Subject;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;

class ResultReportController extends Controller
{
    public function index()
    {
    	$all_sessions=AcademicSession::all();
    	$exam_types=ExamType::all();
        $all_departments=Department::all();
        return view('admin.result.report.index',['all_departments'=>$all_departments,'all_sessions'=>$all_sessions,'exam_types'=>$exam_types]);
    }

	public function show_report(Request $request){
		if ($request->report_type == 'single_student'){
			$all_students=Student::where([['department_id',$request->department_id],['group_id',$request->group_id],['session_id',$request->session_id],['section_id',$request->section_id]])->orderBy('student_id')->get();
			return view('admin.result.report.show',['all_students'=>$all_students, 'report_info'=>$request->all()]);
		}
		elseif ($request->report_type == 'all_student'){
			$all_subjects=Subject::where([['department_id',$request->department_id],['group_id',$request->group_id],['section_id',$request->section_id]])->orderBy('subject_order')->get()->toArray();
			$tutorial_exam_category=ExamCategory::where('exam_type','tutorial')->get()->toArray();

			if (count($tutorial_exam_category) <= 0){
				session()->flash('error','* No Tutorial Exam Category Found');
				return redirect()->back();
			}

			$tutorial_exam_categories=array_column($tutorial_exam_category,'id');

			$all_students=Student::where([['department_id',$request->department_id],['group_id',$request->group_id],['section_id',$request->section_id],['session_id',$request->session_id]])->orderBy('student_id')->get()->toArray();
			$sl=0;
			foreach ($all_subjects as $subject){
				$tutorial_exam=Exam::where([['exam_type_id',$request->exam_type],['department_id',$request->department_id],['group_id',$request->group_id],['section_id',$request->section_id],['subject_id',$subject['id']]])->whereIn('exam_category_id', $tutorial_exam_categories)->get()->toArray();
				$tutorial_exams=array_column($tutorial_exam,'id');
				$all_exam=Exam::with('exam_category')->where([['subject_id',$subject['id']],['exam_type_id',$request->exam_type]])->whereNotIn('id', $tutorial_exams)->get()->toArray();
				$all_subjects[$sl]['exams']=$all_exam;
				$all_subjects[$sl]['tutorial_exams']=$tutorial_exam;
				$all_exams=array_column($all_exam,'id');

				$all_student_marks=DB::table('ic_results')
				                     ->select(DB::raw('SUM(marks) marks, student_id'))
				                     ->where([['department_id',$request->department_id],['group_id',$request->group_id],['session_id',$request->session_id],['section_id',$request->section_id],['subject_id',$subject['id']]])
				                     ->whereIn('exam_id', $all_exams)
				                     ->groupBy('student_id')
				                     ->get()->toArray();

				$all_student_tutorial_marks=DB::table('ic_results')
				                              ->select(DB::raw('SUM(marks) marks, student_id'))
				                              ->where([['department_id',$request->department_id],['group_id',$request->group_id],['session_id',$request->session_id],['section_id',$request->section_id],['subject_id',$subject['id']]])
				                              ->whereIn('exam_id', $tutorial_exams)
				                              ->groupBy('student_id')
				                              ->get()->toArray();

				$all_student_marks=array_column($all_student_marks,'marks','student_id');
				$all_student_tutorial_marks=array_column($all_student_tutorial_marks,'marks','student_id');

				foreach ($all_student_marks as $key => $value) {
					if (!array_key_exists($key, $all_student_tutorial_marks)) {
						$all_student_tutorial_marks[$key] = ($value*.75);
					}
					else{
						$all_student_tutorial_marks[$key] =($all_student_tutorial_marks[$key]/(count($tutorial_exams))) + ($value*.75);
					}
				}

				if ($all_student_tutorial_marks != null){
					$height_mark=ceil(max($all_student_tutorial_marks));
				}
				else{
					$height_mark=0;
				}

				$all_subjects[$sl]['height_mark']=$height_mark;
				$sl++;
			}

			$st=0;

			foreach ($all_students as $student){
				$is_failed=0;
				$fourth_subject=Subject::where([['department_id',$request->department_id],['group_id',$request->group_id],['section_id',$request->section_id],['core_subject_id',$student['fourth_subject']]])->first();
				$total_marks=0;
				$total_gpa=0;
				foreach ($all_subjects as $subject){
					$subject_wise_failed=0;
					$exam_for_subject=array_column($subject['exams'], 'id');
					$tutorial_exams=array_column($subject['tutorial_exams'],'id');
					$results=Result::where([['student_id',$student['id']],['subject_id',$subject['id']],['exam_type_id',$request->exam_type]])->whereIn('exam_id', $exam_for_subject)->groupBy('exam_id')->get()->toArray();
					$all_results=array_column($results,'marks','exam_id');
					$exam_pass_marks=array_column($subject['exams'], 'pass_mark','id');
					foreach ($exam_pass_marks as $exam_id=>$pass_marks){
						if ($pass_marks >0 && !array_key_exists($exam_id, $all_results)){
							$all_students[$st]['result_color'][$subject['id']][$exam_id]='red';
							if ($subject['id'] != $fourth_subject->id){
								$is_failed++;
							}
							$subject_wise_failed++;
						}
						elseif ($pass_marks >0 && $all_results[$exam_id]<$pass_marks){
							$all_students[$st]['result_color'][$subject['id']][$exam_id]='red';
							if ($subject['id'] != $fourth_subject->id){
								$is_failed++;
							}
							$subject_wise_failed++;
						}
					}


					$tutorial_exam_results=DB::table('ic_results')
					                         ->select(DB::raw('SUM(marks) as marks'))
					                         ->where([['student_id',$student['id']],['subject_id',$subject['id']],['exam_type_id',$request->exam_type]])
					                         ->whereIn('exam_id', $tutorial_exams)
					                         ->first();


					$tutorial_exam_marks=$tutorial_exam_results->marks;


					if ($tutorial_exam_marks>0){
						$tutorial_exam_marks=round($tutorial_exam_marks/(count($tutorial_exams)));
					}
					else{
						$tutorial_exam_marks=0;
					}

					$all_students[$st]['marks'][$subject['id']]=$all_results;
					$all_students[$st]['marks'][$subject['id']]['tutorial']=$tutorial_exam_marks;
					$all_students[$st]['marks'][$subject['id']]['tm']=array_sum($all_results);
					$all_students[$st]['marks'][$subject['id']]['ctm']=round(array_sum($all_results) * .75);
					$all_students[$st]['marks'][$subject['id']]['fm']=$tutorial_exam_marks+ round(array_sum($all_results) * .75);
					$total_marks=$total_marks+$all_students[$st]['marks'][$subject['id']]['fm'];


					if ($subject_wise_failed == 0){
						$total_subject_wise_mark=$all_students[$st]['marks'][$subject['id']]['fm'];
						$result_grade=ResultGrade::where([['mark_range_from','<=',$total_subject_wise_mark],['mark_range_to','>=',$total_subject_wise_mark]])->first();
						$all_students[$st]['marks'][$subject['id']]['gpa']=$result_grade['gpa'];
						$total_gpa=$total_gpa+$result_grade['gpa'];
					}
					else{
						$all_students[$st]['marks'][$subject['id']]['gpa']=0;
					}
				}

				$all_students[$st]['total_marks']=$total_marks;

				if ($is_failed == 0){
					if ($all_students[$st]['marks'][$fourth_subject->id]['gpa'] > 2){
						$total_gpa=$total_gpa-2;
					}
					else{
						$total_gpa=($total_gpa-$all_students[$st]['marks'][$fourth_subject->id]['gpa']);
					}

					$lead_gpa=round($total_gpa/(count($all_subjects)-1),2);
					if ($lead_gpa > 5){
						$lead_gpa=5.0;
					}
					$all_students[$st]['fourth_subject_gpa']=$all_students[$st]['marks'][$fourth_subject->id]['gpa'];
					$all_students[$st]['gpa']=$lead_gpa;
				}
				else{
					$all_students[$st]['fourth_subject_gpa']=$all_students[$st]['marks'][$fourth_subject->id]['gpa'];
					$all_students[$st]['gpa']=0;
				}

				$all_students[$st]['fourth_subject']=$fourth_subject->subject_name;
				$st++;
			}

			return view('admin.result.report.report_excel',['all_students'=>$all_students, 'all_subjects'=>$all_subjects, 'input_data'=>$request->all()]);
		}
	}

    public function print_report(Request $request){
	    $student_info=Student::with('department','group','section','session')->find($request->student_id);
	    $student_attendence=DB::table('ic_student_attendances')
	                          ->select(DB::raw('count(*) total, status'))
	                          ->where([['student_id',$request->student_id],['section_id',$request->section_id]])
	                          ->groupBy('status')
	                          ->get()->toArray();

	    $student_attendence=array_column($student_attendence,'total','status');

	    $exam_type=ExamType::find($request->exam_type);
	    $all_exam_category=[];
	    $tutorial_exam_category=ExamCategory::where('exam_type','tutorial')->get()->toArray();
	    if (count($tutorial_exam_category) <= 0){
	    	session()->flash('error','* No Tutorial Exam Category Found');
	    	return redirect()->back();
	    }
	    $tutorial_exam_categories=array_column($tutorial_exam_category,'id');

	    $fourth_subject=Subject::where([['department_id',$student_info->department_id],['group_id',$student_info->group_id],['section_id',$request->section_id],['core_subject_id',$student_info->fourth_subject]])->first();
	    $all_subjects=Subject::where([['department_id',$student_info->department_id],['group_id',$student_info->group_id],['section_id',$request->section_id],['id','<>',$fourth_subject->id]])->orderBy('subject_order')->get();

	    $subjects=[];
	    $total_marks=0;
	    $total_gpa=0;
	    $overall_failed_condition=0;
	    $converted_total_marks=0;
	    $full_marks=0;
	    $sl=0;

	    foreach ($all_subjects as $subject){
		    $subject_wise_failed_condition=0;
		    $tutorial_exam=Exam::where([['exam_type_id',$request->exam_type],['department_id',$student_info->department_id],['group_id',$student_info->group_id],['section_id',$request->section_id],['subject_id',$subject->id]])->whereIn('exam_category_id', $tutorial_exam_categories)->get()->toArray();
		    $tutorial_exams=array_column($tutorial_exam,'id');

		    $subjects[$sl]['subject_name']=$subject['subject_name'];
		    $all_student_marks=DB::table('ic_results')
		                         ->select(DB::raw('SUM(marks) marks, student_id'))
		                         ->where([['department_id',$student_info->department_id],['group_id',$student_info->group_id],['session_id',$student_info->session_id],['section_id',$request->section_id],['subject_id',$subject->id]])
		                         ->whereNotIn('exam_id', $tutorial_exams)
		                         ->groupBy('student_id')
		                         ->get()->toArray();

		    $all_student_tutorial_marks=DB::table('ic_results')
		                                  ->select(DB::raw('SUM(marks) marks, student_id'))
		                                  ->where([['department_id',$student_info->department_id],['group_id',$student_info->group_id],['session_id',$student_info->session_id],['section_id',$request->section_id],['subject_id',$subject->id]])
		                                  ->whereIn('exam_id', $tutorial_exams)
		                                  ->groupBy('student_id')
		                                  ->get()->toArray();

		    $all_student_marks=array_column($all_student_marks,'marks','student_id');
		    $all_student_tutorial_marks=array_column($all_student_tutorial_marks,'marks','student_id');

		    foreach ($all_student_marks as $key => $value) {
			    if (!array_key_exists($key, $all_student_tutorial_marks)) {
				    $all_student_tutorial_marks[$key] = ($value*.75);
			    }
			    else{
				    $all_student_tutorial_marks[$key] =($all_student_tutorial_marks[$key]/(count($tutorial_exams))) + ($value*.75);
			    }
		    }
		    if ($all_student_tutorial_marks != null){
			    $height_mark=ceil(max($all_student_tutorial_marks));
		    }
		    else{
			    $height_mark=0;
		    }

		    $subjects[$sl]['height_mark']=$height_mark;

		    $all_exams=Exam::with('exam_category')->where([['exam_type_id',$request->exam_type],['department_id',$student_info->department_id],['group_id',$student_info->group_id],['section_id',$request->section_id],['subject_id',$subject->id]])->whereNotIn('id', $tutorial_exams)->get();
		    $exam_category=[];
			if ($all_exams->count() > 0){
				foreach ($all_exams as $exam){
					$result=Result::where([['student_id',$request->student_id],['section_id',$request->section_id],['session_id',$student_info->session_id],['subject_id',$subject->id],['exam_id',$exam->id]])->first();
					$subjects[$sl]['marks'][$exam->exam_category_id]=$result['marks'];
					$exam_category[$exam->exam_category['id']]=$exam->exam_category['exam_name'];
					if ($exam->pass_mark > 0 && $result['marks'] < $exam->pass_mark){
						$subject_wise_failed_condition++;
						$subjects[$sl]['result_color'][$exam->exam_category_id]='red';
					}
				}
			}
			else{
				$subjects[$sl]['marks']=null;
			}


		    $all_exam_category=($all_exam_category+$exam_category);
			if ($subjects[$sl]['marks'] != null){
				$subjects[$sl]['total_marks']=array_sum($subjects[$sl]['marks']);
			}
			else{
				$subjects[$sl]['total_marks']=0;
			}

		    $total_marks=$total_marks+$subjects[$sl]['total_marks'];
		    $subjects[$sl]['converted_mark']=round(($subjects[$sl]['total_marks'] * 75)/100);
		    $converted_total_marks=$converted_total_marks+$subjects[$sl]['converted_mark'];

		    $tutorial_exam_mark=DB::table('ic_results')
		                          ->select(DB::raw('COUNT(*) count, SUM(marks) marks'))
		                          ->where([['student_id',$request->student_id],['section_id',$request->section_id],['subject_id',$subject->id]])
		                          ->whereIn('exam_id', $tutorial_exams)
		                          ->first();

		    if ($tutorial_exam_mark->count == 0){
			    $subjects[$sl]['tutorial_mark']=0;
		    }
		    else{
			    $subjects[$sl]['tutorial_mark']=round($tutorial_exam_mark->marks/$tutorial_exam_mark->count);
		    }

		    $subjects[$sl]['full_mark']=$subjects[$sl]['converted_mark']+$subjects[$sl]['tutorial_mark'];
		    $full_marks=$full_marks+$subjects[$sl]['full_mark'];

		    if ($subject_wise_failed_condition == 0){
			    $result_grade=ResultGrade::where([['mark_range_from','<=',$subjects[$sl]['full_mark']],['mark_range_to','>=',$subjects[$sl]['full_mark']]])->first();
			    $subjects[$sl]['grade']=$result_grade['grade'];
			    $subjects[$sl]['gpa']=$result_grade['gpa'];
			    $total_gpa=$total_gpa+$result_grade['gpa'];
		    }
		    else{
			    $subjects[$sl]['grade']='F';
			    $subjects[$sl]['gpa']='0';
			    $overall_failed_condition++;
			    $subjects[$sl]['color']='red';
		    }
		    $sl++;
	    }


//	    For Fourth Subject
	    $tutorial_exam=Exam::where([['exam_type_id',$request->exam_type],['department_id',$student_info->department_id],['group_id',$student_info->group_id],['section_id',$request->section_id],['subject_id',$fourth_subject->id]])->whereIn('exam_category_id', $tutorial_exam_categories)->get()->toArray();
	    $tutorial_exams=array_column($tutorial_exam,'id');
	    $all_student_marks=DB::table('ic_results')
	                         ->select(DB::raw('SUM(marks) marks, student_id'))
	                         ->where([['department_id',$student_info->department_id],['group_id',$student_info->group_id],['session_id',$student_info->session_id],['section_id',$request->section_id],['subject_id',$fourth_subject->id]])
	                         ->whereNotIn('exam_id', $tutorial_exams)
	                         ->groupBy('student_id')
	                         ->get()->toArray();

	    $all_student_tutorial_marks=DB::table('ic_results')
	                                  ->select(DB::raw('SUM(marks) marks, student_id'))
	                                  ->where([['department_id',$student_info->department_id],['group_id',$student_info->group_id],['session_id',$student_info->session_id],['section_id',$request->section_id],['subject_id',$fourth_subject->id]])
	                                  ->whereIn('exam_id', $tutorial_exams)
	                                  ->groupBy('student_id')
	                                  ->get()->toArray();

	    $all_student_marks=array_column($all_student_marks,'marks','student_id');
	    $all_student_tutorial_marks=array_column($all_student_tutorial_marks,'marks','student_id');

	    foreach ($all_student_marks as $key => $value) {
		    if (!array_key_exists($key, $all_student_tutorial_marks)) {
			    $all_student_tutorial_marks[$key] = ($value*.75);
		    }
		    else{
			    $all_student_tutorial_marks[$key] =($all_student_tutorial_marks[$key]/(count($tutorial_exams))) + ($value*.75);
		    }
	    }
	    if ($all_student_tutorial_marks != null){
		    $height_mark=ceil(max($all_student_tutorial_marks));
	    }
	    else{
		    $height_mark=0;
	    }

	    $fourth_subject_mark['height_mark']=$height_mark;
	    $fourth_subject_mark['subject_name']=$fourth_subject->subject_name;

	    $all_exams=Exam::with('exam_category')->where([['exam_type_id',$request->exam_type],['department_id',$student_info->department_id],['group_id',$student_info->group_id],['section_id',$request->section_id],['subject_id',$fourth_subject->id]])->whereNotIn('id', $tutorial_exams)->get();
	    $exam_category=[];
	    if ($all_exams->count() > 0){
		    foreach ($all_exams as $exam){
			    $result=Result::where([['student_id',$request->student_id],['section_id',$request->section_id],['session_id',$student_info->session_id],['subject_id',$fourth_subject->id],['exam_id',$exam->id]])->first();
			    $fourth_subject_mark['marks'][$exam->exam_category_id]=$result['marks'];
			    $exam_category[$exam->exam_category['id']]=$exam->exam_category['exam_name'];
			    if ($exam->pass_mark > 0 && $result['marks'] < $exam->pass_mark){
				    $fourth_subject_mark['result_color'][$exam->exam_category_id]='red';
			    }
		    }
	    }
	    else{
		    $fourth_subject_mark['marks']=null;
	    }

	    $all_exam_category=($all_exam_category+$exam_category);
	    if ($fourth_subject_mark['marks'] != null){
		    $fourth_subject_mark['total_marks']=array_sum($fourth_subject_mark['marks']);
	    }
	    else{
		    $fourth_subject_mark['total_marks']=0;
	    }

	    $total_marks=$total_marks+$fourth_subject_mark['total_marks'];
	    $fourth_subject_mark['converted_mark']=round(($fourth_subject_mark['total_marks'] * 75)/100);
	    $converted_total_marks=$converted_total_marks+$fourth_subject_mark['converted_mark'];

	    $tutorial_exam_mark=DB::table('ic_results')
	                          ->select(DB::raw('COUNT(*) count, SUM(marks) marks'))
	                          ->where([['student_id',$request->student_id],['section_id',$request->section_id],['subject_id',$fourth_subject->id]])
	                          ->whereIn('exam_id', $tutorial_exams)
	                          ->first();

	    if ($tutorial_exam_mark->count == 0){
		    $fourth_subject_mark['tutorial_mark']=0;
	    }
	    else{
		    $fourth_subject_mark['tutorial_mark']=round($tutorial_exam_mark->marks/$tutorial_exam_mark->count);
	    }

	    $fourth_subject_mark['full_mark']=$fourth_subject_mark['converted_mark']+$fourth_subject_mark['tutorial_mark'];
	    $full_marks=$full_marks+$fourth_subject_mark['full_mark'];

	    $result_grade=ResultGrade::where([['mark_range_from','<=',$fourth_subject_mark['full_mark']],['mark_range_to','>=',$fourth_subject_mark['full_mark']]])->first();
	    $fourth_subject_mark['grade']=$result_grade['grade'];
	    $fourth_subject_mark['gpa']=$result_grade['gpa'];

	    if ($overall_failed_condition == 0){
		    if ($fourth_subject_mark['gpa'] > 2){
			    $total_gpa=$total_gpa+($fourth_subject_mark['gpa']-2);
		    }

		    $actual_gpa=round(($total_gpa/(count($all_subjects))),2);
		    if ($actual_gpa>5){
			    $actual_gpa=5.0;
		    }
		    $lead_result_grade=ResultGrade::where('gpa','<=',$actual_gpa)->orderBy('gpa','desc')->first();
		    $actual_grade=$lead_result_grade['grade'];
	    }

	    else{
		    $actual_gpa=0;
		    $actual_grade='F';
	    }

	    $grading_systems=ResultGrade::all();

		return view('admin.result.report.report_print',['grading_systems'=>$grading_systems,'exam_categories'=>$all_exam_category,'student_info'=>$student_info, 'student_attendance'=>$student_attendence,'exam_type'=>$exam_type ,'all_exams'=>$all_exams, 'all_subjects'=>$subjects, 'fourth_subject'=>$fourth_subject_mark,'total_marks'=>$total_marks, 'converted_marks'=>$converted_total_marks, 'full_marks'=>$full_marks,'grade'=>$actual_grade, 'gpa'=>$actual_gpa]);
    }

	public function download_report(Request $request){
		$student_info=Student::with('department','group','section','session')->find($request->student_id);
		$student_attendence=DB::table('ic_student_attendances')
		                      ->select(DB::raw('count(*) total, status'))
		                      ->where([['student_id',$request->student_id],['section_id',$request->section_id]])
		                      ->groupBy('status')
		                      ->get()->toArray();

		$student_attendence=array_column($student_attendence,'total','status');

		$exam_type=ExamType::find($request->exam_type);
		$all_exam_category=[];
		$tutorial_exam_category=ExamCategory::where('exam_type','tutorial')->get()->toArray();
		if (count($tutorial_exam_category) <= 0){
			session()->flash('error','* No Tutorial Exam Category Found');
			return redirect()->back();
		}
		$tutorial_exam_categories=array_column($tutorial_exam_category,'id');

		$fourth_subject=Subject::where([['department_id',$student_info->department_id],['group_id',$student_info->group_id],['section_id',$request->section_id],['core_subject_id',$student_info->fourth_subject]])->first();
		$all_subjects=Subject::where([['department_id',$student_info->department_id],['group_id',$student_info->group_id],['section_id',$request->section_id],['id','<>',$fourth_subject->id]])->orderBy('subject_order')->get();

		$subjects=[];
		$total_marks=0;
		$total_gpa=0;
		$overall_failed_condition=0;
		$converted_total_marks=0;
		$full_marks=0;
		$sl=0;

		foreach ($all_subjects as $subject){
			$subject_wise_failed_condition=0;
			$tutorial_exam=Exam::where([['exam_type_id',$request->exam_type],['department_id',$student_info->department_id],['group_id',$student_info->group_id],['section_id',$request->section_id],['subject_id',$subject->id]])->whereIn('exam_category_id', $tutorial_exam_categories)->get()->toArray();
			$tutorial_exams=array_column($tutorial_exam,'id');

			$subjects[$sl]['subject_name']=$subject['subject_name'];
			$all_student_marks=DB::table('ic_results')
			                     ->select(DB::raw('SUM(marks) marks, student_id'))
			                     ->where([['department_id',$student_info->department_id],['group_id',$student_info->group_id],['session_id',$student_info->session_id],['section_id',$request->section_id],['subject_id',$subject->id]])
			                     ->whereNotIn('exam_id', $tutorial_exams)
			                     ->groupBy('student_id')
			                     ->get()->toArray();

			$all_student_tutorial_marks=DB::table('ic_results')
			                              ->select(DB::raw('SUM(marks) marks, student_id'))
			                              ->where([['department_id',$student_info->department_id],['group_id',$student_info->group_id],['session_id',$student_info->session_id],['section_id',$request->section_id],['subject_id',$subject->id]])
			                              ->whereIn('exam_id', $tutorial_exams)
			                              ->groupBy('student_id')
			                              ->get()->toArray();

			$all_student_marks=array_column($all_student_marks,'marks','student_id');
			$all_student_tutorial_marks=array_column($all_student_tutorial_marks,'marks','student_id');

			foreach ($all_student_marks as $key => $value) {
				if (!array_key_exists($key, $all_student_tutorial_marks)) {
					$all_student_tutorial_marks[$key] = ($value*.75);
				}
				else{
					$all_student_tutorial_marks[$key] =($all_student_tutorial_marks[$key]/(count($tutorial_exams))) + ($value*.75);
				}
			}
			if ($all_student_tutorial_marks != null){
				$height_mark=ceil(max($all_student_tutorial_marks));
			}
			else{
				$height_mark=0;
			}

			$subjects[$sl]['height_mark']=$height_mark;

			$all_exams=Exam::with('exam_category')->where([['exam_type_id',$request->exam_type],['department_id',$student_info->department_id],['group_id',$student_info->group_id],['section_id',$request->section_id],['subject_id',$subject->id]])->whereNotIn('id', $tutorial_exams)->get();
			$exam_category=[];
			if ($all_exams->count() > 0){
				foreach ($all_exams as $exam){
					$result=Result::where([['student_id',$request->student_id],['section_id',$request->section_id],['session_id',$student_info->session_id],['subject_id',$subject->id],['exam_id',$exam->id]])->first();
					$subjects[$sl]['marks'][$exam->exam_category_id]=$result['marks'];
					$exam_category[$exam->exam_category['id']]=$exam->exam_category['exam_name'];
					if ($exam->pass_mark > 0 && $result['marks'] < $exam->pass_mark){
						$subject_wise_failed_condition++;
					}
				}
			}
			else{
				$subjects[$sl]['marks']=null;
			}


			$all_exam_category=($all_exam_category+$exam_category);
			if ($subjects[$sl]['marks'] != null){
				$subjects[$sl]['total_marks']=array_sum($subjects[$sl]['marks']);
			}
			else{
				$subjects[$sl]['total_marks']=0;
			}

			$total_marks=$total_marks+$subjects[$sl]['total_marks'];
			$subjects[$sl]['converted_mark']=round(($subjects[$sl]['total_marks'] * 75)/100);
			$converted_total_marks=$converted_total_marks+$subjects[$sl]['converted_mark'];

			$tutorial_exam_mark=DB::table('ic_results')
			                      ->select(DB::raw('COUNT(*) count, SUM(marks) marks'))
			                      ->where([['student_id',$request->student_id],['section_id',$request->section_id],['subject_id',$subject->id]])
			                      ->whereIn('exam_id', $tutorial_exams)
			                      ->first();

			if ($tutorial_exam_mark->count == 0){
				$subjects[$sl]['tutorial_mark']=0;
			}
			else{
				$subjects[$sl]['tutorial_mark']=round($tutorial_exam_mark->marks/$tutorial_exam_mark->count);
			}

			$subjects[$sl]['full_mark']=$subjects[$sl]['converted_mark']+$subjects[$sl]['tutorial_mark'];
			$full_marks=$full_marks+$subjects[$sl]['full_mark'];

			if ($subject_wise_failed_condition == 0){
				$result_grade=ResultGrade::where([['mark_range_from','<=',$subjects[$sl]['full_mark']],['mark_range_to','>=',$subjects[$sl]['full_mark']]])->first();
				$subjects[$sl]['grade']=$result_grade['grade'];
				$subjects[$sl]['gpa']=$result_grade['gpa'];
				$total_gpa=$total_gpa+$result_grade['gpa'];
			}
			else{
				$subjects[$sl]['grade']='F';
				$subjects[$sl]['gpa']='0';
				$overall_failed_condition++;
			}
			$sl++;
		}


//	    For Fourth Subject
		$tutorial_exam=Exam::where([['exam_type_id',$request->exam_type],['department_id',$student_info->department_id],['group_id',$student_info->group_id],['section_id',$request->section_id],['subject_id',$fourth_subject->id]])->whereIn('exam_category_id', $tutorial_exam_categories)->get()->toArray();
		$tutorial_exams=array_column($tutorial_exam,'id');
		$all_student_marks=DB::table('ic_results')
		                     ->select(DB::raw('SUM(marks) marks, student_id'))
		                     ->where([['department_id',$student_info->department_id],['group_id',$student_info->group_id],['session_id',$student_info->session_id],['section_id',$request->section_id],['subject_id',$fourth_subject->id]])
		                     ->whereNotIn('exam_id', $tutorial_exams)
		                     ->groupBy('student_id')
		                     ->get()->toArray();

		$all_student_tutorial_marks=DB::table('ic_results')
		                              ->select(DB::raw('SUM(marks) marks, student_id'))
		                              ->where([['department_id',$student_info->department_id],['group_id',$student_info->group_id],['session_id',$student_info->session_id],['section_id',$request->section_id],['subject_id',$fourth_subject->id]])
		                              ->whereIn('exam_id', $tutorial_exams)
		                              ->groupBy('student_id')
		                              ->get()->toArray();

		$all_student_marks=array_column($all_student_marks,'marks','student_id');
		$all_student_tutorial_marks=array_column($all_student_tutorial_marks,'marks','student_id');

		foreach ($all_student_marks as $key => $value) {
			if (!array_key_exists($key, $all_student_tutorial_marks)) {
				$all_student_tutorial_marks[$key] = ($value*.75);
			}
			else{
				$all_student_tutorial_marks[$key] =($all_student_tutorial_marks[$key]/(count($tutorial_exams))) + ($value*.75);
			}
		}
		if ($all_student_tutorial_marks != null){
			$height_mark=ceil(max($all_student_tutorial_marks));
		}
		else{
			$height_mark=0;
		}

		$fourth_subject_mark['height_mark']=$height_mark;
		$fourth_subject_mark['subject_name']=$fourth_subject->subject_name;

		$all_exams=Exam::with('exam_category')->where([['exam_type_id',$request->exam_type],['department_id',$student_info->department_id],['group_id',$student_info->group_id],['section_id',$request->section_id],['subject_id',$fourth_subject->id]])->whereNotIn('id', $tutorial_exams)->get();
		$exam_category=[];
		if ($all_exams->count() > 0){
			foreach ($all_exams as $exam){
				$result=Result::where([['student_id',$request->student_id],['section_id',$request->section_id],['session_id',$student_info->session_id],['subject_id',$fourth_subject->id],['exam_id',$exam->id]])->first();
				$fourth_subject_mark['marks'][$exam->exam_category_id]=$result['marks'];
				$exam_category[$exam->exam_category['id']]=$exam->exam_category['exam_name'];
			}
		}
		else{
			$fourth_subject_mark['marks']=null;
		}

		$all_exam_category=($all_exam_category+$exam_category);
		if ($fourth_subject_mark['marks'] != null){
			$fourth_subject_mark['total_marks']=array_sum($fourth_subject_mark['marks']);
		}
		else{
			$fourth_subject_mark['total_marks']=0;
		}

		$total_marks=$total_marks+$fourth_subject_mark['total_marks'];
		$fourth_subject_mark['converted_mark']=round(($fourth_subject_mark['total_marks'] * 75)/100);
		$converted_total_marks=$converted_total_marks+$fourth_subject_mark['converted_mark'];

		$tutorial_exam_mark=DB::table('ic_results')
		                      ->select(DB::raw('COUNT(*) count, SUM(marks) marks'))
		                      ->where([['student_id',$request->student_id],['section_id',$request->section_id],['subject_id',$fourth_subject->id]])
		                      ->whereIn('exam_id', $tutorial_exams)
		                      ->first();

		if ($tutorial_exam_mark->count == 0){
			$fourth_subject_mark['tutorial_mark']=0;
		}
		else{
			$fourth_subject_mark['tutorial_mark']=round($tutorial_exam_mark->marks/$tutorial_exam_mark->count);
		}

		$fourth_subject_mark['full_mark']=$fourth_subject_mark['converted_mark']+$fourth_subject_mark['tutorial_mark'];
		$full_marks=$full_marks+$fourth_subject_mark['full_mark'];

		$result_grade=ResultGrade::where([['mark_range_from','<=',$fourth_subject_mark['full_mark']],['mark_range_to','>=',$fourth_subject_mark['full_mark']]])->first();
		$fourth_subject_mark['grade']=$result_grade['grade'];
		$fourth_subject_mark['gpa']=$result_grade['gpa'];

		if ($overall_failed_condition == 0){
			if ($fourth_subject_mark['gpa'] > 2){
				$total_gpa=$total_gpa+($fourth_subject_mark['gpa']-2);
			}

			$actual_gpa=round(($total_gpa/(count($all_subjects))),2);
			if ($actual_gpa>5){
				$actual_gpa=5.0;
			}
			$lead_result_grade=ResultGrade::where('gpa','<=',$actual_gpa)->orderBy('gpa','desc')->first();
			$actual_grade=$lead_result_grade['grade'];
		}

		else{
			$actual_gpa=0;
			$actual_grade='F';
		}

		$grading_systems=ResultGrade::all();

//		return view('admin.result.report.report_pdf',['grading_systems'=>$grading_systems,'exam_categories'=>$all_exam_category,'student_info'=>$student_info, 'student_attendance'=>$student_attendence,'exam_type'=>$exam_type ,'all_exams'=>$all_exams, 'all_subjects'=>$subjects, 'fourth_subject'=>$fourth_subject,'total_marks'=>$total_marks, 'converted_marks'=>$converted_total_marks, 'full_marks'=>$full_marks,'grade'=>$actual_grade, 'gpa'=>$actual_gpa]);

		$pdf = PDF::loadView('admin.result.report.report_pdf',['grading_systems'=>$grading_systems,'exam_categories'=>$all_exam_category,'student_info'=>$student_info, 'student_attendance'=>$student_attendence,'exam_type'=>$exam_type ,'all_exams'=>$all_exams, 'all_subjects'=>$subjects, 'fourth_subject'=>$fourth_subject,'total_marks'=>$total_marks, 'converted_marks'=>$converted_total_marks, 'full_marks'=>$full_marks,'grade'=>$actual_grade, 'gpa'=>$actual_gpa]);
		$pdf->setPaper('A4');
		return $pdf->download('result_report_'.$student_info->student_id.'.pdf');
	}

	public function download_excel(Request $request){
		$all_subjects=Subject::where([['department_id',$request->department_id],['group_id',$request->group_id],['section_id',$request->section_id]])->orderBy('subject_order')->get()->toArray();
		$tutorial_exam_category=ExamCategory::where('exam_type','tutorial')->get()->toArray();

		if (count($tutorial_exam_category) <= 0){
			session()->flash('error','* No Tutorial Exam Category Found');
			return redirect()->back();
		}

		$tutorial_exam_categories=array_column($tutorial_exam_category,'id');

		$all_students=Student::where([['department_id',$request->department_id],['group_id',$request->group_id],['section_id',$request->section_id],['session_id',$request->session_id]])->orderBy('student_id')->get()->toArray();
		$sl=0;
		foreach ($all_subjects as $subject){
			$tutorial_exam=Exam::where([['exam_type_id',$request->exam_type],['department_id',$request->department_id],['group_id',$request->group_id],['section_id',$request->section_id],['subject_id',$subject['id']]])->whereIn('exam_category_id', $tutorial_exam_categories)->get()->toArray();
			$tutorial_exams=array_column($tutorial_exam,'id');
			$all_exam=Exam::with('exam_category')->where([['subject_id',$subject['id']],['exam_type_id',$request->exam_type]])->whereNotIn('id', $tutorial_exams)->get()->toArray();
			$all_subjects[$sl]['exams']=$all_exam;
			$all_subjects[$sl]['tutorial_exams']=$tutorial_exam;
			$all_exams=array_column($all_exam,'id');

			$all_student_marks=DB::table('ic_results')
			                     ->select(DB::raw('SUM(marks) marks, student_id'))
			                     ->where([['department_id',$request->department_id],['group_id',$request->group_id],['session_id',$request->session_id],['section_id',$request->section_id],['subject_id',$subject['id']]])
			                     ->whereIn('exam_id', $all_exams)
			                     ->groupBy('student_id')
			                     ->get()->toArray();

			$all_student_tutorial_marks=DB::table('ic_results')
			                              ->select(DB::raw('SUM(marks) marks, student_id'))
			                              ->where([['department_id',$request->department_id],['group_id',$request->group_id],['session_id',$request->session_id],['section_id',$request->section_id],['subject_id',$subject['id']]])
			                              ->whereIn('exam_id', $tutorial_exams)
			                              ->groupBy('student_id')
			                              ->get()->toArray();

			$all_student_marks=array_column($all_student_marks,'marks','student_id');
			$all_student_tutorial_marks=array_column($all_student_tutorial_marks,'marks','student_id');

			foreach ($all_student_marks as $key => $value) {
				if (!array_key_exists($key, $all_student_tutorial_marks)) {
					$all_student_tutorial_marks[$key] = ($value*.75);
				}
				else{
					$all_student_tutorial_marks[$key] =($all_student_tutorial_marks[$key]/(count($tutorial_exams))) + ($value*.75);
				}
			}

			if ($all_student_tutorial_marks != null){
				$height_mark=ceil(max($all_student_tutorial_marks));
			}
			else{
				$height_mark=0;
			}

			$all_subjects[$sl]['height_mark']=$height_mark;
			$sl++;
		}

		$st=0;

		foreach ($all_students as $student){
			$is_failed=0;
			$fourth_subject=Subject::where([['department_id',$request->department_id],['group_id',$request->group_id],['section_id',$request->section_id],['core_subject_id',$student['fourth_subject']]])->first();
			$total_marks=0;
			$total_gpa=0;
			foreach ($all_subjects as $subject){
				$subject_wise_failed=0;
				$exam_for_subject=array_column($subject['exams'], 'id');
				$tutorial_exams=array_column($subject['tutorial_exams'],'id');
				$results=Result::where([['student_id',$student['id']],['subject_id',$subject['id']],['exam_type_id',$request->exam_type]])->whereIn('exam_id', $exam_for_subject)->groupBy('exam_id')->get()->toArray();
				$all_results=array_column($results,'marks','exam_id');
				$exam_pass_marks=array_column($subject['exams'], 'pass_mark','id');
				foreach ($exam_pass_marks as $exam_id=>$pass_marks){
					if ($pass_marks >0 && !array_key_exists($exam_id, $all_results)){
						$all_students[$st]['result_color'][$subject['id']][$exam_id]='red';
						if ($subject['id'] != $fourth_subject->id){
							$is_failed++;
						}
						$subject_wise_failed++;
					}
					elseif ($pass_marks >0 && $all_results[$exam_id]<$pass_marks){
						$all_students[$st]['result_color'][$subject['id']][$exam_id]='red';
						if ($subject['id'] != $fourth_subject->id){
							$is_failed++;
						}
						$subject_wise_failed++;
					}
				}


				$tutorial_exam_results=DB::table('ic_results')
				                         ->select(DB::raw('SUM(marks) as marks'))
				                         ->where([['student_id',$student['id']],['subject_id',$subject['id']],['exam_type_id',$request->exam_type]])
				                         ->whereIn('exam_id', $tutorial_exams)
				                         ->first();


				$tutorial_exam_marks=$tutorial_exam_results->marks;


				if ($tutorial_exam_marks>0){
					$tutorial_exam_marks=round($tutorial_exam_marks/(count($tutorial_exams)));
				}
				else{
					$tutorial_exam_marks=0;
				}

				$all_students[$st]['marks'][$subject['id']]=$all_results;
				$all_students[$st]['marks'][$subject['id']]['tutorial']=$tutorial_exam_marks;
				$all_students[$st]['marks'][$subject['id']]['tm']=array_sum($all_results);
				$all_students[$st]['marks'][$subject['id']]['ctm']=round(array_sum($all_results) * .75);
				$all_students[$st]['marks'][$subject['id']]['fm']=$tutorial_exam_marks+ round(array_sum($all_results) * .75);
				$total_marks=$total_marks+$all_students[$st]['marks'][$subject['id']]['fm'];


				if ($subject_wise_failed == 0){
					$total_subject_wise_mark=$all_students[$st]['marks'][$subject['id']]['fm'];
					$result_grade=ResultGrade::where([['mark_range_from','<=',$total_subject_wise_mark],['mark_range_to','>=',$total_subject_wise_mark]])->first();
					$all_students[$st]['marks'][$subject['id']]['gpa']=$result_grade['gpa'];
					$total_gpa=$total_gpa+$result_grade['gpa'];
				}
				else{
					$all_students[$st]['marks'][$subject['id']]['gpa']=0;
				}
			}

			$all_students[$st]['total_marks']=$total_marks;

			if ($is_failed == 0){
				if ($all_students[$st]['marks'][$fourth_subject->id]['gpa'] > 2){
					$total_gpa=$total_gpa-2;
				}
				else{
					$total_gpa=($total_gpa-$all_students[$st]['marks'][$fourth_subject->id]['gpa']);
				}

				$lead_gpa=round($total_gpa/(count($all_subjects)-1),2);
				if ($lead_gpa > 5){
					$lead_gpa=5.0;
				}
				$all_students[$st]['fourth_subject_gpa']=$all_students[$st]['marks'][$fourth_subject->id]['gpa'];
				$all_students[$st]['gpa']=$lead_gpa;
			}
			else{
				$all_students[$st]['fourth_subject_gpa']=$all_students[$st]['marks'][$fourth_subject->id]['gpa'];
				$all_students[$st]['gpa']=0;
			}

			$all_students[$st]['fourth_subject']=$fourth_subject->subject_name;
			$st++;
		}

		return view('admin.result.report.download_excel',['all_students'=>$all_students, 'all_subjects'=>$all_subjects]);
	}

	public function BePositive($value){
    	if ($value<0){
    		$value=0;
	    }
	    return $value;
	}
}
