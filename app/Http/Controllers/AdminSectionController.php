<?php

namespace App\Http\Controllers;

use App\Section;
use App\WebPageSection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AdminSectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sections=WebPageSection::paginate(15);
        return view('admin.sections.index',['sections'=>$sections]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
	    $section=WebPageSection::where('name',$id)->first();
	    return view('admin.sections.'.$section->template_name,['section'=>$section]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $section=WebPageSection::find($id);
        return view('admin.sections.'.$section->template_name,['section'=>$section]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$allData=$request->contents;
	    $section=WebPageSection::find($id);
    	if ($request->hasFile('logo')){
    		if (isset($section->contents['logo']) && $section->contents['logo'] != ''){
    			if (Storage::exists($section->contents['logo'])){
    				Storage::delete($section->contents['logo']);
			    }
		    }
		    $allData['logo']=$request->file('logo')->store('images');
	    }
	    elseif(isset($section->contents['logo'])){
		    $allData['logo']=$section->contents['logo'];
	    }

        $section->contents=$allData;
        $section->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
