<?php

namespace App\Http\Controllers;

use App\HscResult;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HscResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $all_hsc_results=HscResult::paginate(15);
	    return view('admin.result.hsc.index',['all_hsc_results'=>$all_hsc_results]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.result.hsc.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allData=$request->all();
        if ($request->hasFile('download_url')){
        	$allData['download_url']=$request->file('download_url')->store('files');
        }
        HscResult::create($allData);

        return redirect()->action('HscResultController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hsc_result=HscResult::find($id);
	    return view('admin.result.hsc.edit', ['hsc_result'=>$hsc_result]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hsc_result=HscResult::find($id);
        if ($request->hasFile('download_url')){
        	if (Storage::exists($hsc_result->download_url)){
        		Storage::delete($hsc_result->download_url);
	        }
	        $hsc_result->download_url=$request->file('download_url')->store('files');
        }

        $hsc_result->year=$request->year;
        $hsc_result->about=$request->about;
        $hsc_result->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hsc_result=HscResult::find($id);
        if (Storage::exists($hsc_result->download_url)){
        	Storage::delete($hsc_result->download_url);
        }
        HscResult::destroy($id);

        return redirect()->action('HscResultController@index');
    }

    public function download(Request $request){
    	$hsc_year=HscResult::where('year',$request->year)->first();
	    if (Storage::exists($hsc_year->download_url)){
		    return response()->download(storage_path("app/public/{$hsc_year->download_url}"));
	    }
    }
}
