<?php

namespace App\Http\Controllers;

use App\AcademicSession;
use App\Department;
use App\ExamType;
use App\Result;
use App\Student;
use App\Exam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ExamCategory;
use App\Subject;
use App\ResultGrade;

class ResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $all_results=Result::with('department')->paginate(15);
	    return view('admin.result.index',['all_results'=>$all_results]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$all_departments=Department::all();
    	$all_sessions=AcademicSession::all();
    	$exam_types=ExamType::all();
        return view('admin.result.create',['all_departments'=>$all_departments, 'all_sessions'=>$all_sessions, 'exam_types'=>$exam_types]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    if($request->hasFile('results')){
		    $path = $request->file('results')->getRealPath();
		    $data = \Excel::load($path)->get();

		    if($data->count()){
			    $all_exams=Exam::with('exam_category')->where([['department_id',$request->department_id],['group_id',$request->group_id],['section_id',$request->section_id],['subject_id',$request->subject_id],['exam_type_id',$request->exam_type_id]])->get();
			    foreach ($data as $key => $value) {
			    	$student=Student::where([['student_id',(string)$value->roll],['department_id',(int)$request->department_id]])->first();
			    	if ($student !== null){
			    		$st=0;
					    foreach ($all_exams as $exam){
						    $student_info[$st] = ['student_id' => $student->id,'department_id' => (int)$request->department_id, 'group_id' => (int)$request->group_id, 'section_id' => (int)$request->section_id, 'session_id' => (int)$request->session_id, 'subject_id' => (int)$request->subject_id, 'exam_type_id' => (int)$request->exam_type_id, 'exam_id' => $exam->id];
					    	$exam_category_name=strtolower($exam->exam_category['exam_name']);
					    	if (isset($value->$exam_category_name)){
							    $mark_lists[$exam_category_name] = ['marks' => $value->$exam_category_name,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')];
						    }
						    else{
							    $mark_lists=[];
						    }
						    $st++;
					    }

					    if(!empty($student_info) && !empty($mark_lists)){
					    	$mark_sl=0;
						    foreach ($mark_lists as $mark_list){
							    Result::updateOrCreate($student_info[$mark_sl], $mark_list);
							    $mark_sl++;
						    }
					    }
				    }
			    }
		    }
		    \Session::flash('success','Result Uploaded Successfully');
	    }
	    else{
		    \Session::flash('error','There is no file to import');
	    }
	    return redirect()->action('ResultController@create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result=Result::find($id);
        $all_departments=Department::all();
        return view('admin.result.edit',['result'=>$result,'all_departments'=>$all_departments]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $result=Result::find($id);
        $result->student_id=$request->student_id;
        $result->department_id=$request->department_id;
        $result->semester=$request->semester;
        $result->year=$request->year;
        $result->result=$request->result;

	    $result->save();
	    return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    Result::destroy($id);
	    return redirect()->action('ResultController@index');
    }

	public function download_result(Request $request){

		$student_info=Student::where('student_id',$request->student_id)->first();
		$tutorial_exam_category=ExamCategory::where('exam_type','tutorial')->get()->toArray();

		if (count($tutorial_exam_category) <= 0 || is_null($student_info)){
			$Response['status']='failed';
			$Response['result']="* No Result Found";
			echo json_encode($Response);
		}
		else{
			$tutorial_exam_categories=array_column($tutorial_exam_category,'id');
			$fourth_subject=Subject::where([['department_id',$student_info->department_id],['group_id',$student_info->group_id],['section_id',$request->section_id],['core_subject_id',$student_info->fourth_subject]])->first();
			$all_subjects=Subject::where([['department_id',$student_info->department_id],['group_id',$student_info->group_id],['section_id',$request->section_id],['id','<>',$fourth_subject->id]])->get();
			$subjects=[];
			$total_gpa=0;
			$overall_failed_condition=0;
			$sl=0;

			foreach ($all_subjects as $subject){
				$subject_wise_failed_condition=0;
				$tutorial_exam=Exam::where([['exam_type_id',$request->exam_type],['department_id',$student_info->department_id],['group_id',$student_info->group_id],['section_id',$request->section_id],['subject_id',$subject->id]])->whereIn('exam_category_id', $tutorial_exam_categories)->get()->toArray();
				$tutorial_exams=array_column($tutorial_exam,'id');

				$all_exams=Exam::with('exam_category')->where([['exam_type_id',$request->exam_type],['department_id',$student_info->department_id],['group_id',$student_info->group_id],['section_id',$request->section_id],['subject_id',$subject->id]])->whereNotIn('id', $tutorial_exams)->get();
				$exam_category=[];
				if ($all_exams->count() > 0){
					foreach ($all_exams as $exam){
						$result=Result::where([['student_id',$student_info->id],['section_id',$request->section_id],['session_id',$student_info->session_id],['subject_id',$subject->id],['exam_id',$exam->id]])->first();
						$subjects[$sl]['marks'][$exam->exam_category_id]=$result['marks'];
						$exam_category[$exam->exam_category['id']]=$exam->exam_category['exam_name'];
						if ($exam->pass_mark > 0 && $result['marks'] < $exam->pass_mark){
							$subject_wise_failed_condition++;
						}
					}
				}
				else{
					$subjects[$sl]['marks']=null;
				}

				if ($subjects[$sl]['marks'] != null){
					$subjects[$sl]['total_marks']=array_sum($subjects[$sl]['marks']);
				}
				else{
					$subjects[$sl]['total_marks']=0;
				}

				$subjects[$sl]['converted_mark']=round(($subjects[$sl]['total_marks'] * 75)/100);

				$tutorial_exam_mark=DB::table('ic_results')
				                      ->select(DB::raw('COUNT(*) count, SUM(marks) marks'))
				                      ->where([['student_id',$student_info->id],['section_id',$request->section_id],['subject_id',$subject->id]])
				                      ->whereIn('exam_id', $tutorial_exams)
				                      ->first();

				if ($tutorial_exam_mark->count == 0){
					$subjects[$sl]['tutorial_mark']=0;
				}
				else{
					$subjects[$sl]['tutorial_mark']=round($tutorial_exam_mark->marks/$tutorial_exam_mark->count);
				}

				$subjects[$sl]['full_mark']=$subjects[$sl]['converted_mark']+$subjects[$sl]['tutorial_mark'];

				if ($subject_wise_failed_condition == 0){
					$result_grade=ResultGrade::where([['mark_range_from','<=',$subjects[$sl]['full_mark']],['mark_range_to','>=',$subjects[$sl]['full_mark']]])->first();
					$subjects[$sl]['gpa']=$result_grade['gpa'];
					$total_gpa=$total_gpa+$result_grade['gpa'];
				}
				else{
					$subjects[$sl]['gpa']='0';
					$overall_failed_condition++;
				}
				$sl++;
			}

//	    For Fourth Subject
			$tutorial_exam=Exam::where([['exam_type_id',$request->exam_type],['department_id',$student_info->department_id],['group_id',$student_info->group_id],['section_id',$request->section_id],['subject_id',$fourth_subject->id]])->whereIn('exam_category_id', $tutorial_exam_categories)->get()->toArray();
			$tutorial_exams=array_column($tutorial_exam,'id');
			$all_exams=Exam::with('exam_category')->where([['exam_type_id',$request->exam_type],['department_id',$student_info->department_id],['group_id',$student_info->group_id],['section_id',$request->section_id],['subject_id',$fourth_subject->id]])->whereNotIn('id', $tutorial_exams)->get();
			$exam_category=[];
			if ($all_exams->count() > 0){
				foreach ($all_exams as $exam){
					$result=Result::where([['student_id',$student_info->id],['section_id',$request->section_id],['session_id',$student_info->session_id],['subject_id',$fourth_subject->id],['exam_id',$exam->id]])->first();
					$fourth_subject_mark['marks'][$exam->exam_category_id]=$result['marks'];
					$exam_category[$exam->exam_category['id']]=$exam->exam_category['exam_name'];
				}
			}
			else{
				$fourth_subject_mark['marks']=null;
			}

			if ($fourth_subject_mark['marks'] != null){
				$fourth_subject_mark['total_marks']=array_sum($fourth_subject_mark['marks']);
			}
			else{
				$fourth_subject_mark['total_marks']=0;
			}

			$fourth_subject_mark['converted_mark']=round(($fourth_subject_mark['total_marks'] * 75)/100);

			$tutorial_exam_mark=DB::table('ic_results')
			                      ->select(DB::raw('COUNT(*) count, SUM(marks) marks'))
			                      ->where([['student_id',$student_info->id],['section_id',$request->section_id],['subject_id',$fourth_subject->id]])
			                      ->whereIn('exam_id', $tutorial_exams)
			                      ->first();

			if ($tutorial_exam_mark->count == 0){
				$fourth_subject_mark['tutorial_mark']=0;
			}
			else{
				$fourth_subject_mark['tutorial_mark']=round($tutorial_exam_mark->marks/$tutorial_exam_mark->count);
			}

			$fourth_subject_mark['full_mark']=$fourth_subject_mark['converted_mark']+$fourth_subject_mark['tutorial_mark'];

			$result_grade=ResultGrade::where([['mark_range_from','<=',$fourth_subject_mark['full_mark']],['mark_range_to','>=',$fourth_subject_mark['full_mark']]])->first();
			$fourth_subject_mark['gpa']=$result_grade['gpa'];


			if ($overall_failed_condition == 0){
				if ($fourth_subject_mark['gpa'] > 2){
					$total_gpa=$total_gpa+($fourth_subject_mark['gpa']-2);
				}

				$actual_gpa=round(($total_gpa/(count($all_subjects))),2);
				if ($actual_gpa>5){
					$actual_gpa=5.0;
				}
				$lead_result_grade=ResultGrade::where('gpa','<=',$actual_gpa)->orderBy('gpa','desc')->first();
				$actual_grade=$lead_result_grade['grade'];
			}

			else{
				$actual_gpa=0;
				$actual_grade='F';
			}

			$Response['status']='success';
			$Response['result']='CGPA : ' .$actual_gpa;
			echo json_encode($Response);
		}
	}
}
