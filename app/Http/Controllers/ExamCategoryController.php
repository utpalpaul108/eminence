<?php

namespace App\Http\Controllers;

use App\ExamCategory;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class ExamCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $exam_categories=ExamCategory::paginate(15);
	    return view('admin.exam.exam_category.index',['exam_categories'=>$exam_categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    return view('admin.exam.exam_category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$validation=Validator::make($request->all(),[
		    'exam_name' => 'required|unique:ic_exam_categories',
	    ]);
    	if ($validation->fails()){
    		return redirect()->back()->withErrors($validation)->withInput();
	    }
	    ExamCategory::create($request->all());
	    return redirect()->action('ExamCategoryController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    $exam_category=ExamCategory::find($id);
	    return view('admin.exam.exam_category.edit',['exam_category'=>$exam_category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

	    $validation=Validator::make($request->all(), [
		    'exam_name' => [
			    'required',
			    Rule::unique('ic_exam_categories')->ignore($id),
		    ],
	    ]);

	    if ($validation->fails()){
	    	return redirect()->back()->withErrors($validation)->withInput();
	    }
	    $exam_category=ExamCategory::find($id);
	    $exam_category->exam_name=$request->exam_name;
	    $exam_category->exam_type=$request->exam_type;
	    $exam_category->details=$request->details;
	    $exam_category->save();
	    session()->flash('success','Exam Category Updated Successfully');
	    return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    ExamCategory::destroy($id);
	    return redirect()->action('ExamCategoryController@index');
    }
}
