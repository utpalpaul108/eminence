<?php

namespace App\Http\Controllers;

use App\PhotoCategory;
use App\PhotoGallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class PhotoGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $all_categories=PhotoCategory::all();
	    $all_photos=PhotoGallery::orderBy('image_order','ASC')->get();
	    return view('admin.photo_gallery.show',['all_categories'=>$all_categories,'photo_gallery'=>$all_photos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$all_categories=PhotoCategory::all();
        return view('admin.photo_gallery.create',['all_categories'=>$all_categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $all_photo_id=$request->image_id;
	    $sl=0;
	    foreach ($request->previous_image as $previous_image){
		    if ($request->hasFile('gallery_photo'.$sl)){
			    if ($previous_image != null){
				    if (File::exists(Storage::url($previous_image))){
					    File::delete(Storage::url($previous_image));
				    }
				    $old_photo=PhotoGallery::find($all_photo_id[$sl]);
				    $old_photo->photo=$request->file('gallery_photo'.$sl)->store('public');

				    $image = Image::make(Storage::get($old_photo->photo))->resize(900, null, function ($constraint) {
					    $constraint->aspectRatio();
				    })->encode();

				    Storage::put($old_photo->photo, $image);

				    $old_photo->save();
			    }
			    else{
				    $photo_gallery=new PhotoGallery();
			    	$photo_gallery->category_id=$request->category_id;
			    	$photo_gallery->photo=$request->file('gallery_photo'.$sl)->store('public');

				    $image = Image::make(Storage::get($photo_gallery->photo))->resize(900, null, function ($constraint) {
					    $constraint->aspectRatio();
				    })->encode();

				    Storage::put($photo_gallery->photo, $image);

			    	$photo_gallery->save();
			    }
		    }
		    $sl++;
	    }
	    return redirect()->action('PhotoGalleryController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$all_categories=PhotoCategory::all();
        $photo=PhotoGallery::find($id);
        return view('admin.photo_gallery.edit',['photo'=>$photo,'all_categories'=>$all_categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$photo=PhotoGallery::find($id);
        if ($request->hasFile('photo')){
        	if (Storage::exists($photo->photo)){
        		Storage::delete($photo->photo);
	        }
	        $photo->photo=$request->file('photo')->store('images');

	        $image = Image::make(Storage::get($photo->photo))->resize(900, null, function ($constraint) {
		        $constraint->aspectRatio();
	        })->encode();
	        Storage::put($photo->photo, $image);
        }
        $photo->name=$request->name;
        $photo->details=$request->details;
        $photo->category_id=$request->category_id;
        $photo->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $photo=PhotoGallery::find($id);
        if (Storage::exists($photo->photo)){
        	Storage::delete($photo->photo);
        }

        PhotoGallery::destroy($id);
        return redirect()->action('PhotoGalleryController@index');
    }

    public function delete_photo(Request $request){
		$photo=PhotoGallery::find($request->id);
		if (File::exists(Storage::url($photo->photo))){
			File::delete(Storage::url($photo->photo));
		}
		PhotoGallery::destroy($request->id);
    }

	public function order_image(Request $request){
    	$category_type=$request->category_type;
    	if ($category_type == 'all'){
		    $all_images = PhotoGallery::orderBy('image_order','ASC')->get();
		    $itemID = $request->itemID;
		    $itemIndex = $request->itemIndex;

		    foreach($all_images as $image){
			    return PhotoGallery::where('id','=',$itemID)->update(array('image_order'=> $itemIndex));
		    }
	    }
	    else{
		    $all_images = PhotoGallery::where('category_id',$category_type)->orderBy('category_order','ASC')->get();
		    $itemID = $request->itemID;
		    $itemIndex = $request->itemIndex;

		    foreach($all_images as $image){
			    return PhotoGallery::where('id','=',$itemID)->update(array('category_order'=> $itemIndex));
		    }
	    }

	}
}
