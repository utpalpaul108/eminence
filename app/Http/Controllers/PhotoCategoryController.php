<?php

namespace App\Http\Controllers;

use App\PhotoCategory;
use Illuminate\Http\Request;

class PhotoCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_categories=PhotoCategory::paginate(15);
        return view('admin.photo_gallery.category.index',['all_categories'=>$all_categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.photo_gallery.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$all_data = $request->all();
	    $all_data['category_class']=str_replace(' ','_',$request->category_class);
        PhotoCategory::create($all_data);
        return redirect()->action('PhotoCategoryController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $photo_category=PhotoCategory::find($id);
        return view('admin.photo_gallery.category.edit',['photo_category'=>$photo_category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$category=PhotoCategory::find($id);
    	$category->name=$request->name;
    	$category->category_class=str_replace(' ','_',$request->category_class);
    	$category->save();
	    return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PhotoCategory::destroy($id);
        return redirect()->action('PhotoCategoryController@index');
    }
}
