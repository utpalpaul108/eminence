<?php

namespace App\Http\Controllers;

use App\AcademicSection;
use App\Department;
use App\ExamSchedule;
use App\StudyGroup;
use Illuminate\Http\Request;

class ExamScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_exams=ExamSchedule::with('department')->paginate(15);
        return view('admin.exam.exam_schedule.index',['all_exams'=>$all_exams]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$all_departments=Department::all();
        return view('admin.exam.exam_schedule.create',['all_departments'=>$all_departments]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ExamSchedule::create($request->all());
        return redirect()->action('ExamScheduleController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $exam=ExamSchedule::find($id);
        $departments=Department::all();
        $groups=StudyGroup::where('department_id',$exam->department_id)->get();
        $sections=AcademicSection::where([['department_id',$exam->department_id],['group_id',$exam->group_id]])->get();

        return view('admin.exam.exam_schedule.edit',['all_departments'=>$departments,'all_groups'=>$groups,'all_sections'=>$sections, 'exam'=>$exam]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$exam=ExamSchedule::find($id);
    	$exam->exam_name=$request->exam_name;
    	$exam->exam_date=$request->exam_date;
    	$exam->exam_time=$request->exam_time;
    	$exam->duration=$request->duration;
    	$exam->total_marks=$request->total_marks;
    	$exam->department_id=$request->department_id;
    	$exam->group_id=$request->group_id;
    	$exam->section_id=$request->section_id;
    	$exam->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ExamSchedule::destroy($id);
        return redirect()->action('ExamScheduleController@index');
    }
}
