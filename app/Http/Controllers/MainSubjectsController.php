<?php

namespace App\Http\Controllers;

use App\MainSubject;
use Illuminate\Http\Request;

class MainSubjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $main_subjects=MainSubject::paginate(15);
        return view('admin.academic_subject.core_subject.index',['main_subjects'=>$main_subjects]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.academic_subject.core_subject.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        MainSubject::create($request->all());
        return redirect()->action('MainSubjectsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $main_subject=MainSubject::find($id);
        return view('admin.academic_subject.core_subject.edit',['main_subject'=>$main_subject]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $main_subject=MainSubject::find($id);
        $main_subject->name=$request->name;
        $main_subject->details=$request->details;
        $main_subject->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MainSubject::destroy($id);
        return redirect()->action('MainSubjectsController@index');
    }
}
