<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourseOutline;
use Illuminate\Support\Facades\Storage;
use App\Department;

class CourseOutlineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $all_departments=Department::paginate(15);
	    return view('admin.course_outline.index',['all_departments'=>$all_departments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    $all_departments=Department::all();
	    return view('admin.course_outline.create',['all_departments'=>$all_departments]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $allData=$request->all();
	    if ($request->hasFile('download_url')){
		    $allData['download_url']=$request->file('download_url')->store('files');
	    }
	    CourseOutline::create($allData);
	    return redirect()->action('CourseOutlineController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
	    $all_courses=CourseOutline::where('department_id',$id)->paginate(15);
	    return view('admin.course_outline.show',['all_courses'=>$all_courses]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    $all_departments=Department::all();
	    $course=CourseOutline::find($id);
	    return view('admin.course_outline.edit',['all_departments'=>$all_departments,'course'=>$course]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $course=CourseOutline::find($id);
	    if ($request->hasFile('download_url')){
		    if (Storage::exists($course->download_url)){
			    Storage::delete($course->download_url);
		    }
		    $course->download_url=$request->file('download_url')->store('files');
	    }

	    $course->department_id=$request->department_id;
	    $course->course_title=$request->course_title;
	    $course->about_course=$request->about_course;
	    $course->save();
	    return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    $routine=CourseOutline::find($id);
	    if (Storage::exists($routine->download_url)){
		    Storage::delete($routine->download_url);
	    }

	    CourseOutline::destroy($id);
	    return redirect()->action('CourseOutlineController@index');
    }
}
