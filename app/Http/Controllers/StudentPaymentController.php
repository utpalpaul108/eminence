<?php

namespace App\Http\Controllers;

use App\AcademicSection;
use App\Department;
use App\PaymentStructure;
use App\Student;
use App\StudentPayment;
use App\StudyGroup;
use Illuminate\Http\Request;
use App\PaymentInvoice;
use Barryvdh\DomPDF\Facade as PDF;

class StudentPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_payments=StudentPayment::with('student')->paginate(15);
        return view('admin.payment.index',['all_payments'=>$all_payments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$all_departments=Department::all();
        return view('admin.payment.create',['all_departments'=>$all_departments]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('student_payment')){
            $path = $request->file('student_payment')->getRealPath();
            $year=$request->payment_year;
            $month=$request->payment_month;

            \Excel::selectSheetsByIndex(0)->filter('chunk')->load($path)->chunk(10000, function($results)use ($year, $month)
            {

                foreach ($results as $value) {
                    $student=Student::where('student_id',(string)$value->student_id)->first();
                    if ($student !== null){
                        StudentPayment::updateOrCreate(
                            ['student_id' => (string)$value->student_id, 'payment_year' => $year, 'payment_month' => $month],
                            ['payment_amount' => $value->payment, 'due_amount'=>$value->due]
                        );
                    }
                }
            });


            flash('Student Payment Uploaded Successfully')->success();
        }
        else{
            flash('There is no file to import')->error();
        }
        return redirect()->action('StudentPaymentController@create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        StudentPayment::destroy($id);
        return redirect()->action('StudentPaymentController@index');
    }



}
