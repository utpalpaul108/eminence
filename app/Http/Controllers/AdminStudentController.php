<?php

namespace App\Http\Controllers;

use App\AcademicSession;
use App\Department;
use App\MainSubject;
use App\StudentPayment;
use App\StudyGroup;
use App\AcademicSection;
use App\Student;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class AdminStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
	    $validation=Validator::make($request->all(),[
	    	'department_id'=>'required',
	    	'group_id'=>'required',
	    	'section_id'=>'required',
	    	'session_id'=>'required',
	    ]);

	    if ($validation->fails()){
	    	return redirect()->action('AdminStudentController@select_students');
	    }

	    $departments=Department::all();
	    $groups=StudyGroup::where('department_id',$request->department_id)->get();
	    $sections=AcademicSection::where([['department_id',$request->department_id],['group_id',$request->group_id]])->get();
	    $sessions=AcademicSession::all();

	    $students=Student::where([['department_id',$request->department_id],['group_id',$request->group_id],['section_id',$request->section_id],['session_id',$request->session_id]])->orderBy('student_id', 'asc')->get();
		return view('admin.student.index',['input_data'=>$request->all(), 'students'=>$students, 'departments'=>$departments, 'groups'=>$groups, 'sections'=>$sections, 'sessions'=>$sessions]);
    }

    public function select_students(Request $request){
    	$departments=Department::all();
    	$sessions=AcademicSession::all();
    	return view('admin.student.select_student',['departments'=>$departments, 'sessions'=>$sessions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    $departments=Department::all();
	    $sessions=AcademicSession::all();
	    $all_subjects=MainSubject::all();
        return view('admin.student.create',['departments'=>$departments,'sessions'=>$sessions, 'all_subjects'=>$all_subjects]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$validation=Validator::make($request->all(),[
			'student_id'=>'required | unique:ic_students'
		]);

		if ($validation->fails()){
			return redirect()->back()->withErrors($validation)->withInput();
		}

    	$allData=$request->all();
    	$allData['password']=bcrypt($request->password);
    	if ($request->hasFile('student_image')){
		    $allData['student_image'] = $request->file('student_image')->store('images');
	    }

        Student::create($allData);

        return redirect()->action('AdminStudentController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$student=Student::with('department','group','section','session','student_fourth_subject')->find($id);
        $student_payment=StudentPayment::where('student_id',$student->student_id)->orderBy('id','desc')->first();
//        dd($student_payment);
    	return view('admin.student.show',compact('student', 'student_payment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student=Student::find($id);
        $departments=Department::all();
	    $groups=StudyGroup::where('department_id',$student->department_id)->get();
	    $sections=AcademicSection::where([['department_id',$student->department_id],['group_id',$student->group_id]])->get();
        $sessions=AcademicSession::all();
        $all_subjects=MainSubject::all();

        return view('admin.student.edit',['student'=>$student,'departments'=>$departments,'groups'=>$groups,'sections'=>$sections,'sessions'=>$sessions, 'all_subjects'=>$all_subjects]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $validation=Validator::make($request->all(),[
		    'student_id' => [
			    'required',
			    Rule::unique('ic_students')->ignore($id),
		    ]
	    ]);

	    if ($validation->fails()){
		    return redirect()->back()->withErrors($validation)->withInput();
	    }
        $student=Student::find($id);
        $student->name=$request->name;
        $student->student_info=$request->student_info;
        $student->department_id=$request->department_id;
        $student->group_id=$request->group_id;
        $student->section_id=$request->section_id;
        $student->session_id=$request->session_id;
        $student->fourth_subject=$request->fourth_subject;
		$student->student_id=$request->student_id;

		if ($request->hasFile('student_image')){
			if (Storage::exists($student->student_image)){
				Storage::delete($student->student_image);
			}
			$student->student_image=$request->file('student_image')->store('images');
		}

		$student->save();
		\session()->flash('success','Student Updated Successfully');
		return redirect()->back();
    }

	public function promotion(){
		$departments=Department::all();
		$sessions=AcademicSession::all();
		return view('admin.student.promotion',['departments'=>$departments, 'sessions'=>$sessions]);
	}

	public function manage_promotion(Request $request){

		$departments=Department::all();
		$groups=StudyGroup::where('department_id',$request->department)->get();
		$sessions=AcademicSession::all();
		$sections=AcademicSection::where([['department_id',$request->department],['group_id',$request->group]])->get();

		$students=Student::where([['department_id',$request->department],['group_id',$request->group],['session_id',$request->session_id],['section_id',$request->section]])->get();

		$promote_from=AcademicSection::find($request->section);
		$promote_to=AcademicSection::find($request->promote_to);

		return view('admin.student.manage_promotion',['promote_from'=>$promote_from,'promote_to'=>$promote_to,'departments'=>$departments,'groups'=>$groups,'sessions'=>$sessions,'sections'=>$sections,'students'=>$students,'input_data'=>$request->all()]);
	}

	public function update_promotion(Request $request){
    	$student=Student::find((int)$request->student_id);
    	$student->section_id=(int)$request->section_id;
    	$student->save();
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        Storage::delete($student->student_image);
        $student->delete();
        return redirect()->action('AdminStudentController@index');
    }

    public function sendSMS(Request $request){
	    $students=Student::where([['department_id',$request->department_id],['group_id',$request->group_id],['section_id',$request->section_id],['session_id',$request->session_id]])->orderBy('student_id', 'asc')->get();

	    $sms_array = array();

//create a json array of your sms
	    $row_array['trxID'] = 	$this->udate('YmdHisu');
	    $row_array['trxTime'] = date('Y-m-d H:i:s');

	    $sl=0;
	    foreach ($students as $student){
	    	if ($student->student_info['sms_number'] != null){
			    $mySMSArray [$sl]['smsID'] = $this->udate('YmdHisu');
			    $mySMSArray [$sl]['smsSendTime'] = date('Y-m-d H:i:s');
			    $mySMSArray [$sl]['mask'] = 'Eminence College';
			    $mySMSArray [$sl]['mobileNo'] = '88'.$student->student_info['sms_number'];
			    $mySMSArray [$sl]['smsBody'] = $request->sms_text;
			    $sl++;
		    }
	    }

	    $row_array['smsDatumArray'] = $mySMSArray;

	    $myJSonDatum = json_encode($row_array);

//specifi the url
        $url="http://api.infobuzzer.net/v3.1/SendSMS/sendSmsInfoStore";

	    if($ch = curl_init($url))
	    {
		    //Your valid username & Password ----------Please update those field
		    $username = 'tamal@itclanbd.com';
		    $password = 'itclan';

		    curl_setopt( $ch , CURLOPT_HTTPAUTH , CURLAUTH_BASIC ) ;
		    curl_setopt( $ch, CURLOPT_USERPWD , $username . ':' . $password ) ;
		    curl_setopt( $ch , CURLOPT_CUSTOMREQUEST , 'POST' ) ;

		    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
			    'Content-Length: ' . strlen($myJSonDatum)));

		    curl_setopt($ch, CURLOPT_POST, true);
		    curl_setopt($ch, CURLOPT_POSTFIELDS,$myJSonDatum);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		    curl_setopt( $ch, CURLOPT_TIMEOUT , 300 ) ;
		    $response=curl_exec($ch);
		    curl_close($ch);
//		    echo('Response is: '.$response);
		    return redirect()->back()->with('success','SMS Send Successfully');
	    }
	    else
	    {
		    return redirect()->back()->with('error','SMS Send Failed');
//		    echo("Sorry,the connection cannot be established");
	    }

    }

	public function sendSingleSMS(Request $request){
		$student=Student::find($request->student_id);

		//create a json array of your sms
		$row_array['trxID'] = 	$this->udate('YmdHisu');
		$row_array['trxTime'] = date('Y-m-d H:i:s');
		if ($student->student_info['sms_number'] != null){
			$mySMSArray [0]['smsID'] = $this->udate('YmdHisu');
			$mySMSArray [0]['smsSendTime'] = date('Y-m-d H:i:s');
			$mySMSArray [0]['mask'] = 'Eminence College';
			$mySMSArray [0]['mobileNo'] = '88'.$student->student_info['sms_number'];
			$mySMSArray [0]['smsBody'] = $request->message;
		}


		$row_array['smsDatumArray'] = $mySMSArray;

		$myJSonDatum = json_encode($row_array);

	//specifi the url
        $url="http://api.infobuzzer.net/v3.1/SendSMS/sendSmsInfoStore";

		if($ch = curl_init($url))
		{
			//Your valid username & Password ----------Please update those field
			$username = 'tamal@itclanbd.com';
			$password = 'itclan';

			curl_setopt( $ch , CURLOPT_HTTPAUTH , CURLAUTH_BASIC ) ;
			curl_setopt( $ch, CURLOPT_USERPWD , $username . ':' . $password ) ;
			curl_setopt( $ch , CURLOPT_CUSTOMREQUEST , 'POST' ) ;

			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
				'Content-Length: ' . strlen($myJSonDatum)));

			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$myJSonDatum);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			curl_setopt( $ch, CURLOPT_TIMEOUT , 300 ) ;
			$response=curl_exec($ch);
			curl_close($ch);
			return redirect()->back()->with('success','SMS Send Successfully');
		}
		else
		{
			return redirect()->back()->with('error','SMS Send Failed');
		}

	}

	public function udate($format, $utimestamp = null)
	{
		$m = explode(' ',microtime());
		list($totalSeconds, $extraMilliseconds) = array($m[1], (int)round($m[0]*1000,3));
		return date("YmdHis", $totalSeconds) . sprintf('%03d',$extraMilliseconds) ;
	}
}
