<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseOutline extends Model
{
    protected $table='ic_course_outline';
    protected $fillable=['department_id','course_title','download_url','about_course'];
}
