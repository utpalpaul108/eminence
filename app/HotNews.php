<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotNews extends Model
{
    protected $table='ic_hot_news';
    protected $fillable=['news'];
}
