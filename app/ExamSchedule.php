<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamSchedule extends Model
{
    protected $table='ic_exam_schedule';
    protected $fillable=['exam_name','exam_date','exam_time','duration','total_marks','department_id','group_id','section_id'];

    public function department(){
    	return $this->belongsTo('App\Department');
    }
}
