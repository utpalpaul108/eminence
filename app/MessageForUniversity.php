<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageForUniversity extends Model
{
    protected $table='ic_message_for_university';
    protected $fillable=['message_title','provide_by','qualification','designation','message','profile_image'];
}
