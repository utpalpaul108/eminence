<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table='ic_subjects';
    protected $fillable=['department_id','group_id','section_id','core_subject_id','subject_code','subject_name',];
    public function department(){
    	return $this->belongsTo('App\Department');
    }

    public function group(){
        return $this->belongsTo('App\StudyGroup');
    }

    public function section(){
        return $this->belongsTo('App\AcademicSection');
    }
}
