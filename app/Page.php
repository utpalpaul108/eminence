<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table='ic_pages';
	protected $casts=['contents'=>'array'];
}
