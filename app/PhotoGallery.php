<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotoGallery extends Model
{
    protected $table='ic_photo_gallery';
	protected $fillable=['name','details','photo','category_id'];
	public function photo_category(){
		return $this->belongsTo('App\PhotoCategory','category_id');
	}
}
