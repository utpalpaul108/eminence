<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentAttendance extends Model
{
    protected $table='ic_student_attendances';
    protected $fillable=['student_id','subject_id','section_id','status','date'];
}
