<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcademicSection extends Model
{
    protected $table='ic_academic_sections';
    protected $fillable=['section_name','about_section','department_id','group_id'];
}
