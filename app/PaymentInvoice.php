<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentInvoice extends Model
{
    protected $table='ic_payment_invoice';
    protected $fillable=['student_id','payments','status'];
    protected $casts=[
		'payments'=>'array',
    ];
}
