<?php

namespace App\Providers;

use App\ClassRoutine;
use App\CourseOutline;
use App\Department;
use App\ExamType;
use App\HotNews;
use App\HscResult;
use App\LatestNews;
use App\MessageForUniversity;
use App\NoticeBoard;
use App\PhotoCategory;
use App\PhotoGallery;
use App\Result;
use App\StudyGroup;
use App\User;
use App\WebPageSection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Slider;
use DOMDocument;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

    //        For Header
	    view()->composer('partials.main', function ($view){
		    $header=WebPageSection::where('template_name','header')->value('contents');
		    $view->with(['header'=>$header]);
	    });

	//        For Footer
	    view()->composer('partials.main', function ($view){
		    $footer=WebPageSection::where('template_name','footer')->value('contents');
		    $view->with(['footer'=>$footer]);
	    });

	//        For Sliders
	    view()->composer('partials.slider', function ($view){
		    $sliders=Slider::all();
		    $view->with(['sliders'=>$sliders]);
	    });

	//        For Latest News
	    view()->composer('partials.latest_news', function ($view){
		    $all_news=LatestNews::with('user')->orderBy('news_order','ASC')->get();
		    $view->with(['all_news'=>$all_news]);
	    });
    //        For Hot News
	    view()->composer('partials.hot_news', function ($view){
		    $hot_news=HotNews::orderBy('news_order','ASC')->get(['news']);
		    $all_hot_news='';
		    foreach ($hot_news as $news){
		    	$all_hot_news=$all_hot_news.' *' .$news->news;
		    }
		    $view->with(['all_hot_news'=>$all_hot_news]);
	    });
    //        Message For University
	    view()->composer('partials.message_for_university', function ($view){
		    $all_messages=MessageForUniversity::all();
		    $view->with(['all_messages'=>$all_messages]);
	    });

	//        For Board Members
	    view()->composer('partials.board_members', function ($view){
		    $board_members=User::where('type','board_member')->orderBy('user_type_order','ASC')->get();
		    $view->with(['board_members'=>$board_members]);
	    });

    //        For HSC Result
	    view()->composer('pages.results', function ($view){
		    $hsc_results=HscResult::distinct('year')->get();
		    $exam_types=ExamType::get();
		    $results=Result::distinct('year')->get();
		    $view->with(['hsc_results'=>$hsc_results,'exam_types'=>$exam_types,'results'=>$results]);
	    });

    //        For Faculty Members
	    view()->composer('partials.all_faculty_members', function ($view){
		    $all_faculty_members=User::where('type','faculty_member')->orderBy('user_type_order','ASC')->get();
		    $view->with(['all_faculty_members'=>$all_faculty_members]);
	    });
    //        For Administrative Staffs
	    view()->composer('partials.all_administrative_staff', function ($view){
		    $all_administrative_staff=User::where('type','administrative_staff')->orderBy('user_type_order','ASC')->get();
		    $view->with(['all_administrative_staff'=>$all_administrative_staff]);
	    });

    //        For Office Staffs
	    view()->composer('partials.all_office_staff', function ($view){
		    $all_office_staff=User::where('type','office_staff')->orderBy('user_type_order','ASC')->get();
		    $view->with(['all_office_staff'=>$all_office_staff]);
	    });

    //        For All Achievements
	    view()->composer('partials.all_achievements', function ($view){
		    $college_achievements=StudyGroup::with('achievements')->get();
		    $view->with(['college_achievements'=>$college_achievements]);
	    });

    //        For Notice Board
	    view()->composer('partials.notice_board', function ($view){
		    $all_notice=NoticeBoard::with('user')->orderBy('notice_order','ASC')->get();
		    $view->with(['all_notice'=>$all_notice]);
	    });

	//        All Gallery
	    view()->composer('partials.all_gallery', function ($view){
	    	$all_photo_categories=PhotoCategory::all();
		    $all_photos=PhotoGallery::with('photo_category')->orderBy('image_order','ASC')->orderBy('category_order','ASC')->get();
		    $view->with(['all_photo_categories'=>$all_photo_categories,'all_photos'=>$all_photos]);
	    });

    //        Class Routine
	    view()->composer('pages.class_routine', function ($view){
		    $department_id=$view->getData()['content']['department_id'];
		    $all_routine=ClassRoutine::where('department_id',$department_id)->get();
		    $view->with(['all_routine'=>$all_routine]);
	    });

    //        Course Outline
	    view()->composer('pages.course_outline', function ($view){
		    $department_id=$view->getData()['content']['department_id'];
		    $all_courses=CourseOutline::where('department_id',$department_id)->get();
		    $view->with(['all_courses'=>$all_courses]);
	    });

    //        For All Departments
	    view()->composer(['admin.pages.class_routine','admin.pages.course_outline'], function ($view){
		    $all_departments=Department::all();
		    $view->with(['all_departments'=>$all_departments]);
	    });

	//        All Photos
	    view()->composer('partials.photo_gallery', function ($view){
		    $all_photos=PhotoGallery::orderBy('image_order','ASC')->take(8)->get();
		    $view->with(['all_photos'=>$all_photos]);
	    });

    //        For Admin
//	    view()->composer('admin.partials.main', function ($view){
//		    $user=PhotoGallery::all();
//		    $view->with(['all_photos'=>$all_photos]);
//	    });


	//	    For SVG Footer Logo
	    view()->composer('*', function($view) {
		    // Instantiate new DOMDocument object
		    $svg = new DOMDocument();
		    // Load SVG file from public folder
		    $svg->load(public_path('images/itclanLogo.svg'));
		    // Add CSS class (you can omit this line)
		    $svg->documentElement->setAttribute("class", "logo");
		    // Get XML without version element
		    $logo = $svg->saveXML($svg->documentElement);
		    // Attach data to view
		    $view->with(['logo'=>$logo]);
	    });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
