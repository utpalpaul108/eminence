<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentStructure extends Model
{
    protected $table='ic_payment_structure';
    protected $fillable=['department_id','group_id','section_id','session_id','payment_type','amount'];
    public function department(){
    	return $this->belongsTo('App\Department');
    }
	public function group(){
		return $this->belongsTo('App\StudyGroup');
	}
	public function section(){
		return $this->belongsTo('App\AcademicSection');
	}
	public function session(){
		return $this->belongsTo('App\AcademicSession');
	}
}
