<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamType extends Model
{
    protected $table='ic_exam_type';
    protected $fillable=['exam_type','about'];
}
