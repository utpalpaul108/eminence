<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentPayment extends Model
{
    protected $table='ic_student_payments';
    protected $fillable=['student_id','payment_amount','due_amount','payment_year','payment_month'];
    public function student(){
    	return $this->belongsTo('App\Student');
    }
}
