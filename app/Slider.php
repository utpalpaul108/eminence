<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table='ic_sliders';

    protected $fillable=['title','subtitle','bg_image'];
}
