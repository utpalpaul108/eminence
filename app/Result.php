<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $table ='ic_results';
    protected $fillable=['student_id','department_id','group_id','section_id','session_id','subject_id','exam_type_id','exam_id','marks'];
    public function department(){
    	return $this->belongsTo('App\Department','department_id');
    }
}
