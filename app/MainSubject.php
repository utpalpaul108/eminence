<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainSubject extends Model
{
    protected $table='ic_core_subjects';
    protected $fillable=['name','details'];
}
