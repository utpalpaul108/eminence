<?php

use Illuminate\Database\Seeder;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    factory(App\Student::class, 5)->create()->each(function ($u) {
		    $u->attendance()->save(factory(App\StudentAttendance::class)->make());
	    });
    }
}
