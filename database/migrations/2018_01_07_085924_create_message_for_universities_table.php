<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageForUniversitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ic_message_for_university', function (Blueprint $table) {
            $table->increments('id');
            $table->string('message_title');
            $table->string('provide_by');
            $table->string('qualification')->nullable();
            $table->string('designation')->nullable();
            $table->text('message')->nullable();
            $table->string('profile_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ic_message_for_university');
    }
}
