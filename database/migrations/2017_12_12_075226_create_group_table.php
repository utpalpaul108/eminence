<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ic_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('details');
	        $table->integer('department_id')->unsigned();
	        $table->foreign('department_id')->references('id')->on('ic_departments')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ic_groups');
    }
}
