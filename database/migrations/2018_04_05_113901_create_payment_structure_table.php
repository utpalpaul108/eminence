<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentStructureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ic_payment_structure', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('department_id')->unsigned();
            $table->foreign('department_id')->references('id')->on('ic_departments')->onDelete('cascade');
	        $table->integer('group_id')->unsigned();
	        $table->foreign('group_id')->references('id')->on('ic_study_groups')->onDelete('cascade');
	        $table->integer('section_id')->unsigned();
	        $table->foreign('section_id')->references('id')->on('ic_academic_sections')->onDelete('cascade');
	        $table->integer('session_id')->unsigned();
	        $table->foreign('session_id')->references('id')->on('ic_academic_session')->onDelete('cascade');
            $table->string('payment_type');
            $table->string('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ic_payment_structure');
    }
}
