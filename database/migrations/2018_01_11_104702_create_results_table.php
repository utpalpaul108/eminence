<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ic_results', function (Blueprint $table) {
            $table->increments('id');
            $table->string('student_id');
            $table->integer('department_id')->unsigned();
            $table->foreign('department_id')->references('id')->on('ic_departments')->onDelete('cascade');
	        $table->integer('group_id')->unsigned();
	        $table->foreign('group_id')->references('id')->on('ic_study_groups')->onDelete('cascade');
	        $table->integer('section_id')->unsigned();
	        $table->foreign('section_id')->references('id')->on('ic_academic_sections')->onDelete('cascade');
	        $table->integer('session_id')->unsigned();
	        $table->foreign('session_id')->references('id')->on('ic_academic_session')->onDelete('cascade');
	        $table->integer('subject_id')->unsigned();
	        $table->foreign('subject_id')->references('id')->on('ic_subjects')->onDelete('cascade');
	        $table->integer('exam_type_id')->unsigned();
	        $table->foreign('exam_type_id')->references('id')->on('ic_exam_type')->onDelete('cascade');
	        $table->integer('exam_id')->unsigned();
	        $table->foreign('exam_id')->references('id')->on('ic_exams')->onDelete('cascade');
	        $table->string('marks')->after('exam_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ic_results');
    }
}
