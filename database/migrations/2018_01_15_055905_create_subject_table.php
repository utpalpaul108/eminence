<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ic_subjects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('department_id')->unsigned();
            $table->foreign('department_id')->references('id')->on('ic_departments')->onDelete('cascade');
	        $table->integer('group_id')->unsigned();
	        $table->foreign('group_id')->references('id')->on('ic_study_groups')->onDelete('cascade');
	        $table->integer('section_id')->unsigned();
	        $table->foreign('section_id')->references('id')->on('ic_academic_sections')->onDelete('cascade');
	        $table->integer('core_subject_id')->unsigned();
	        $table->foreign('core_subject_id')->references('id')->on('ic__core_subjects')->onDelete('cascade');
	        $table->string('subject_code')->nullable();
	        $table->string('subject_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ic_subjects');
    }
}
