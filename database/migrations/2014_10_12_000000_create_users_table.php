<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ic_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique()->nullable();
            $table->string('qualification')->nullable();
            $table->string('designation')->nullable();
            $table->string('type')->nullable();
            $table->integer('status')->nullable();
	        $table->integer('is_admin')->nullable();
            $table->text('biography')->nullable();
            $table->string('profile_image')->nullable();
            $table->string('password')->nullable();
            $table->text('assign_subjects')->nullable();
	        $table->integer('user_order')->nullable();
	        $table->integer('user_type_order')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ic_users');
    }
}
