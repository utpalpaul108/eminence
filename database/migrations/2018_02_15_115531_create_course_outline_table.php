<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseOutlineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ic_course_outline', function (Blueprint $table) {
	        $table->increments('id');
	        $table->integer('department_id')->unsigned();
	        $table->foreign('department_id')->references('id')->on('ic_departments')->onDelete('cascade');
	        $table->string('course_title');
	        $table->string('download_url')->nullable();
	        $table->string('about_course')->nullable();
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ic_course_outline');
    }
}
