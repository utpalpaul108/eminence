<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentAttendancesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ic_student_attendances', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('student_id')->unsigned();
			$table->foreign('student_id')->references('id')->on('ic_students')->onDelete('cascade');
			$table->integer('section_id')->unsigned();
			$table->foreign('section_id')->references('id')->on('ic_academic_sections')->onDelete('cascade');
			$table->integer('subject_id')->unsigned();
			$table->foreign('subject_id')->references('id')->on('ic_subjects')->ondelete('cascade');
			$table->string('status');
			$table->string('date');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('ic_student_attendances');
	}
}
