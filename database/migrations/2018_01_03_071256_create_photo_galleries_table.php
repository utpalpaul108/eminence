<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotoGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ic_photo_gallery', function (Blueprint $table) {
	        $table->increments('id');
	        $table->string('name')->nullable();
	        $table->text('details')->nullable();
	        $table->string('photo')->nullable();
	        $table->integer('category_id')->unsigned();
	        $table->foreign('category_id')->references('id')->on('ic_photo_categories')->onDelete('cascade');
	        $table->integer('image_order')->nullable();
	        $table->integer('category_order')->nullable();
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('ic_photo_gallery');
    }
}
