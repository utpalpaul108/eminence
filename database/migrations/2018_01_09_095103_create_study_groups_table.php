<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudyGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ic_study_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('group_name');
            $table->text('about_group')->nullable();
	        $table->integer('department_id')->unsigned();
	        $table->foreign('department_id')->references('id')->on('ic_departments')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ic_study_groups');
    }
}
