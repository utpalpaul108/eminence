<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcademicSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ic_academic_sections', function (Blueprint $table) {
	        $table->increments('id');
	        $table->string('section_name');
	        $table->string('about_section');
	        $table->integer('department_id')->unsigned();
	        $table->foreign('department_id')->references('id')->on('ic_departments')->onDelete('cascade');
	        $table->integer('group_id')->unsigned();
	        $table->foreign('group_id')->references('id')->on('ic_study_groups')->onDelete('cascade');
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ic_academic_sections');
    }
}
