<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ic_exam_schedule', function (Blueprint $table) {
            $table->increments('id');
            $table->string('exam_name');
            $table->string('exam_date');
            $table->string('exam_time');
            $table->string('duration');
            $table->string('total_marks');
            $table->integer('department_id')->unsigned();
            $table->foreign('department_id')->references('id')->on('ic_departments')->onDelete('cascade');
            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('ic_study_groups')->onDelete('cascade');
            $table->integer('section_id')->unsigned();
            $table->foreign('section_id')->references('id')->on('ic_academic_sections')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ic_exam_schedule');
    }
}
