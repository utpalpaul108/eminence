<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','PageController@homePage');
Route::get('/download_file','DownloadFileController@download');
Route::get('/message/{title}','MessageForUniversityController@show');
Route::get('results/show','ResultController@download_result');
Route::get('hsc_result','HscResultController@download');
Route::get('/notice_board/{title}','NoticeBoardController@show');
Route::get('/latest_news/{title}','LatestNewsController@show');

Route::post('/admin/authenticate', 'UserController@authenticate');
Route::get('/admin/logout', 'UserController@logout');
Route::get('/admin','AdminController@index');
Route::post('admin/send_mail','SendMailController@send');
Route::get('admin/management/login','AdminManagementPanelController@login');
Route::post('admin/management/authenticate','AdminManagementPanelController@authenticate');
Route::get('admin/management','AdminManagementPanelController@index');
Route::get('admin/management/logout','AdminManagementPanelController@logout');



Route::get('student/profile','StudentProfileController@edit');
Route::post('student/profile/update','StudentProfileController@update');
Route::resource('admin/payment','StudentPaymentController');
Route::resource('admin/payment-structure','PaymentStructureController');

Route::group(['middleware'=>'checkAuth'], function (){

	Route::resource('/admin/slider','AdminSliderController');
	Route::get('/admin/page/remove_file','AdminPageController@remove_file');
	Route::get('/admin/page/download_file','AdminPageController@download_file');
	Route::resource('/admin/pages','AdminPageController');
	Route::get('/admin/user/order','UserController@order_user');
	Route::get('/admin/teachers','UserController@assign_subject');
	Route::get('/admin/teachers/assign_subject/{id}','UserController@edit_assign_subject');
	Route::put('/admin/teachers/assign_subject/{id}','UserController@update_assign_subject');
	Route::get('/admin/user_type/order','UserController@order_user');
	Route::resource('/admin/user','UserController');
	Route::resource('/admin/section','AdminSectionController');
	Route::get('/admin/latest_news/order','LatestNewsController@order_news');
	Route::resource('/admin/latest_news','LatestNewsController',['except'=>['show']]);
	Route::get('/admin/hot_news/order','HotNewsController@order_news');
	Route::resource('/admin/hot_news','HotNewsController');
	Route::get('/admin/notice_board/order','NoticeBoardController@order_notice');
	Route::resource('/admin/notice_board','NoticeBoardController',['except'=>['show']]);
	Route::resource('/admin/photo_category','PhotoCategoryController');
	Route::post('/admin/delete_photo','PhotoGalleryController@delete_photo');
	Route::get('/admin/gallery_image/order','PhotoGalleryController@order_image');
	Route::resource('/admin/photo_gallery','PhotoGalleryController');
	Route::resource('/admin/message_for_university','MessageForUniversityController',['except'=>'show']);
	Route::resource('admin/college_achievement','CollegeAchievementController');
	Route::resource('admin/departments','DepartmentController');
	Route::resource('admin/class_routine','ClassRoutineController');
	Route::resource('admin/course_outline','CourseOutlineController');
	Route::resource('admin/hsc_result','HscResultController');
	Route::resource('admin/permissions','PermissionController');
	Route::resource('admin/roles','RoleController');
	Route::get('admin/result/report','ResultReportController@index');
	Route::get('admin/result/single_report','ResultReportController@single_report');
	Route::get('admin/result/print_report','ResultReportController@print_report');
	Route::get('admin/result/download_report','ResultReportController@download_report');
	Route::get('admin/result/download_excel','ResultReportController@download_excel');
	Route::get('admin/result/report/show','ResultReportController@show_report');
	Route::resource('admin/result','ResultController');
	Route::resource('admin/result_grade','ResultGradeController');
	Route::resource('admin/academic_session','AcademicSessionController');
	Route::resource('admin/academic_section','AcademicSectionController');
	Route::get('/admin/attendance/manage','StudentAttendanceController@manage');
	Route::get('/admin/attendance/report','StudentAttendanceController@report');
	Route::get('/admin/attendance/report/show','StudentAttendanceController@show_report');
	Route::post('/admin/attendance/report/download_pdf_report','StudentAttendanceController@download_pdf_report');
	Route::resource('/admin/attendance','StudentAttendanceController');
	Route::get('admin/student/promotion','AdminStudentController@promotion');
	Route::get('admin/student/promotion/manage','AdminStudentController@manage_promotion');
	Route::get('admin/student/promotion/update','AdminStudentController@update_promotion');
	Route::get('/admin/student/select_students','AdminStudentController@select_students');
	Route::post('/admin/student/send_sms','AdminStudentController@sendSMS');
	Route::post('/admin/student/send_single_sms','AdminStudentController@sendSingleSMS');
	Route::resource('/admin/student','AdminStudentController');
	Route::resource('admin/study_group','StudyGroupController');
	Route::get('admin/group_wise_data','SubjectController@group_wise_data');
	Route::get('admin/section_wise_data','SubjectController@section_wise_data');
	Route::get('admin/department_wise_group','SubjectController@department_wise_group');
	Route::resource('admin/main_subjects','MainSubjectsController');
	Route::post('admin/academic_subjects/order','SubjectController@order_subjects');
	Route::resource('admin/academic_subjects','SubjectController');
	Route::resource('admin/exams','ExamController');
	Route::resource('admin/exam_category','ExamCategoryController');
	Route::resource('admin/exam_type','ExamTypesController');
	Route::resource('admin/exam_schedule','ExamScheduleController');
	Route::get('admin/dashboard','DashboardController@index');
});


// For Ajax Call
Route::get('ajax_call/send_message','AjaxCallController@send_sms');
Route::get('ajax_call/select_type_wise_user','AjaxCallController@select_user');
Route::get('ajax_call/category_wise_image','AjaxCallController@select_image');
Route::get('ajax_call/section_wise_data','AjaxCallController@section_wise_data');
Route::get('ajax_call/exam_wise_subject','AjaxCallController@exam_wise_subject');
Route::get('ajax_call/department_wise_data','AjaxCallController@department_wise_data');
Route::get('ajax_call/get_student_sections','AjaxCallController@get_student_sections');
Route::get('ajax_call/get_tution_fee','AjaxCallController@get_tution_fee');
Route::get('ajax_call/get_due_payment','AjaxCallController@get_due_payment');
Route::get('ajax_call/get_exams','AjaxCallController@get_exams');

Route::get('/course_outline/{course}', 'PageController@download_course_outline');
Route::get('/{slug}','PageController@show')->name('page.show');



